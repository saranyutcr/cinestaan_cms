# Script to fetch Google Analytics data

require 'google/apis/analyticsreporting_v4'
require 'typhoeus'
require 'uri'

module Ga
def get_auth_bearer_token(credentials_file)
	credentials = Google::Auth::ServiceAccountCredentials.make_creds(json_key_io: IO.new(IO.sysopen("#{Rails.root}/config/cinestaan-staging-c70fe0aee983.json")))
	credentials.scope = 'https://www.googleapis.com/auth/analytics.readonly'
	return credentials.fetch_access_token!({})["access_token"]
end

def get_analytics(parameters, credentials_file)
	api = "https://content-analyticsreporting.googleapis.com/v4/reports:batchGet?alt=json"
	token = get_auth_bearer_token(credentials_file)
	auth = "Bearer " + token.to_s
	headers = { Authorization: auth, Accept: 'application/JSON', 'Content-Type'=>'application/JSON'}
	response = Typhoeus::Request.post(api, body: parameters.to_json, headers: headers, ssl_verifyhost: 0, ssl_verifypeer: false)
	return response
end

def get_realtime(rt_params)
	api = "https://content.googleapis.com/analytics/v3/data/realtime?" + URI.escape(rt_params)
	token = get_auth_bearer_token()
	auth = "Bearer " + token.to_s
	headers = { Authorization: auth, Accept: 'application/JSON'}
	response = Typhoeus::Request.get(api, headers: headers, ssl_verifyhost: 0, ssl_verifypeer: false)
end 

def build_realtime_parameters()
	id = "ids=ga:141655433"
	dimensions = "dimensions="
	metrics = "metrics="
	dimensions += "rt:eventCategory"
	dimensions += ","
	dimensions += "rt:eventAction"
	metrics += "rt:totalEvents"
	rt_params = id +"&"+ dimensions +"&"+ metrics
	response = get_realtime(rt_params)
	# p JSON.parse(response.body) #this is a hash
end
end


module GaCommonCode
	include Ga
	  @@region = "IN"
	  @@credentials_file = File.read("#{Rails.root}/config/cinestaan-staging-c70fe0aee983.json")

	def get_catalogs
		 catalogs_api = "http://"+get_ott_base_url+ "/catalogs?auth_token=" + current_bg.ott_auth_token + "&region=" + @@region
		response = http_request("get",{},catalogs_api,60)
		catalogs = response["body"]["data"]["items"]
		return catalogs
	end

	def get_catalog_items(catalog_id)
		catalogitems_api = "http://"+get_ott_base_url+ "/catalogs/" + catalog_id + "/items?auth_token=" + current_bg.ott_auth_token + "&region=" + @@region+ "&page_size=10000&status=any"
		response = http_request("get",{},catalogitems_api,60)
		catalog_items = response["body"]["data"]["items"]
		return catalog_items
	end

	def get_all_catalog_items(content_owner_id)
		@catalog_items = []
		catalogs = get_catalogs
		catalogs.each do |catalog_item|
		items = get_catalog_items(catalog_item["catalog_id"])
		if !content_owner_id.nil?
		@catalog_items << items.select {|h1| h1['content_owner_id']== content_owner_id}
		else
				@catalog_items << items
		end
		end
		return @catalog_items 
	end

	def get_ga_data(startdate, enddate)
	
	parameters = {"reportRequests" => [{"viewId" => "141655433","dateRanges" => [{"startDate" => startdate,"endDate" => enddate}],"metrics" => [{"expression" => "ga:totalEvents"},{"expression" => "ga:metric3"},{"expression" => "ga:metric4"},{"expression" => "ga:metric5"},{"expression" => "ga:metric6"},{"expression" => "ga:metric7"}],"dimensions" => [{"name" =>"ga:eventCategory"},{"name" => "ga:eventAction"},{"name" => "ga:eventLabel"}, {"name" => "ga:dimension8"}]}]}
	
	response = get_analytics(parameters, @@credentials_file)
	
	if response.code == 200
		ga_data = JSON.parse(response.body)
		return ga_data['reports'][0]
	end
	
	return nil
	end

	def process_ga_data(ga_data)
	aggregate_ga_data = []
	if !ga_data["data"]["rows"].nil?
		# Delete unnecessary data
		ga_data["data"]["rows"].delete_if { |row| row['dimensions'][0] != "Playback" }
		# Aggregate content
		ga_data["data"]["rows"].each do |row|
			content_index = ga_data["data"]["rows"].each_index.select{|i| ga_data["data"]["rows"][i]['dimensions'][2] == row['dimensions'][2]}
			h = {}
			h["content_id"]       = row['dimensions'][2]
			
			content_index.each do |i|	
				if (ga_data["data"]["rows"][i]['dimensions'][1] == "PlaybackTime")
					h["PlaybackTime"] = ga_data["data"]["rows"][i]['metrics'][0]['values'][3]
				else 
					if (ga_data["data"]["rows"][i]['dimensions'][1] == "playStart")
						h["PlayStart"] = ga_data["data"]["rows"][i]['metrics'][0]['values'][1]
					end
				end
			end	
			
			ga_data["data"]["rows"].delete_if { |x| x['dimensions'][2] == row['dimensions'][2] }
			
			aggregate_ga_data.push(h)
		end
	end
	return aggregate_ga_data
	end
	def merge_catalog_with_ga_data catalog_items,aggregate_ga_data
		merged_data = []
		total_views = 0
		items_count = 0
		total_revenue = 0.0
		duration_count = 0
		catalog_items.each do |item|
			if !item.nil?
				item.each do |i|
					if !i.nil?
						data = i
						res = aggregate_ga_data.find {|h1| h1['content_id']== i['content_id']}
						if !res.nil?
							data["views"] = res["PlayStart"]
						data["total_secs_viewed"] = res["PlaybackTime"]
						total_views += data["total_secs_viewed"].to_i / (60 * 60)
						total_revenue = total_revenue + (res["PlaybackTime"].to_i / (60 * 60)).to_f * data["cost_per_hour"].to_f
						
						else
							data["views"] = "0"
						data["total_secs_viewed"] = "0"
						end
						duration_count = duration_count + (convert_ds_into_hrs data["duration_string"])
						merged_data.push(data)
					end
					
					items_count = items_count  + 1
				end
			end
		end
		h = {"merged_data" => merged_data, "total_views" => total_views, "items_count" => items_count,"total_revenue" => total_revenue,"duration_count" => duration_count }
		return h
	end
	def convert_ds_into_hrs duration_string
		dur_in_secs = duration_string.to_s.split(':').map { |a| a.to_i }.inject(0) { |a, b| a * 60 + b}
		dur_in_hrs = dur_in_secs/(60 * 60)
		return dur_in_hrs.to_i
	end	
end

	
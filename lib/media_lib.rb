module MediaLib
	def validate_ott_media_params(params)
    @videoflag = params[:videoflag]
    if !params.has_key? "catalog_id"
      return false,"Please select Catalog!"
    end
  
    if params[:catalog_id].blank?
      return false,"Please select Catalog!"
    end
    
  	if @videoflag == "yes"
    else
    if !params.has_key? "apps"
  		return false,"Please Select Applications!"
  	end	
    end

  	if @videoflag == "yes"
    else
    if params["language"].empty?
  		return false,"Please Select Language!"
  	end	
    end

    if @videoflag == "yes"
    else
  	if !params.has_key? "genres"
  		return false,"Please Select Genres!"
  	end	
    end

    

    if params[:source_url]
  	  if params[:source_url].empty?	&& !params[:url_names]
  	  	return false,"Please provide source_url/play_url"
  	  end	

  	  if !(params[:source_url].empty?) && !(params[:url_names].include? "")
  	  	return false,"Please provide either source_url or play_url"
  	  end	

  	  if (params[:source_url].empty?) && (params[:url_names].include? "")
  	  	return false,"Please provide valid play url"
  	  end	
    else
      if !params[:url_names]
        return false,"Please provide play_url"
      end 

      if params[:url_names].include? ""
        return false,"Please provide valid play url"
      end 
    end  

    if params[:url_names]
    	if !params[:url_names].empty?
    		status,error = validate_play_urls(params)
    		return status,error if !status
      end
    end

    if !params[:utc_published_date].blank?
      if params[:utc_published_date] < DateTime.now.utc
        return false,"Published date can not be less than the current time!"
      end  
    end  

    if !params[:utc_published_date].blank? && !params[:utc_unpublished_date].blank?
      if params[:utc_unpublished_date] < params[:utc_published_date]
        return false,"Published date can not be greater than unpublished date!"
      end 

      if params[:utc_unpublished_date] == params[:utc_published_date]
        return false,"Published and unpublished date can not be equal!"
      end 
    end  

    return true,""
	end	

  

	def validate_play_urls(params)
		status,obj = check_duplicates(params["url_types"])
		return status,obj
	end	

	def check_duplicates(input_data)
    input_data.map!{ |e| e.downcase }
    dup_element = input_data.detect{ |e| input_data.count(e)> 1 }
	    if dup_element
	      return false,"Invalid Play URL"
	    end
    return true,nil
  end	

  def create_people_object(params)
    people_catalog_array = params["people"]
    roles = params["roles"]
    descriptions = params["descriptions"]
    # images = params["images"]
    url = "http://" + get_ott_base_url + "/people?auth_token=" + current_bg.ott_auth_token
    response = http_request("get",{},url,60)
    if response["code"] == 200
      @people = response["body"]["data"]["items"]
    end
    p_array = []
    i = 0
    people_catalog_array.each do |pc|
      person = {}
      person_id = pc.split("-")[0]
      catalog_id = pc.split("-")[1]
      details = @people.select{|h| h["catalog_id"]==catalog_id && h["person_id"]==person_id }
      if !details.blank?
        person_details = details.first 
        person["person_id"] = person_details["person_id"]
        person["catalog_id"] = person_details["catalog_id"]
        if roles[i].include? ","
          person["roles"] = roles[i].split(",") 
        else
          person["roles"] = [roles[i]]
        end
        person["description"] = descriptions[i]
        # person["display_image"] = images[i]
        p_array << person
      end
      i = i + 1
    end
    return p_array
  end

end
class SubscriptionController < ApplicationController

    include SubscriptionHelper
    include ApplicationHelper

	def new
		@bg = current_bg
		@all_tenants = Tenant.where(:bg_id => @bg.id).order_by(:created_at => 'desc')
	end

	def get_apps
		status = true
		error = "No apps"

		tenant_id = params[:tenant_id]
		apps = App.where(:tenant_id=>tenant_id)

		if status
        render json: {:apps => apps}
        else
        render :json => {:error => error}.to_json, :status => 500
        end
	end

	def create
		status = true
		error = ""
		@bg = current_bg
		if status
			metadata = subscription_items(@bg,params)
		    logger.debug "Subscription metadata is " + metadata.to_s
			
			status = metadata["status"]
			error = metadata["error"]
		end
		if status
			render json: {} , status: :ok
		else
			render :json => {:error => error}.to_json, :status => 500
		end
	end

	def view_all		
		@tenant_id = params[:tenant_id]
		@content_id = params[:content_id]
		@app_id = params[:app_id]
	end

	def get_all
		status = true
		error = ""

		@bg = current_bg
		catalogs = Catalog.find_by(:bg_id => @bg.id, :theme => "subscription", :tenant_id => params[:tenant_id])
		url = "http://"+get_ott_base_url+"/catalogs/"+catalogs.ott_id+"/items?region=IN&auth_token="+@bg.ott_auth_token+"&status=edit"
		response = http_request("get",{},url,60)
		@subscription = response["body"]["data"]
		status = response["status"]
		error  = response["error"]

		if status
		data_list = Kaminari.paginate_array(@subscription["items"]).page(params[:page])
        render json: {:items => @subscription, :total_count => data_list.total_count, :total_pages => data_list.total_pages}
        else
        render :json => {:error => error}.to_json, :status => 500
        end
	end

	def add_plans
		status = true
		error = ""
		app_id = params[:app_id]
		@items_id = params[:content_id]
		@bg = current_bg
		@tenant_id = params[:tenant_id]
		catalogs = Catalog.find_by(:bg_id => @bg.id, :theme => "subscription", :tenant_id => params[:tenant_id])
		if app_id == "0"
		url = "http://"+get_ott_base_url+"/catalogs/"+catalogs.ott_id+"/items/"+@items_id+"?auth_token="+@bg.ott_auth_token+"&region=IN&status=any"
		else
		apps = App.find_by(:ott_id => app_id)
		url = "http://"+get_ott_base_url+"/catalogs/"+catalogs.ott_id+"/items/"+@items_id+"?auth_token="+apps.ott_auth_token+"&region=IN&status=any"
		end
		response = http_request("get",{},url,60)
		@subscription = response["body"]["data"]

		if params[:method] == "viewall"
		#render "subscription/view_all"
		#render json: {:items => @subscription}
		subtitle = @subscription["title"]
		data_list = Kaminari.paginate_array(@subscription["plans"]).page(params[:page])
        render json: {:items => data_list, :subtitle=> subtitle, :total_count => data_list.total_count, :total_pages => data_list.total_pages}
         
		else
		if params[:method] == "view"
		if status
        render partial: "view_info"
        else
        render :json => {:error => error}.to_json, :status => 500
        end
    	else
    	render "subscription/add_plans"
    	end
    	end
	end

	def add_more_plans
		status = true
		error = ""
		items_id = params[:items_id]
		@tenant_id = params[:tenant_id]
		@bg = current_bg
		if status
			metadata = subscription_plans(@bg,params,items_id,@tenant_id)
		    logger.debug "Plans metadata is " + metadata.to_s
			
			status = metadata["status"]
			error = metadata["error"]
		end
		if status
			render json: {} , status: :ok
		else
			render :json => {:error => error}.to_json, :status => 500
		end
	end

	def get_plan
		status = true
		error = ""
		@app_id = params[:app_id]
		@plan_id = params[:plan_id]
		@tenant_id = params[:tenant_id]
		@bg = current_bg
		apps = App.find_by(:ott_id => @app_id)
		url = "http://"+get_ott_base_url+"/subscriptions/plans/"+@plan_id+"?auth_token="+apps.ott_auth_token+"&region=IN&status=any"
		response = http_request("get",{},url,60)
		@plans = response["body"]["data"]

		if params[:method] == "edit"
			render "subscription/edit_plans"
		else   
		if status
			render json: {:items => @plans}
		else
			render :json => {:error => error}.to_json, :status => 500
		end
		end
	end

	def get_user_pack
		status = true
		error = ""
		@bg = current_bg
		@user_id = params[:user_id].strip
		@tenant_id = params[:tenant_id]
		apps = App.find_by(:tenant_id => @tenant_id)
		url = "http://"+get_ott_base_url+"/admin/plans?auth_token="+apps.ott_auth_token+"&region=IN&user_id="+@user_id
		response = http_request("get",{},url,60)
		@packs = response["body"]["data"]
		if params[:method] == "view"
		if status
			@spacks = []

			@packs.each do |pa|
			pack = {}
			pack["user_id"] = pa["user_id"]
			pack["plan_id"] = pa["plan_id"]
			pack["region"] = pa["region"]
			pack["category"] = pa["category"]
			pack["valid_till"] = DateTime.parse(pa["valid_till"]).strftime("%B %d %y %H:%M:%S")
			pack["start_date"] = DateTime.parse(pa["start_date"]).strftime("%B %d %y %H:%M:%S")
			@spacks << pack
			end

			@spacks = Kaminari.paginate_array(@spacks).page(params[:page])
        	render json: {:packs => @spacks, :total_count => @spacks.total_count, :total_pages => @spacks.total_pages}
			#render json: {:packs => @packs}
		else
			render :json => {:error => error}.to_json, :status => 500
		end
	   else
	   	if @packs.nil?
	   		error = response["body"]["error"]["message"]
	   		puts "error======"
	   		p error
	   		render :json => {:error => error}, :status => 500
	   	elsif @packs.empty?
	   		render partial: "user_data"
	   	else
		lastpack = @packs.last["valid_till"]
		puts "@lastpackvalid======"
		cdt = DateTime.parse(lastpack)
		@lastpackvalid = cdt.strftime("%B %d %y %H:%M:%S")
		p @lastpackvalid
		render partial: "user_data"
		end
	   	
	   end
	end

	def tenant_plans
		@bg = current_bg
		@tid = params[:tenant_id]
		env = Rails.env
		case env
		when "development"
			@webview = @bg.staging_webview
		when "preproduction"
			@webview = @bg.preprod_webview
		when "production"
			@webview = @bg.prod_webview
		end
		if params[:pack] == "pack"
		render partial: "get_user"
		elsif params[:coupon] == "coupon"
		render partial: "coupons_data"
	    elsif params[:access] == "access"
		render partial: "/access/view_all"
		elsif params[:appsdata] == "appsdata"
		render partial: "/apps/view_all"
		elsif params[:appsmessages] == "appsmessages"
		render partial: "/message/view_all"
		else			
		render partial: "plans_data"
		end

	end 
end

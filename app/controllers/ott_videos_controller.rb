class OttVideosController < ApplicationController
	include ApplicationHelper
	
	def fetch_all
		status = true
		error = ""
		body = {}
  		maxtime = 60
  		video_list = nil
  		bg = current_bg
		begin
			Timeout::timeout(maxtime) do
				Fiber.new{
					url = "http://#{get_ott_base_url}/catalog/videos?auth_token=#{bg.ott_auth_token}"
					ret = Typhoeus::Request.get(url,:body => body.to_json,:headers => {"content-type" => "application/json","accept" =>"application/json"})
					if ret.options[:response_body]
						video_list = JSON.parse(ret.options[:response_body])["videos"]
					end
				}.resume
			end
		rescue Timeout::Error
			status = false
			error = "Request Timed Out"
		rescue 	Exception => e
			Rails.logger.debug e.message
			Rails.logger.debug e.backtrace.inspect
			status = false
			error = "Exception Raised"
		end
		if status
			render json: {:video_list => video_list} , status: :ok
		else
			render :json => {:error => error}.to_json, :status => 500
		end
	end
	def fetch
	end
	def create
		status = true
		error = ""
		body = {}
		body["title"] = params[:title]
		body["smart_url"] = params[:smart_url]
		body["content_type"] = "video"
		body["language"] = params[:language]
		body["production_year"] = 1900
		body["synopsis_short"] = ""
		body["synopsis_large"] = ""
		body["genre"] = ["Infotainment"]
		body["production_country"] = "aaa"
		body["keywords"] = "hgj gjh gjhgjhg"
		body["content_rating"] = 1
		body["trivia"] = [""]
		body["sub_type"] = "hgj hgjhgjhgjhgj"
		body["video_series_name"] = "video"
		body["listing_image_medium"] = "ftp://dittotv:ditto@123/home/ubuntu/pic.jpg"
		body["meta_keywords"] = ""
		body["duration"] = params[:duration].to_i
		body["region"] = ["IN"]
		body["type"] = "videos"
		body["video_path"] = "x"
		body["rating"] = params[:rating].to_i
		body["listing_series_image"] = "s"
		body["featured_series_image"] = "s"
		body["video_image"] = "asd"
		body["certification"] = "A"
		body2 = {}
		body2["videos"] = body
  		maxtime = 60
  		bg = current_bg
		begin
			ret = nil
			Timeout::timeout(maxtime) do
				Fiber.new{
					url = "http://#{get_ott_base_url}/catalog/videos/info?auth_token=#{bg.ott_auth_token}"
					ret = Typhoeus::Request.post(url,:body => body2.to_json,:headers => {"content-type" => "application/json","accept" =>"application/json"})
				}.resume
			end
		rescue Timeout::Error
			status = false
			error = "Request Timed Out"
		rescue 	Exception => e
			Rails.logger.debug e.message
			Rails.logger.debug e.backtrace.inspect
			status = false
			error = "Exception Raised"
		end
		if status
			render json: {} , status: :ok
		else
			render :json => {:error => error}.to_json, :status => 500
		end
	end
	def update
	end
	def delete
		status = true
		error = ""
		body = {}
  		maxtime = 60
  		bg = current_bg
		begin
			Timeout::timeout(maxtime) do
				Fiber.new{
					url = "http://#{get_ott_base_url}/catalog/videos/#{params["content_id"]}?auth_token=#{bg.ott_auth_token}"
					ret = Typhoeus::Request.delete(url,:body => body.to_json,:headers => {"content-type" => "application/json","accept" =>"application/json"})
				}.resume
			end
		rescue Timeout::Error
			status = false
			error = "Request Timed Out"
		rescue 	Exception => e
			Rails.logger.debug e.message
			Rails.logger.debug e.backtrace.inspect
			status = false
			error = "Exception Raised"
		end
		if status
			render json: {} , status: :ok
		else
			render :json => {:error => error}.to_json, :status => 500
		end
	end
end
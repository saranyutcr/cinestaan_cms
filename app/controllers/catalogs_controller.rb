class CatalogsController < ApplicationController
	include CatalogsHelper
	include ApplicationHelper
	include TrackerLib
	
    before_filter :get_default, only: [:edit]
    before_filter :get_bg_plan, only: [:create]

    def get_default
    	url = get_ott_base_url+"/configurations?auth_token="+current_bg.ott_auth_token
   		response = http_request("get",{},url,60)
   		@default_data = response["body"]["data"]
    end	

    def get_bg_plan
    	@bg_plan = BusinessGroupPlan.find_by(:status => "active",:business_group_id => current_bg.id)
    end	

	def create
		@bg = current_bg
		status = true
		error = ""
		#validate_catalog_params(params)
		if status
			params[:bg_object] = @bg
			params[:entity] = "catalog"
			params[:bg_plan] = @bg_plan
			status,message = track_entity_count(params)
			if status
				params[:name] = params[:name].strip
				response = post_catalog_to_ott(@bg,params)
				status = response["status"]
				error = response["error"]
			else
			    error = message	
			end
		end
		if status
			render json: {} , status: :ok
		else
			render :json => {:error => error}.to_json, :status => 500
		end
	end
	def update
		status = true
		error = ""
		case params[:method]
		when "delete"
			
			catalog = Catalog.find(params[:catalog_id])
			if catalog
				params[:catalog_obj] = catalog
				theme = catalog.theme
				case theme
				when "show"
					show = ShowTheme.new.get_show_by_catalog(params)
					if show.count > 0
						status = false
						error = "Please delete shows under this Catalog!"
					else
						#delete catalog from OTT
						status,error = catalog.delete_at_ott

						#Delete catalog from CMS
						if status
							status,error = catalog.delete_at_cms
						end	
					end	
				when "livetv"

				when "person"	
					url = "http://"+get_ott_base_url+"/catalogs/"+catalog.ott_id+"/items?auth_token="+current_bg.ott_auth_token+"&status=any&region=any&page=0&page_size=30"
					response = http_request("get",{},url,60)
					if !response["status"]
						status = response["status"]
						error = response["error"]
					else
					    data = response["body"]["data"]["items"]	
					    if data.count > 0
					    	status = false
							error = "Please delete people from this Catalog!"
					    else
					    	#delete catalog from OTT
							status,error = catalog.delete_at_ott

							#Delete catalog from CMS
							if status
								status,error = catalog.delete_at_cms
							end	
					    end	
					end
				else
					media = Media.new.get_media_by_catalog(params)
					if media.count > 0
						status = false
						error = "Please delete media under this Catalog!"
					else
						#delete catalog from OTT
						status,error = catalog.delete_at_ott

						#Delete catalog from CMS
						if status
							status,error = catalog.delete_at_cms
						end	
					end	
				end	
				
			else
				status = false
				error = "Unable to delete catalog!"	
			end	
		when "edit"
			@bg = current_bg	
			response = post_catalog_to_ott(@bg,params)
			status = response["status"]
			error = response["error"]
		end
		
		if status
			render json: {} , status: :ok
		else
			render :json => {:error => error}.to_json, :status => 500
		end

	end
	def fetch_items
		status = true
		error = ""
		catalog = Catalog.find(params[:catalog_id])
		bg = BusinessGroup.find(catalog.bg_id)
		#if params[:search]
			#search_content?catalog_id=5811f2fd4937d30b9900022e&q=kyaa&region=IN&auth_token=hsByrxoM7TJjzhTETQMz
			#url = "http://"+get_ott_base_url+"/search_content?catalog_id="+catalog.ott_id+"&auth_token="+bg.ott_auth_token+"&q="+params[:search]+"&status=any&region=any&page=0&page_size=30"	
		#else
			#catalog remote fetch
			url = "http://"+get_ott_base_url+"/catalogs/"+catalog.ott_id+"/items?auth_token="+bg.ott_auth_token+"&status=any&region=IN"
		#end
		response = http_request("get",{},url,60)
		if !response["status"]
			status = response["status"]
			error = response["error"]
		end
		if status
			data = response["body"]["data"]["items"]
			final_data = []
			data.each do |h|
			  h["theme"] = catalog.theme	
			  h["cms_id"] = ""
			  if catalog.theme == "show"
			  	if h['content_id'].nil?
			  		h["cms_id"] = ShowTheme.find_by(:ott_id => h["content_id"]).id
			  	end
			  end
			  if catalog.theme == "article"
			  	h["cms_id"] = ArticleTheme.find_by(:ott_id => h["content_id"]).id
			  end	
			  final_data << h
			end	
			content_list = Kaminari.paginate_array(final_data).page(params[:page])
			render json: {:list => content_list, :total_count => content_list.total_count, :total_pages => content_list.total_pages} , status: :ok
		end
	end

	def fetch_all
		@bg = current_bg
		catalog_list = Catalog.all_of(:tenant_id => params[:tenant_id]).order_by(:created_at => 'desc').page(params[:page]).not_in(:theme => [ "access_control","subscription","message","coupon"])
		render json: {:list => catalog_list, :total_count => catalog_list.total_count, :total_pages => catalog_list.total_pages} , status: :ok
	end

	def new
		@tenant_id = params[:tid]
		@bg = current_bg
		url = "http://"+get_ott_base_url+"/configurations?auth_token="+@bg.ott_auth_token
		response = http_request("get",{},url,60)
		@ott_default_config = response["body"]["data"]

		# Upload image to s3 server
		s3uploadremovspace = @bg.name
		s3upload = s3uploadremovspace.gsub(" ", "_");
	    @signature = get_s3_upload_key(s3upload,@bg.bucket_name,@bg.domain_name)
	end

	def get_all

		@bg = current_bg
		catalog_list = Catalog.all_of(:bg_id => current_bg.id).order_by(:created_at => 'desc').page(params[:page]).per(params[:page_size]).not_in(:theme => [ "access_control","subscription","message","coupon"])
		render json: {:list => catalog_list, :total_count => catalog_list.total_count, :total_pages => catalog_list.total_pages} , status: :ok
		
       #  catalogs = Catalog.where(:bg_id => current_bg.id)
        
       #  data = catalogs.as_json.map do |catalog|
       #  	tenant_names = []
       #      tenants = catalog["tenant_id"]
       #      tenants.each do |t|
       #          name = Tenant.find(t).name
       #          tenant_names << name if name
       #      end 
       #      catalog.merge!({"tenant_names" => tenant_names})	
       #  end	
       #  data_list = Kaminari.paginate_array(data).page(params[:page])
       # render json: {:list => data_list, :total_count => data_list.total_count, :total_pages => data_list.total_pages}
    end		

	def view_all
		@bg = current_bg
			if params[:page].nil?
				page = 1
				next_page = page + 1
			else
				page = params[:page]
				next_page = page.to_i + 1
			end
			page_size = params[:page_size] ? params[:page_size] : 25
		
		@catalogs = Catalog.all_of(:bg_id => current_bg.id).order_by(:created_at => 'desc').page(page).per(page_size).not_in(:theme => [ "access_control","subscription","message","coupon"])
		
		@next_catalogs = Catalog.all_of(:bg_id => current_bg.id).order_by(:created_at => 'desc').page(next_page).per(page_size).not_in(:theme => [ "access_control","subscription","message","coupon"])
    end

	def view_catalog
		@page_no = 0
		@catalog = Catalog.find(params[:catalog_id]) || Catalog.find_by(:ott_id => params[:catalog_id])
		bg = current_bg 

		#Optimise this if condition
		if @catalog.theme == "person" || @catalog.theme == "show" || @catalog.theme == "livetv" || @catalog.theme == "user"

			if params[:page].nil?
				page = 0
				next_page = page + 1
			else

				page = params[:page]
				next_page = page.to_i + 1
			end
			page_size = params[:page_size] ? params[:page_size] : 25



				url = "http://"+get_ott_base_url+"/catalogs/"+@catalog.ott_id+"/items?auth_token="+bg.ott_auth_token+"&status=any&region=any&page="+page.to_s+"&page_size="+page_size.to_s
				next_url = "http://"+get_ott_base_url+"/catalogs/"+@catalog.ott_id+"/items?auth_token="+bg.ott_auth_token+"&status=any&region=any&page="+next_page.to_s+"&page_size="+page_size.to_s
			
			response = http_request("get",{},url,60)
			if !response["status"]
				status = response["status"]
				error = response["error"]
			end
			if response["status"]
				data = response["body"]["data"]["items"]

				
				@items = []
				data.each do |h|
				  h["theme"] = @catalog.theme	
				  if @catalog.theme == "show"
				  	
				  	if !h['content_id'].nil?
				  		h["cms_id"] = ShowTheme.find_by(:ott_id => h["content_id"]).id
				  		
				  	end
				  end
				  if @catalog.theme == "article"
				  	h["cms_id"] = ArticleTheme.find_by(:ott_id => h["content_id"]).id
				  end
				  
				  @items << h	
				end	
				
			end
			
				
			response = http_request("get",{},next_url,60)
			if !response["status"]
				status = response["status"]
				error = response["error"]
			end	
			if response["status"]
				@next_items = response["body"]["data"]["items"]
			end		

		else
			@bg = current_bg
			if params[:page].nil?
				page = 1
				params[:page] = page
				next_page = page + 1
			else
				page = params[:page]
				next_page = page.to_i + 1
			end

			page_size = params[:page_size] ? params[:page_size] : 2	

			status = true
			catalog_id = params[:catalog_id]
			ott_catalog_id = Catalog.find(catalog_id).ott_id
						
			current_list =  Media.where(:ott_catalog_id=>ott_catalog_id).where(:is_removed => false, :videolist => false).order_by(:created_at => 'desc').page(page).per(page_size)

			if params["search"] && params["search"] != ""
				current_list = current_list.where(:title => /params["search"]/i)
			end

			#Finding next items for pagination disabling
			next_list = Media.where(:ott_catalog_id=>ott_catalog_id).where(:is_removed => false, :videolist => false).order_by(:created_at => 'desc').page(next_page).per(page_size)
			
			@items = current_list
			@next_items = next_list
			
			
		end
	end

	def view_item
		status = true
		error = ""
		@bg = current_bg
		catalog = Catalog.find(params[:catalog_id])
		url = "http://"+get_ott_base_url+"/catalogs/"+catalog.ott_id+"/items/#{params["item_id"]}?auth_token="+@bg.ott_auth_token+"&region=any&status=any"
		response = http_request("get",{},url,60)
		if !response["status"]
			status = response["status"]
			error = response["error"]
		end
		if status
			@item = {}
			@item["title"] = response["body"]["data"]["title"]
			@item["content_id"] = response["body"]["data"]["content_id"]
			@item["description"] = response["body"]["data"]["description"]
			@item["language"] = response["body"]["data"]["language"]
			@item["genres"] = response["body"]["data"]["genres"]
			@item["smart_url"] = response["body"]["data"]["smart_url"]
			@item["thumbnail"] = response["body"]["data"]["thumbnails"]["l_large"]["url"]
		end
		#logger.debug @item
		render partial: "view_item"
	end

	def show
		status = true
		@bg = current_bg
		catalog = Catalog.find_by(:id => params[:catalog_id])
		optradio = params[:videoflag]

       	if optradio == "yes"

	       	url = "http://"+get_ott_base_url+"/catalogs/"+catalog.ott_id+"/videolist_tags"+"?auth_token="+@bg.ott_auth_token

			response = http_request("get",{},url,60)
			if !response["status"]
				status = response["status"]
				error = response["error"]
			end

			if status
			  @videolist_tags = response["body"]["data"]["items"]
			end	
			render json: { :videolist_tags => @videolist_tags}

       	else
			url = "http://"+get_ott_base_url+"/catalogs/"+catalog.ott_id+"?auth_token="+@bg.ott_auth_token

			response = http_request("get",{},url,60)
			if !response["status"]
				status = response["status"]
				error = response["error"]
			end

			if status
			    @catalog = response["body"]["data"]	

			    @t_ott_id = response["body"]["data"]["tenant_ids"]
			    @videolist_tags = response["body"]["data"]["videolist_tags"]

			    @tenant_list = []
			    @accesscontrol = []
			    @valvideolist_tags = []

			    @videolist_tags.each do |vlt|
			    	@valvideolist_tags << vlt
			    end

			    @t_ott_id.each do |obj|
			  	  Tenant.where(:ott_id=>obj).each do |tenan|
				  	
				  	h = {}
				 	h["id"] = tenan.id
				 	h["name"] = tenan.name
				 	h["ott_id"] = tenan.ott_id

					h ["genres"] =[]
					response["body"]["data"]["genres"].keys.each do |g|
			          h["genres"] = (response["body"]["data"]["genres"][tenan.ott_id]).map{|gen| gen["name"] } if g == h["ott_id"]
					end

					h ["languages"] =[]
					response["body"]["data"]["languages"].keys.each do |l|
					  h ["languages"] = (response["body"]["data"]["languages"][tenan.ott_id]).map{|lan| lan["name"] } if l == h["ott_id"]
					end 
			    	
				    h["applications"] = []
				    App.where(:tenant_id => tenan.id).each do |tapp|
				        if @catalog["application_ids"].include? tapp.ott_id	
				            application = {}
				            application["a_id"] = tapp.id
						    application["a_name"] = tapp.name
						    application["a_tid"] = tapp.tenant_id
						    h['applications'] << application
					    end
				    end
			 		@tenant_list << h

			 		catalogs = Catalog.find_by(:bg_id => @bg.id, :theme => "access_control", :tenant_id => tenan.id.to_s)
			 		acurl = "http://"+get_ott_base_url+"/catalogs/"+catalogs.ott_id+"/items?auth_token="+@bg.ott_auth_token+"&status=any&region=any"
					acresponse = http_request("get",{},acurl,60)
					if acresponse["code"] == 200
					accesscontrols = acresponse["body"]["data"]["items"]

					accesscontrols.each do |acc|
						ac = {}
						ac["name"] = tenan.name
						ac["title"] = acc["title"]
						ac["content_id"] = acc["content_id"]
						@accesscontrol << ac
					end
					end
			      end
		        end
			end	
			render json: { :tenants => @tenant_list, :valvideolist_tags => @valvideolist_tags, :catalog => @catalog, :accesscontrol => @accesscontrol}
	    end
    end	

    def edit
    	@tenant_id = params[:tid]
    	@bg = current_bg
    	@catalog = (Catalog.find(params[:catalog_id])).as_json
    	url = "http://"+get_ott_base_url+"/catalogs/"+@catalog["ott_id"]+"?auth_token="+@bg.ott_auth_token
    	response = http_request("get",{},url,60)

    	s3uploadremovspace = @bg.name
    	s3upload = s3uploadremovspace.gsub(" ", "_");
	    @signature = get_s3_upload_key(s3upload,@bg.bucket_name,@bg.domain_name)
	    
    	if response["code"] == 200
    		@catalog_info = response["body"]["data"] 
    	end	
    	if params[:method] == "view"
    		render "catalogs/view_catalog_info"
    	end
    end

    def view_catalog_info
    	@data = {}
		catalog = Catalog.find(params[:catalog_id]) || Catalog.find_by(:ott_id => params[:catalog_id])
		if catalog
			catalog_apps = catalog["app_id"]
			@data.merge!(catalog.as_json)
			@data["tenants"] = []
			catalog["tenant_id"].each do |tenant|
				h = {}
				t = Tenant.find(tenant)["name"]
				h["tenant_name"] = t
				app_names = App.where(:tenant_id => tenant).map { |app| 
					if catalog_apps.include?("#{app.id}") 
						app["name"]
					end	
					}.compact
				h["app_names"] = app_names
				@data["tenants"] << h
			end	
	    end		
	    render partial: "catalogs/view_catalog_info"
	end	

	def export_data
		@bg = current_bg
		catalog_id = params[:catalog_id].strip
		catalog = Catalog.find(catalog_id)
		if catalog
			email_id = current_user.email
			ott_id = catalog.ott_id
			url = "http://"+get_ott_base_url+"/catalogs/"+ott_id+"/export_data?auth_token="+@bg.ott_auth_token+"&email_id=" + email_id
	    	response = http_request("get",{},url,60)
	    	if response["code"] == 200
				flash[:notice] = "CSV will be send to your registered Email"
			else
			    flash[:notice] = "No items present under this catalog. Please add some data!"	
			end
			render json: { } , status: :ok
		else	

		end	
	end	

	def view_media_status
		@catalog_id = params[:catalog_id]
		render partial: "catalogs/view_media_status"
	end	

end

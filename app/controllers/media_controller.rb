class MediaController < ApplicationController
	include UsersHelper
	include MediaHelper
	include HomeHelper
	include PaginateMe
	include ApplicationHelper
	include MediaLib
	include CatalogsHelper
	include TrackerLib
	
	before_filter :login_check, :set_objects
	before_filter :get_bg_plan, only: [:create,:bulk_upload,:bulk_videolist_upload,:bulk_videolist_update]
	#before_filter :get_default, only: [:edit,:new]

	def set_objects
	    @catalog_object = Catalog.new
	end
		
	def get_bg_plan
    	@bg_plan = BusinessGroupPlan.find_by(:status => "active",:business_group_id => current_bg.id)
    end		

	def create
		@videoflag = params[:videoflag]
		@Eshow_id = params[:Eshow_id]
		@Esubcategory_id = params[:Esubcategory_id]
		status = true
		error = ""
		bg = current_bg
		
		if bg.plan != "transcoding"
		  params[:catalog_id] = params[:catalog_id].split("-")[0]	
		end	

		if status
			if !params[:title].strip
				status = false
				error = "Title cannot be empty!"
			end
		end
		if status
			if params[:title].strip == ""
				status = false
				error = "Title cannot be empty!"
			end
		end

		if status && (bg.plan=="transcoding")
			if status
				if !params[:source_url]
					status = false
					error = "Video URL cannot be empty!"
				end
			end
			if status
				if (!params[:source_url].start_with?('ftp://')) && (!params[:source_url].start_with?('http://'))
					status = false
					error = "Error in Video URL Format"
				end
			end
			if status
				if !params[:ps_id]
					status = false
					error = "Publish Settings ID cannot be empty!"
				end
			end
			if status
				if params[:ps_id].strip == ""
					status = false
					error = "Publish Settings ID cannot be empty!"
				end
			end

			if status
                if params[:source_url].empty?
                	status = false
                	error = "Please provide source url!"
                end	
			end	
		else

			if params[:utc_published_date] 
				if params[:utc_published_date] == "undefined"
					params[:utc_published_date] = ""
				else
					t = params[:utc_published_date].to_i / 1000
					params[:utc_published_date] =  Time.at(t).utc
				end	
			end	

			if params[:utc_unpublished_date] 
				if params[:utc_unpublished_date] == "undefined"
					params[:utc_unpublished_date] = ""
				else	
					t = params[:utc_unpublished_date].to_i / 1000
					params[:utc_unpublished_date] =  Time.at(t).utc
				end	
			end	

            if status
            	# if @videoflag == "yes"
            	# else
	            	if !params.has_key? "apps"
	            		status = false
	            		error = "Please Select Applications!"
	            	end	
                #end
            end	

            if status
            	# if @videoflag == "yes"
            	# else
            	if params["language"].empty?
            		status = false
            		error = "Please Select Language!"
            	end	
            	# end
            end	

            if status
            	# if @videoflag == "yes"
            	# else
            	if !params.has_key? "genres"
            		status = false
            		error = "Please Select Genres!"
            	end	
            	# end
            end

            if status
				status,error = validate_ott_media_params(params)
			end

		end
		media_params = {}
		if status
			media_params["cut_intervals"] = params[:cut_intervals]
			media_params["banner"] = params[:banner]
			media_params["title"] = params[:title].strip
			source_type = params[:source_type]
			if source_type == "uploadvideo"
				source_type = "HTTP"
			else
				source_type
			end
			media_params["source_type"] = source_type
			media_params["source_url"] = fix_multiple_slashes(params[:source_url])
			#media_params["source_url"] = params[:source_url]
			#content owner details
			media_params["duration_string"] = params[:duration_string]
			media_params["end_time"] = params[:end_time]
			media_params["start_time"] = params[:start_time]
			media_params["censor_rating"] = params[:censor_rating]
			media_params["currency"] = params[:currency]
			media_params["cost_per_hour"] = params[:cost_per_hour] 
			media_params["content_owner_id"] = params[:content_owner]
			media_params["notification_url"] = params[:notification_url]
			media_params["user_id"] = current_user.id
			media_params["user_role"] = current_user.roles.first
			if current_bg.plan == "ott_saas" 
				media_params["publish_settings_id"] = PublishSettings.find_by(:bg_id => current_bg.id).id
			else	
			    media_params["publish_settings_id"] = params[:ps_id]
		    end
			media_params["bg_id"] = current_bg.id
			if (!params[:priority]) || (params[:priority] == "")
				media_params["priority"] = "low"
			else
				media_params["priority"] = params[:priority]
			end
			media_params["status"] = "Created"
			media_params["ott_status"] = "edit"
			media_params["is_removed"] = false
			media_params["bulk_upload_id"] = nil
			media_params["custom_keys"] = nil
			media_params["smart_url"] = params[:smart_url]
			media_params["videoflag"] = params[:videoflag]
			media_params["show_id"] = params[:Eshow_id]
			media_params["subcategory_id"] = params[:Esubcategory_id]
			media_params["utc_published_date"] = params[:utc_published_date]
			media_params["utc_unpublished_date"] = params[:utc_unpublished_date]
			if params.has_key? "imgoptradio"
				media_params["imgoptradio"] = params["imgoptradio"]
			else
			  media_params["imgoptradio"] = "no"	
			end	
			status = validate_media_params(media_params)
		end
		if status

			params[:bg_object] = bg
			params[:entity] = "media"
			params[:bg_plan] = @bg_plan
			status,message = track_entity_count(params)

			if status
			media = create_media(media_params)
			#if BusinessGroup.find(media.bg_id).plan=="transcoding" #auto-transcode if non-ott bg
			if bg.plan == "transcoding"	
				status,error = media.transcode_request
			else
				if params[:catalog_id]
					# if !media_params["smart_url"].empty?
					if params[:source_url].empty?
                        media.status = "Created Externally" 
					end	
					catalog = Catalog.find(params[:catalog_id])
					theme = catalog.theme
					data = params
					# status,params = check_images(catalog,data,bg)
					# if status 
						media.ott_catalog_id = catalog.ott_id
						media.save
							videoflag = params[:videoflag]
							# if videoflag == "yes"
							# metadata = extract_metadata_from_videolistparams(params)
							# logger.debug "Extracted videolist metadata is " + metadata.to_s
							# else
							metadata = extract_metadata_from_params(params)
							logger.debug "Extracted metadata is " + metadata.to_s
							# end
							if media.ott_id
								status,error = media.update_at_ott(metadata)
							else
								videoflag = params[:videoflag]
								# if videoflag == "yes"
								# 	status,error = media.create_videotags_at_ott(metadata,catalog)
								# else
									if theme == "show"
										if @Eshow_id.nil? || @Eshow_id.empty?
										status,error = media.create_at_ott(metadata,catalog,@Esubcategory_id)
										else
								        status,error = media.create_at_ott(metadata,catalog,@Eshow_id)
								    	end
									else
								    status,error = media.create_at_ott(metadata,catalog,@Eshow_id)
									end
							    # end

								if !status
									media.destroy  #Check for integrity on delete
									message = error
								else
								#if status is true then check publish and unpublish date and run delayed	
								   	if !metadata["published_date"].blank? && metadata["status"] != "published"
								   		media.add_media_in_publish_queue(metadata)
								   	end
								   	if !metadata["unpublished_date"].blank?
								   		media.add_media_in_unpublish_queue(metadata)
								   	end
								   	media.save
								end	
							end
						# else
						# 	message = "Please provide images"
						#     media.destroy	
						# end	
					end
				end
			end	
		else
			message = error 
			error = "Validation Error in Input Media Parameters"
		end

		if status
			render json: {:media_id => media.id} , status: :ok
		else
			#render :json => {:error => error}.to_json, :status => 500
			render :json => {:error => message}.to_json, :status => 500
		end
	end

	def update
		if params[:method] == "update"
			status = true
			error = ""
			bg = current_bg
			if status
				if !params[:title].strip
					status = false
					error = "Title cannot be empty!"
				end
			end
			if status
				if params[:title].strip == ""
					status = false
					error = "Title cannot be empty!"
				end
			end

			if params[:utc_published_date] 
				if params[:utc_published_date] == "undefined"
					params[:utc_published_date] = ""
				else	
					t = params[:utc_published_date].to_i / 1000
					params[:utc_published_date] =  Time.at(t).utc
				end	
			end	

			if params[:utc_unpublished_date] 
				if params[:utc_unpublished_date] == "undefined"
					params[:utc_unpublished_date] = ""
				else	
					t = params[:utc_unpublished_date].to_i / 1000
					params[:utc_unpublished_date] =  Time.at(t).utc
				end	
			end	

			if status && (bg.plan=="transcoding")
				if status
					if !params[:source_url]
						status = false
						error = "Source URL cannot be empty!"
					end
				end
				if status
					if !params[:source_url].start_with?('ftp://')
						status = false
						error = "Error in Source URL Format"
					end
				end
				if status
					if !params[:ps_id]
						status = false
						error = "Publish Settings ID cannot be empty!"
					end
				end
				if status
					if params[:ps_id].strip == ""
						status = false
						error = "Publish Settings ID cannot be empty!"
					end
				end
			else
				if status
					status,error = validate_ott_media_params(params)
				end	
			end
			if status
				media = Media.find(params[:media_id])
				# media.title = params[:title]
				if media.status=="Created"
					source_type = params[:source_type]
					if source_type == "uploadvideo"
						source_type = "HTTP"
					else
						source_type
					end
					media.source_type = source_type
					media.source_url = params[:source_url]
					media.publish_settings_id = params[:ps_id]
					if (!params[:priority]) || (params[:priority] == "")
						media.priority = "low"
					else
						media.priority = params[:priority]
					end
				end
				media.duration_string = params[:duration_string]
				media.content_owner_id = params[:content_owner]
				media.cost_per_hour = params[:cost_per_hour]
				media.currency = params[:currency]
				media.censor_rating = params[:censor_rating]
				media.save
				if BusinessGroup.find(media.bg_id).plan=="transcoding" #auto-transcode if non-ott bg
					status,error = media.transcode_request
			    else
			        metadata = extract_metadata_from_params(params)
			        status,error = media.update_at_ott(metadata)		
				end
			end
			if status
				#delete existing delayed jobs if presents
				if media.ott_publish_req_job_id
					Delayed::Job.where(:id => media.ott_publish_req_job_id).delete_all
					media.ott_publish_req_job_id = nil
				end	

				if media.ott_unpublish_req_job_id
					Delayed::Job.where(:id => media.ott_unpublish_req_job_id).delete_all
					media.ott_unpublish_req_job_id = nil
				end

				#if status is true then check publish and unpublish date and run delayed	
			   	if !metadata["published_date"].blank?
			   		media.add_media_in_publish_queue(metadata)
			   	end
			   	if !metadata["unpublished_date"].blank?
			   		media.add_media_in_unpublish_queue(metadata)
			   	end
			   	media.save
				render json: {} , status: :ok
			else
				render :json => {:error => error}.to_json, :status => 500
			end
		elsif params[:method] == "delete"
			status = true
			m = Media.find(params[:media_id])
			if m
			  if m.ingest_req_job_id
				if Delayed::Job.where(:id => m.ingest_req_job_id).count > 0
					dj = Delayed::Job.find(m.ingest_req_job_id)
					dj.destroy
				end
			  end
			  if m.publish_req_job_id
				if Delayed::Job.where(:id => m.publish_req_job_id).count > 0
					dj = Delayed::Job.find(m.publish_req_job_id)
					dj.destroy
				end
			  end

			  #delete existing delayed jobs if presents
				if m.ott_publish_req_job_id
					Delayed::Job.where(:id => m.ott_publish_req_job_id).delete_all
				end	

				if m.ott_unpublish_req_job_id
					Delayed::Job.where(:id => m.ott_unpublish_req_job_id).delete_all
				end
			  
			  if m.ott_id
			  	status,error = m.delete_at_ott
			  end
			  
			  if status
				if m.platform_media_id
				  status,error = m.delete_at_tx
				end

				if status
				  m.is_removed = true
				  m.delete_user_id = current_user.id
				  m.save
				  render json: {} , status: :ok
				else
				  render :json => {:error => "Delete action failed!"}.to_json, :status => 500	  	
				end	
			  else
			    render :json => {:error => "Delete action failed!"}.to_json, :status => 500	
			  end	  
			else
			  render :json => {:error => "Media Not Found!!"}.to_json, :status => 500
			end	
		elsif params[:method] == "update_priority"
			media = Media.find(params[:media_id])
			status = true
			error = ""
			if !Media.find_by(:id => params[:media_id])
				status = false
				error = "Invalid Media ID"
			end
			if status && params[:priority]
				if media.status == "Created"
					media.priority = params[:priority]
					media.save
				elsif media.status == "Ingesting"
					media.priority = params[:priority]
					media.save
				else
					status,error = media.update_priority(params[:priority])
				end
			end
			if status
				render json: {} , status: :ok
			else
				render :json => {:error => "Update action failed!"}.to_json, :status => 500
			end
		elsif params[:method] == "transcode"
			status = true
			error = ""
			media = nil
			if !Media.find_by(:id => params[:media_id])
				status = false
				error = "Invalid Media ID"
			else
				media = Media.find_by(:id => params[:media_id])
			end
			if status
				status,error = Media.new.validate_media_for_transcoding(media)
			end
			if status
				status,error = media.transcode_request
			end
			if status
				flash[:notice] = "An Email will be sent to your Email once the media is published!"
				render json: {} , status: :ok
			else
				render :json => {:error => error}.to_json, :status => 500
			end
		elsif params[:method] == "publish_at_ott"
			status = true
			error = ""
			media = nil
			if !Media.find_by(:id => params[:media_id])
				status = false
				error = "Invalid Media ID"
			else
				media = Media.find_by(:id => params[:media_id])
			end
			if media

				 #delete existing delayed jobs if presents
				if media.ott_publish_req_job_id
					Delayed::Job.where(:id => media.ott_publish_req_job_id).delete_all
				end	

	            # if media.status == "Created Externally" || media.status == "Published"
	            if media.status == "Created Externally" || media.status == "published"
	            	status,error = media.publish_at_ott
	            else
	            	status = false
	                error = "Smart URL is not available!"	
	            end
        	end
			if status
				render json: {} , status: :ok
			else
				render :json => {:error => error}.to_json, :status => 500
			end
		elsif params[:method] == "unpublish_at_ott"
			status = true
			error = ""
			media = nil
			if !Media.find_by(:id => params[:media_id])
				status = false
				error = "Invalid Media ID"
			else
				media = Media.find_by(:id => params[:media_id])
			end
			if status
				 #delete existing delayed jobs if presents
				if media.ott_unpublish_req_job_id
					Delayed::Job.where(:id => media.ott_unpublish_req_job_id).delete_all
				end	
				status,error = media.unpublish_at_ott
			end
			if status
				render json: {} , status: :ok
			else
				render :json => {:error => error}.to_json, :status => 500
			end
		elsif params[:method] == "republish_media"
			status = true
			error = ""
			bg = current_bg
			media = nil
			if !Media.find_by(:id => params[:media_id])
				status = false
				error = "Invalid Media ID"
			else
				media = Media.find_by(:id => params[:media_id])
			end

			if status
				data = {}
				data["auth_token"] = bg.tx_auth_token
				data["publish_videos"] = {}	
				data["publish_videos"]["action"] = "republish_full"
            	url = "http://"+bg.tx_base_url+"/publish_videos/"+media.publisher_id
            	response = http_request("put",data,url,60)
            	if response["code"] == 200
					media.status = "Processing Request"
		            		media.save
					render json: {} , status: :ok
				else
					render :json => {:error => error}.to_json, :status => 500
				end
            end
		else
			render :json => {:error => "Invalid action!"}.to_json, :status => 500
		end
	end
	
	def fetch
		m = Media.find(params[:media_id])
		if m
			media = {}
			media["title"] = m.title
			media["_id"] = m.id
			if user = User.find_by(:id=> m.user_id)
				media["user_email"] = user.email
			else
				media["user_email"] = nil
			end
			if user = User.find_by(:id=> m.transcoding_user_id)
				media["transcoding_user_email"] = user.email
			else
				media["transcoding_user_email"] = nil
			end
			if user = User.find_by(:id=> m.publish_user_id)
				media["publish_user_email"] = user.email
			else
				media["publish_user_email"] = nil
			end
			media["source_url"] = m.source_url
			media["created_at"] = m.created_at.in_time_zone.strftime("%I:%M%p on %A, %d/%m/%Y")
			if m.ingested_at
				media["ingested_at"] = m.ingested_at.in_time_zone.strftime("%I:%M%p on %A, %d/%m/%Y")
			end
			media["platform_media_id"] = m.platform_media_id
			media["publisher_id"] = m.publisher_id
			media["smart_url"] = m.smart_url
			media["input_info"] = m.input_info
			if m.published_at
				media["published_at"] = m.published_at.in_time_zone.strftime("%I:%M%p on %A, %d/%m/%Y")
			end
			media["output_info"] = m.output_info
			media["sprite_url"] = m.sprite_url
			media["status"] = m.status
			render json: {:media => media} , status: :ok
		else
			render :json => {:error => "Fetch action failed!"}.to_json, :status => 500
		end
	end
	def play
		media = Media.find(params[:media_id])
		bg = BusinessGroup.find(media.bg_id)
		smart_url_key = bg.tx_auth_token
		# smart_url_key = "ywVXaTzycwZ8agEs3ujx"
		header = {"Accept" => "application/json", "Cache-Control" => "no-cache",  "User-Agent" => "Mozilla/5.0 (iPad; CPU OS 5_0 like Mac OS X) AppleWebKit/534.46 (KHTML, like Gecko) Version/5.1 Mobile/9A334 Safari/7534.48.3"}
		
		if bg.plan != "transcoding" && media.status == "Created Externally"
			catalog = Catalog.find_by(:ott_id => media.ott_catalog_id)
            case catalog.theme
            when "show"	
            	#url = "http://"+get_ott_base_url+"/catalogs/"+media.ott_catalog_id+"/episodes/"+media.ott_id+"?auth_token="+bg.ott_auth_token+"&status=any&region=any"
            	mediaid = Media.find_by(:id => params[:media_id])
            	show_data = ShowTheme.find_by(:id => mediaid.show_id) || ShowTheme.find_by(:ott_id => mediaid.show_id)
            	if show_data.nil?
            		subcategory_data = Subcategory.find_by(:id => mediaid.subcategory_id)
            		url = "http://"+get_ott_base_url+"/catalogs/shows/subcategories/"+subcategory_data.ott_id+"/episodes/"+media.ott_id+"?auth_token="+bg.ott_auth_token+"&status=any&region=any"
            	else
            		url = "http://"+get_ott_base_url+"/catalogs/shows/"+show_data.ott_id+"/episodes/"+media.ott_id+"?auth_token="+bg.ott_auth_token+"&status=any&region=any"
            	end
            else
            if 	media.videolist == true
            	url = "http://"+get_ott_base_url+"/catalogs/"+media.ott_catalog_id+"/videolists?auth_token="+bg.ott_auth_token+"&status=any&region=IN"
            else
            	url = "http://"+get_ott_base_url+"/catalogs/"+media.ott_catalog_id+"/items/"+media.ott_id+"?auth_token="+bg.ott_auth_token+"&status=any&region=any"
            end
            end
            resp = http_request("get",{},url,60)
            if resp["code"] == 200
             if media.videolist == true
               videodata = resp["body"]["video_list"]
               videodata.each do |vli|
               	if media.ott_id == vli["videolist_id"]
               		surl = vli["play_url"]["saranyu"]["url"]
               	end
           	   end
           	 else
               data = resp["body"]["data"]
               if data["play_url"].has_key? "saranyu"
               	   surl = data["play_url"]["saranyu"]["url"]	  
              else
                   render :json => {:error => "Saranyu Smart URL is not available!"}.to_json, :status => 500 and return	    
               end
              end
            else
               render :json => {:error => "Temporarily Media Details Are Not Available!"}.to_json, :status => 500 and return	
            end	
		end

		if bg.plan != "transcoding" && media.status == "Published"
			surl = media.smart_url
		end	

		if bg.plan == "transcoding"
			surl = media.smart_url
		end
		split_url = surl.split('/')
		if split_url.include?"v2"
			v2= true
		else
			v2= false
		end

		signed_url = sign_smart_url(smart_url_key,"#{surl}?service_id=6&play_url=yes&protocol=hls&us=")
		response = Typhoeus::Request.get(signed_url, headers: header)
		if response.code == 200
	    	urls = JSON.parse(response.body)
	    	@playback_profiles = []
	    	@playback_urls = []
	    	@sprite_url = ""
	    	@adaptive_url = ""
	    	@adaptive_url_main = ""
	    	
	    	
	    	if v2
	    		urls["playback_urls"].each do |url|
	    			if url["profile"]
		    			@playback_profiles << url["profile"]
		    		end
		    		if url["playback_url"]
		    			@playback_urls << url["playback_url"]
		    		end
		    	end
		    	
		    	urls["adaptive_urls"].each do |url|	
		    	if url["playback_url"]
		    		@adaptive_url = url["playback_url"]
		    		@adaptive_url_main = url["playback_url"]
		    	end
		   	 end

		    	if urls["sprite"]["sprite_url"]
		    			@sprite_url = urls["sprite"]["sprite_url"]
		    	end

	    	else
		    	urls.each do |url|
		    		if url["profile"]
		    			@playback_profiles << url["profile"]
		    		end
		    		if url["playback_url"]
		    			@playback_urls << url["playback_url"]
		    		end
		    		if url["sprite_url"]
		    			@sprite_url = url["sprite_url"]
		    		end
		    		if url["adaptive_url"]
		    			@adaptive_url = url["adaptive_url"]
		    		end

		    		if url["adaptive_url_main"]
		    			@adaptive_url_main = url["adaptive_url_main"]
		    		end	

		    	end
		    	if !@adaptive_url_main.empty?
		    		@adaptive_url = @adaptive_url_main
		    	end	
	    	end
			#@embed_code = JSON.parse(response.body)["embed link"]
	    	render partial: "media/player"
	    else
	    	render :json => {:error => "Smart URL Signing Failed!"}.to_json, :status => 500
	    end
	end
	def ingest_noti_handler
		if params["notification"]["status"] == 202
			media = Media.where(:is_removed=>false).find(params[:media_id])
			media.status = "Ingesting"
			media.wait_till = nil
			media.save
		elsif params["notification"]["status"] == 200
			media = Media.where(:is_removed=>false).find(params[:media_id])
			media.status = "Ingested"
			media.ingested_at = Time.now
			media.save
			bg = BusinessGroup.find(media.bg_id)
			header = {"Accept" => "application/json", "Cache-Control" => "no-cache",  "User-Agent" => "Mozilla/5.0 (iPad; CPU OS 5_0 like Mac OS X) AppleWebKit/534.46 (KHTML, like Gecko) Version/5.1 Mobile/9A334 Safari/7534.48.3"}
			url = "http://#{bg.tx_base_url}/media_files/#{media.platform_media_id}?auth_token=#{bg.tx_auth_token}"
			response = Typhoeus::Request.get(url, headers: header)
			if response.code == 200
				input_info = {}
				ret = JSON.parse(response.body)
				input_info["file_type"] = ret[""]["media_info"]["file_type"]
				input_info["acodec"] = ret[""]["media_info"]["acodec"]
				input_info["vcodec"] = ret[""]["media_info"]["vcodec"]
				input_info["duration"] = ret[""]["media_info"]["duration"]
				input_info["width"] = ret[""]["media_info"]["vwidth"]
				input_info["height"] = ret[""]["media_info"]["vheight"]
				media.input_info = input_info
				media.save
			end
			media.publish_req_job_id = Delayed::Job.enqueue(PublishMedia.new(media), :run_at => Time.now, :queue => "#{Rails.env}_cms").id
			media.save
		else
			media = Media.where(:is_removed=>false).find(params[:media_id])
			media.status = "Ingestion Error"
			media.save
		end
		render json: {} , status: :ok
	end
	def publish_noti_handler
		if params["notification"]["status"] == 201
			media = Media.where(:is_removed=>false).find(params[:media_id])
			media.status = "Transcoding"
			media.wait_till = nil
			media.save
		elsif params["notification"]["status"] == 200
			media = Media.where(:is_removed=>false).find(params[:media_id])
			media.status = "Published"
			media.published_at = Time.now
			media.save
			if media.bulk_upload_id
				bulkupload = BulkUpload.find(media.bulk_upload_id)
				bulkupload.published_count = bulkupload.published_count + 1
				bulkupload.save
				if bulkupload.mail_option == "never"
				elsif bulkupload.mail_option == "after_each"
					Delayed::Job.enqueue(SendPublishedMail.new(User.find(media.user_id),media), :run_at => Time.now, :queue => "#{Rails.env}_cms")
				elsif bulkupload.mail_option == "after_last"
					if bulkupload.published_count == bulkupload.total_count
						Delayed::Job.enqueue(SendPublishedMail.new(User.find(media.user_id),media), :run_at => Time.now, :queue => "#{Rails.env}_cms")
					end
				end
			else
				Delayed::Job.enqueue(SendPublishedMail.new(User.find(media.user_id),media), :run_at => Time.now, :queue => "#{Rails.env}_cms")
			end
			bg = BusinessGroup.find(media.bg_id)
			smart_url_key = bg.tx_auth_token
			header = {"Accept" => "application/json", "Cache-Control" => "no-cache",  "User-Agent" => "Mozilla/5.0 (iPad; CPU OS 5_0 like Mac OS X) AppleWebKit/534.46 (KHTML, like Gecko) Version/5.1 Mobile/9A334 Safari/7534.48.3"}
			surl = media.smart_url
			signed_url = sign_smart_url(smart_url_key,"#{surl}?service_id=6&play_url=yes&protocol=hls&us=")
			response = Typhoeus::Request.get(signed_url, headers: header)
			if response.code == 200
		    	urls = JSON.parse(response.body)
		    	urls.each do |url|
		    		if url["sprite_url"]
		    			media.sprite_url = url["sprite_url"]
		    			media.save
		    		end
		    	end
		    end
		else
			media = Media.where(:is_removed=>false).find(params[:media_id])
			media.status = "Transcoding Error"
			media.save
		end
		render json: {} , status: :ok
	end

	def bulk_upload
		status = true
		error = ""
		if status
			if !params[:excel_file_path]
				status = false
				error = "No file selected for upload!"
			end
			if status
				if !params[:excel_file_path].content_type.include? ".sheet"
					status = false
					error = "Please provide valid file format(xslx)!"
				end	
			end
		end
		if status
			bg = current_bg
			upload_file = params[:excel_file_path]
			#write file to local disk
	  		name =  upload_file.original_filename
			directory = Rails.root.join("public/excel_files")
			path = File.join(directory, name)
			File.open(path, "wb") { |f| f.write(upload_file.read) }
			#parse the excel sheet
			media_list = excel_to_hash(path)

			#check bulk upload for billable items
			params[:bg_object] = bg
			params[:entity] = "media"
			params[:bg_plan] = @bg_plan
			params[:is_bulk_upload] = "yes"
			params[:bulk_upload_count] = media_list.count
			status,message = track_entity_count(params)
			if status
				
			    media_list.each_with_index do |media_row,index|
			    	# if params[:validate] == "yes"
			    	# 	params[:validate] = true
			    		#validate all data
					    # if status
					    # 	status,error = validate_media_row_for_ott_saas(media_row,index+1) 
					    # end
					# else 
					    #validate only mandatory fields
					    if status
					    	# params[:validate] = false
 					    	status,error = validate_only_mandatory_params(media_row,index+1)
					    end    
					# end    

				end

				if status   
					status = check_for_identical_catalog(media_list) 	
					if !status
						status = false
						error = "Please provide indentical catalog id!!"
					else
					    catalog_id = media_list.map { |h| h["Catalog Id"] }.uniq.last	
						catalog = Catalog.new.get_catalog_by_id(catalog_id)
						if catalog
							url = "http://"+get_ott_base_url+"/catalogs/"+catalog["ott_id"]+"?auth_token="+bg.ott_auth_token
	    					response = http_request("get",{},url,60)
	    					if response["code"] == 200
					    		catalog_info = response["body"]["data"] 
					    		params[:catalog_ott_obj] = catalog_info
					    	end
					    else
					    	status = false
					        error = "Please provide valid Catalog Id"	
					    end	
					end	
				end	
			else
			    error = message	
			end	   
		end

	    
		

	   	##No Extra key support for ott_saas users
	   	
	   	if status
	   		ps_list = []	
		    PublishSettings.where(:bg_id=>current_bg.id).each do |ps|
				ps_list << ps.attributes
	    	end
		   	media_params = {}
		   	bulkupload = BulkUpload.new
			bulkupload.excel_file_path = path
			bulkupload.mail_option = params[:mail_option]
			bulkupload.bg_id = current_bg.id
			bulkupload.user_id = current_user.id
			bulkupload.save
			#creating ID appended excel sheet filename
			justfilename = File.basename(path)
			justdirpath = File.dirname(path)
			new_name = justdirpath+"/"+bulkupload.id+"_"+justfilename
			system("sudo mv "+path+" "+new_name)
			bulkupload.excel_file_path = new_name
			bulkupload.total_count = 0
			bulkupload.published_count = 0
			bulkupload.save
	    end
		#build media list


		if status && (bg.plan == "ott_saas")
			status,error = validate_data_at_cms(media_list,"bulk_upload")
			if status
				flag = 0
				i = 2
				media_list.each do |media_row|
					source_url_present = 0
			    	media_params["title"] = media_row["Title"]
					
					media_params["priority"] = "low"
					if media_row["Priority"]
						media_params["priority"] = media_row["Priority"]
					end

					if media_row.key? "Source URL" 
						if !media_row["Source URL"].blank?
							media_params["source_type"] = "FTP"
					    	media_params["source_url"] = fix_multiple_slashes(media_row["Source URL"]) 
					    	#media_params["source_url"] = media_row["Source URL"]
					    	media_params["status"] = "Created"
					    	media_params["smart_url"] = nil
					    	media_params["imgoptradio"] = !media_row["Image Extraction Flag"].blank? ? media_row["Image Extraction Flag"] : "no"
					    	source_url_present = 1 		
					    end	
					end

					if source_url_present == 0
						media_params["source_url"] = nil
						# media_params["smart_url"] = media_row["Smart URL"]
						media_params["smart_url"] = nil
						media_params["imgoptradio"] = "no"
						media_params["source_type"] = "external"
						media_params["status"] = "Created Externally"
					end
					
					media_params["notification_url"] = media_row["Notification URL"]
					media_params["user_id"] = current_user.id
					media_params["user_role"] = current_user.roles.first
					media_params["publish_settings_id"] = ps_list.first["_id"]
					media_params["bg_id"] = current_bg.id
					
					media_params["ott_status"] = "edit"
					media_params["is_removed"] = false
					media_params["asset_id"] = media_row["Asset Id"] if media_row["Asset Id"]
					media_params["bulk_upload_id"] = bulkupload.id

					#if show is present then assign episode to show
					if !media_row["Show Id"].blank?
						parent_id = media_row["Show Id"]
						media_params["show_id"] = media_row["Show Id"]
					else
					#if subcategory is present then assign episode to subcategory
						parent_id = media_row["Subcategory Id"]	
						media_params["subcategory_id"] = media_row["Subcategory Id"]
					end	

					#media_params["metadata"] = extract_metadata_from_row_for_ott(media_row)
					media_params["custom_keys"] = {}
					bulkupload.total_count = bulkupload.total_count+1
					bulkupload.save
					
					media = create_media(media_params)
					# catalog_id = media_row["Catalog Id"]
					# catalog = Catalog.new.get_catalog_by_id(catalog_id)
					media.ott_catalog_id = catalog.ott_id
					if media.save
						params[:type] = "bulk_upload"
						data = extract_metadata_from_row_for_ott(media_row,bulkupload,params)
						metadata = extract_metadata_from_params_for_bulk_upload(data,bg)

						status,error = media.create_at_ott(metadata,catalog,parent_id)
						if !status
						   #delete all media from ott and cms db 	
						   remove_data_from_ott(bulkupload,catalog,bg)	
						   remove_data_from_cms(bulkupload)	
						   #make bulkupload status failed
						   error = "#{error} at row number #{i}!"
						   flag = 1
						end	
				    end
				    if flag == 1
				    	break
				    end	
				    i = i + 1
				end	
			end	
	   	end

	    #media addition
	    if status
			flash[:notice] = "All media in the bulk upload sheet has been added to the queue!"
		else
			flash[:error] = error
		end
		redirect_to "/media/view_all"
	end

	def fetch_all
		status = true
		error = ""
		media_list = ""
		video_list = params[:video_list]
		parent_media_id = params[:parent_media_id]
		@bg = current_bg
		if params[:page].nil?
				page = 0
				next_page = page + 1
			else
				page = params[:page]
				next_page = page.to_i + 1
			end
			page_size = 2
			
		catalog_id = params[:catalog_id]
		if ((current_user.roles.include?"cms_manager") || (current_user.roles.include?"admin") || (current_user.roles.include?"content_editor"))
			if params[:search]
				if catalog_id.nil? 
				    if video_list == "yes" 
						media_list = Media.where(:title=>/#{params[:search]}/i).where(:bg_id=>@bg.id).where(:is_removed => false, :videolist => true, :parent_media_id => parent_media_id).order_by(:created_at => 'desc').page(page).per(page_size)
					else
						media_list = Media.where(:title=>/#{params[:search]}/i).where(:bg_id=>@bg.id).where(:is_removed => false, :videolist => false).order_by(:created_at => 'desc').page(page).per(page_size)
					end
				else
					if video_list == "yes"
						media_list = Media.where(:title=>/#{params[:search]}/i).where(:ott_catalog_id=>catalog_id).where(:is_removed => false, :videolist => true, :parent_media_id => parent_media_id).order_by(:created_at => 'desc').page(page).per(page_size)
					else
						media_list = Media.where(:title=>/#{params[:search]}/i).where(:ott_catalog_id=>catalog_id).where(:is_removed => false, :videolist => false).order_by(:created_at => 'desc').page(page).per(page_size)
					end
				end
			else
				if catalog_id.nil?
					if video_list == "yes"
						media_list = Media.where(:bg_id=>@bg.id).where(:is_removed => false, :videolist => true, :parent_media_id => parent_media_id).order_by(:created_at => 'desc').page(page).per(page_size)
					else
						media_list = Media.where(:bg_id=>@bg.id).where(:is_removed => false, :videolist => false).order_by(:created_at => 'desc').page(page).per(page_size)
					end
				else
					if video_list == "yes"
						media_list = Media.where(:ott_catalog_id=>catalog_id).where(:is_removed => false, :videolist => true, :parent_media_id => parent_media_id).order_by(:created_at => 'desc').page(page).per(page_size)
					else
						media_list = Media.where(:ott_catalog_id=>catalog_id).where(:is_removed => false, :videolist => false).order_by(:created_at => 'desc').page(page).per(page_size)
					end
				end
			end
		elsif current_user.roles.include?"publisher"
			if params[:search]
				if catalog_id.nil?
				media_list = Media.where(:title=>/#{params[:search]}/i).where(:bg_id=>@bg.id)
			else
				media_list = Media.where(:title=>/#{params[:search]}/i).where(:ott_catalog_id=>catalog_id)
			end
			else
				if catalog_id.nil?
				media_list = Media.where(:bg_id=>@bg.id)
			else
				media_list = Media.where(:ott_catalog_id=>catalog_id)
			end
			end
			if current_user.has_public_access
				if video_list == "yes"
				media_list = media_list.where(:is_removed => false, :videolist => true, :parent_media_id => parent_media_id).order_by(:created_at => 'desc').page(page).per(page_size)
				else
				media_list = media_list.where(:is_removed => false, :videolist => false).order_by(:created_at => 'desc').page(page).per(page_size)
				end
			else
				if video_list == "yes"
				media_list = media_list.any_of({user_id: current_user.id},{user_role: "content_editor"}).where(:is_removed => false, :videolist => true, :parent_media_id => parent_media_id).order_by(:created_at => 'desc').page(page).per(page_size)
				else
				media_list = media_list.any_of({user_id: current_user.id},{user_role: "content_editor"}).where(:is_removed => false, :videolist => false).order_by(:created_at => 'desc').page(page).per(page_size)
				end
			end
		else
			status = false
			error = "authorization failed"
		end
		if status
			render json: {:list => media_list, :total_count => media_list.total_count, :total_pages => media_list.total_pages} , status: :ok
		else
			render :json => {:error => error}.to_json, :status => 500
		end
	end

	def view_all
		@bg = current_bg
		if BusinessGroup.find_by(:id => @bg.id).name == "Trial"
			flash[:notice] = "Please note that Trial Users are limited by a maximum duration of 1 minute of transcoding and only to a single profile by default!"
		end
		@tenant = params[:tenant]
	end
	def new
		status = true
		@bg = current_bg
        @catalog_id = params[:catalog_id]
        @show_id = params[:show_id]
        @subcategory_id = params[:subcategory_id]
		@ps_list_single = []
		@ott_base_url = get_ott_base_url
		PublishSettings.where(:bg_id => @bg.id).order_by(:created_at => 'desc').each do |ps|
			disqualify = false
			if ps.naming_tag
				if has_custom_keys(ps.naming_tag)
					disqualify = true
				end
			end
			if extra_keys_required_for_storage(ps.cdn.first).count > 0
				disqualify = true
			end
			if !disqualify
				@ps_list_single << ps
			end
		end

		if @bg.plan != "transcoding"
			# @catalog_list = []
			# Catalog.where(:bg_id => @bg.id).not_in(:theme => [ "access_control","subscription","person","article","livetv"]).each do |cata|
			# 	h = {}
			#  	h["name"] = cata.name+"["+cata.theme+"]"
			#  	h["id"] = cata.id
			#  	h["tenant_id"] = cata.tenant_id
			#  	h["bg_id"] = cata.bg_id
			# 	@catalog_list << h
			# end

			if params[:show_id]
				@show_data = ShowTheme.find_by(:id => params[:show_id]) || ShowTheme.find_by(:ott_id => params[:show_id])
				@catalog_id = @show_data.catalog_id

				url = "http://" + get_ott_base_url + "/catalogs/" + @show_data.ott_catalog_id + "/items/" + @show_data.ott_id + "?region=IN&auth_token=" + @bg.ott_auth_token + "&status=any"
				response = http_request("get",{},url,60)
				@show_resp = response["body"]["data"]
			end

			if params[:subcategory_id]
				@subcategory_data = Subcategory.find_by(:id => params[:subcategory_id])
				@catalog_id = @subcategory_data.catalog_id
				url = "http://" + get_ott_base_url + "/catalogs/shows/subcategories/" + @subcategory_data["ott_id"] +"?auth_token=" + @bg.ott_auth_token + "&region=any&status=any"
				response = http_request("get",{},url,60)
				@subcategory_resp = response["body"]["data"]
			end
			@catalog = Catalog.find_by(:id => @catalog_id)
			@catalogtheme = @catalog.theme
			# tvod_url = "http://"+get_ott_base_url+"/catalogs/"+@catalog.ott_id+"/items/?auth_token="+ @bg.ott_auth_token + "&region=any"
			tvod_url = "http://54.165.33.205:10000/catalogs/590fffda39ac6e6ef10005d9/items/?auth_token=dz25UevHcFq72awidzMx&region=US"
			@tvod_response = http_request("get",{},tvod_url,60)
			@tvod_plans = @tvod_response["body"]["data"]["items"].select{|r| r["category"] == "tvod"}
			@tvod_plans = @tvod_plans.empty? ? "" : @tvod_plans.first["plans"] 
			if params['tvod_region'].present?
				@tvod_plan = @tvod_plans.select{|r| r['id'] == params['tvod_id'] }
				a = @tvod_plan.first['region'].select{|r| r["region"].include? params["tvod_region"]}
				tvod_currency = a.first["price"] +" "+ a.first["currency_symbol"]
				tvod_currency_type = a.first["currency"]
				tvod_duration = @tvod_plan.first["duration"] + " " + @tvod_plan.first["period"]
				render :json => {:tvod_currency => tvod_currency, tvod_currency_type: tvod_currency_type, tvod_duration: tvod_duration}.to_json
			end
			if params['tvod_id'].present? && !params['tvod_region'].present?
				@tvod_plan = @tvod_plans.select{|r| r['id'] == params['tvod_id'] }
				regions = []
				@tvod_plan.first["region"].each do |r|
					regions << r["region"].collect{|j|  a = j + " - "+r["price"]+ " " +r["currency"] + ", "+@tvod_plan.first['duration'] + @tvod_plan.first['period']}
					# a = r["region"] + "-"+ r['price'] +" " +r[currency] + ", "+@tvod_plan.first['duration'] + @tvod_plan.first['period'] }
				end
				regions = regions.flatten.compact
				render :json => {:tvod_region => regions}.to_json
			end

			

			url = "http://"+get_ott_base_url+"/catalogs/"+@catalog.ott_id+"?auth_token="+@bg.ott_auth_token
			response = http_request("get",{},url,60)
			if !response["status"]
				status = response["status"]
				error = response["error"]
			end


			if status
			    @catalog = response["body"]["data"]	

			    @t_ott_id = response["body"]["data"]["tenant_ids"]
			    @videolist_tags = response["body"]["data"]["videolist_tags"]

			    @tenant_list = []
			    @accesscontrol = []
			    @valvideolist_tags = []

			    # @videolist_tags.each do |vlt|
			    # 	@valvideolist_tags << vlt
			    # end

			    @t_ott_id.each do |obj|
			  	  Tenant.where(:ott_id=>obj).each do |tenan|
				  	
				  	h = {}
				 	h["id"] = tenan.id
				 	h["name"] = tenan.name
				 	h["ott_id"] = tenan.ott_id

					h ["genres"] =[]
					response["body"]["data"]["genres"].keys.each do |g|
			          h["genres"] = (response["body"]["data"]["genres"][tenan.ott_id]).map{|gen| gen["name"] } if g == h["ott_id"]
					end

					h ["languages"] =[]
					response["body"]["data"]["languages"].keys.each do |l|
					  h ["languages"] = (response["body"]["data"]["languages"][tenan.ott_id]).map{|lan| lan["name"] } if l == h["ott_id"]
					end 
			    	
				    h["applications"] = []
				    App.where(:tenant_id => tenan.id).each do |tapp|
				        if @catalog["application_ids"].include? tapp.ott_id	
				            application = {}
				            application["a_id"] = tapp.id
						    application["a_name"] = tapp.name
						    application["a_tid"] = tapp.tenant_id
						    h['applications'] << application
					    end
				    end

			 		@tenant_list << h

			 		catalogs = Catalog.find_by(:bg_id => @bg.id, :theme => "access_control", :tenant_id => tenan.id.to_s)
			 		acurl = "http://"+get_ott_base_url+"/catalogs/"+catalogs.ott_id+"/items?auth_token="+@bg.ott_auth_token+"&status=any&region=any"
					acresponse = http_request("get",{},acurl,60)
					if acresponse["code"] == 200
					accesscontrols = acresponse["body"]["data"]["items"]

					accesscontrols.each do |acc|
						ac = {}
						ac["name"] = tenan.name
						ac["title"] = acc["title"]
						ac["content_id"] = acc["content_id"]
						@accesscontrol << ac
					end
					end  #end of acc if
			      end
		        end
             end	

			url = "http://" + get_ott_base_url + "/people?auth_token=" + current_bg.ott_auth_token
			response = http_request("get",{},url,60)
			if response["code"] == 200
				@people = response["body"]["data"]["items"]
				@total_people_count = response["body"]["data"]["total_people_count"]
			end
		end	


		# Tenant.where(:bg_id=>@bg.id).each do |tenant|
		# 	Catalog.where(:tenant_id=>tenant.id.to_s).each do |catalog|
		# 		h = {}
		# 		h["name"] = tenant.name+" - "+catalog.name+"  ["+catalog.theme+"]"
		# 		h["id"] = catalog.id
		# 		h["tenant_id"] = tenant.id
		# 		@catalog_list << h
		# 	end
		# end
	end
	def edit
		@bg = current_bg
		if @bg.plan == "transcoding"
			@ps_list_single = []
			PublishSettings.where(:bg_id => @bg.id).order_by(:created_at => 'desc').each do |ps|
				disqualify = false
				if ps.naming_tag
					if has_custom_keys(ps.naming_tag)
						disqualify = true
					end
				end
				if extra_keys_required_for_storage(ps.cdn.first).count > 0
					disqualify = true
				end
				if !disqualify
					@ps_list_single << ps
				end
			end
			@edit_media = Media.find(params[:media_id])
			if @edit_media.status == "Created"
				@hide_transcode_data = false
			else
				@hide_transcode_data = true
			end
			@catalog_list = []
			Tenant.where(:bg_id=>@bg.id).each do |tenant|
				Catalog.where(:tenant_id=>tenant.id.to_s).each do |catalog|
					h = {}
					h["name"] = tenant.name+" - "+catalog.name
					h["id"] = catalog.id
					h["tenant_id"] = tenant.id
					@catalog_list << h
				end
			end
			render "new"
	    else
	    	@ps_list_single = []
			PublishSettings.where(:bg_id => @bg.id).order_by(:created_at => 'desc').each do |ps|
				disqualify = false
				if ps.naming_tag
					if has_custom_keys(ps.naming_tag)
						disqualify = true
					end
				end
				if extra_keys_required_for_storage(ps.cdn.first).count > 0
					disqualify = true
				end
				if !disqualify
					@ps_list_single << ps
				end
			end
			@edit_media = Media.find(params[:media_id]) || Media.find_by(:ott_id => params[:media_id]) || Media.find_by(:parent_media_id => params[:media_id])
			# if @edit_media.status == "Created"
			# 	@hide_transcode_data = false
			# else
			# 	@hide_transcode_data = true
			# end
			@hide_transcode_data = true
			# tvod_url = "http://54.165.33.205:10000/catalogs/590fffda39ac6e6ef10005d9/items/5a5459ede859403c5a000003?auth_token=dz25UevHcFq72awidzMx&region=US"
			# @tvod_response = http_request("get",{},tvod_url,60)
			# @tvod_plans = @tvod_response["body"]["data"]["plans"]
			
			catalog = Catalog.find_by(:ott_id => @edit_media.ott_catalog_id)
			# tvod_url = "http://"+get_ott_base_url+"/catalogs/"+catalog.ott_id+"/items/?auth_token="+ @bg.ott_auth_token + "&region=any"
			tvod_url = "http://54.165.33.205:10000/catalogs/590fffda39ac6e6ef10005d9/items/?auth_token=dz25UevHcFq72awidzMx&region=US"
			@tvod_response = http_request("get",{},tvod_url,60)
			@tvod_plans = @tvod_response["body"]["data"]["items"].select{|r| r["category"] == "tvod"}
			@tvod_plans = @tvod_plans.empty? ? "" : @tvod_plans.first["plans"] 
			if params['tvod_region'].present?
				@tvod_plan = @tvod_plans.select{|r| r['id'] == params['tvod_id'] }
				a = @tvod_plan.first['region'].select{|r| r["region"].include? params["tvod_region"]}
				tvod_currency = a.first["price"] +" "+ a.first["currency_symbol"]
				tvod_currency_type = a.first["currency"]
				tvod_duration = @tvod_plan.first["duration"] + " " + @tvod_plan.first["period"]
				render :json => {:tvod_currency => tvod_currency, tvod_currency_type: tvod_currency_type, tvod_duration: tvod_duration}.to_json
				return
			end
			if params['tvod_id'].present? && !params['tvod_region'].present?
				@tvod_plan = @tvod_plans.select{|r| r['id'] == params['tvod_id'] }
				regions = []
				@tvod_plan.first["region"].each do |r|
					regions << r["region"].collect{|j|  a = j + " - "+r["price"]+ " " +r["currency"] + ", "+@tvod_plan.first['duration'] + @tvod_plan.first['period']}
					# a = r["region"] + "-"+ r['price'] +" " +r[currency] + ", "+@tvod_plan.first['duration'] + @tvod_plan.first['period'] }
				end
				# regions << @tvod_plan.first["region"].collect{|r|  r["region"]}
				# regions = regions.flatten.compact
				render :json => {:tvod_region => regions}.to_json
				return
			end

            case catalog.theme
            when "show"	
            	#url = "http://"+get_ott_base_url+"/catalogs/"+@edit_media.ott_catalog_id+"/episodes/"+@edit_media.ott_id+"?auth_token="+@bg.ott_auth_token+"&status=any&region=any"
            	mediaid = Media.find_by(:id => params[:media_id])
            	# show_data = ShowTheme.find_by(:id => mediaid.show_id) || ShowTheme.find_by(:ott_id => mediaid.show_id)
            	subcategory_data = Subcategory.find_by(:id => mediaid.subcategory_id)
            	if !subcategory_data.nil?
            		@subcatgeory = "yes"
            		subcategory_data = Subcategory.find_by(:id => mediaid.subcategory_id)
            		shows = ShowTheme.find_by(:id => subcategory_data.show_id);
            		show_name = shows.title

            	if subcategory_data.parent_subcategory_id == "show"
			  		s3uploadremovspace = @bg.name+"/"+catalog.name+"/shows/"+show_name+"/subcategories/"+subcategory_data.title+"/episodes/"+@edit_media.title+"/images"
			  		s3upload = s3uploadremovspace.gsub(" ", "_");
			    else
			    parent_subcategory = Subcategory.find_by(:id => subcategory_data.parent_subcategory_id)
			    if parent_subcategory.parent_subcategory_id != "show"
			    	parent_s_subcategory = Subcategory.find_by(:id => parent_subcategory.parent_subcategory_id)
			    	s3uploadremovspace = @bg.name+"/"+catalog.name+"/shows/"+show_name+"/subcategories/"+parent_s_subcategory.title+"/subcategories/"+parent_subcategory.title+"/subcategories/"+subcategory_data.title+"/episodes/"+@edit_media.title+"/images"
			    	s3upload = s3uploadremovspace.gsub(" ", "_");
			    else
			    	s3uploadremovspace = @bg.name+"/"+catalog.name+"/shows/"+show_name+"/subcategories/"+parent_subcategory.title+"/subcategories/"+subcategory_data.title+"/episodes/"+@edit_media.title+"/images"
			    	s3upload = s3uploadremovspace.gsub(" ", "_");
			  	end
			    end
            		#s3upload = @bg.name+"/"+catalog.name+"/"+subcategory_data.title+"/episodes/"+@edit_media.title+"/images"
            		puts "======s3upload subcategory======"
            		url = "http://"+get_ott_base_url+"/catalogs/shows/subcategories/"+subcategory_data.ott_id+"/episodes/"+@edit_media.ott_id+"?auth_token="+@bg.ott_auth_token+"&status=any&region=any"
            	else
            		@show = "yes"
            		show_data = ShowTheme.find_by(:id => mediaid.show_id) || ShowTheme.find_by(:ott_id => mediaid.show_id)
            		#s3upload = @bg.name+"/"+catalog.name+"/"+show_data.title+"/episodes/"+@edit_media.title+"/images"
            		s3uploadremovspace = @bg.name+"/"+catalog.name+"/shows/"+show_data.title+"/episodes/"+@edit_media.title+"/images"
            		s3upload = s3uploadremovspace.gsub(" ", "_");
            		puts "======s3upload show======"
            		p s3upload
            		url = "http://"+get_ott_base_url+"/catalogs/shows/"+show_data.ott_id+"/episodes/"+@edit_media.ott_id+"?auth_token="+@bg.ott_auth_token+"&status=any&region=any"
            	end
            else	
            	   s3uploadremovspace = @bg.name+"/"+catalog.name+"/"+@edit_media.title+"/images"
            	   s3upload = s3uploadremovspace.gsub(" ", "_");
            		puts "======s3upload show======"
            		p s3upload
            	url = "http://"+get_ott_base_url+"/catalogs/"+@edit_media.ott_catalog_id+"/items/"+@edit_media.ott_id+"?auth_token="+@bg.ott_auth_token+"&status=any&region=any"
            end
            @signature = get_s3_upload_key(s3upload,@bg.bucket_name,@bg.domain_name)
            response = http_request("get",{},url,60)
            if response["code"] == 200
                @item = response["body"]["data"]
                if !@item['published_date'].blank?
                	@item['published_date'] = @item['published_date'].to_datetime.utc.to_i * 1000
                end
                	
                if !@item['unpublished_date'].blank?
                	@item['unpublished_date'] = @item['unpublished_date'].to_datetime.utc.to_i * 1000
                end

                catalog = Catalog.find_by(:ott_id => @edit_media.ott_catalog_id)
                item_apps = App.any_in(:ott_id => @item["app_ids"]).as_json.map {|h| h["_id"]}

                #get catalog info from ott
                url = "http://"+get_ott_base_url+"/catalogs/"+@edit_media.ott_catalog_id+"?auth_token="+@bg.ott_auth_token
                response = http_request("get",{},url,60)
                if response["code"] == 200
                	@catalog = response["body"]["data"]

                	tenants_data = []
                	@accesscontrol = []
                	@catalog["tenant_ids"].each do |tenant|
                	  t = Tenant.find_by(:ott_id => tenant)
                	  h = {}
                	  h["name"] = t.name
                	  h["cms_id"] = t.id
                	  h["ott_id"] = t.ott_id
                	  h["languages"] = (@catalog["languages"][tenant]).map{ |h| h["name"] }
                	  h["genres"] = (@catalog["genres"][tenant]).map{ |h| h["name"] }
                	  h["apps"] = []

					  App.where(:tenant_id => t.id).each do |app|
					  	id = app.id
					  	applications = catalog.app_id
					  	if applications.include?("#{id}")
				          app_h = {}
				          app_h["app_id"] = app.id
						  app_h["app_name"] = app.name
						  app_h["app_tid"] = app.tenant_id
						  h["apps"] << app_h
						end  
					  end
                	  tenants_data << h

                	catalogs = Catalog.find_by(:bg_id => @bg.id, :theme => "access_control", :tenant_id => t.id.to_s)
			 		acurl = "http://"+get_ott_base_url+"/catalogs/"+catalogs.ott_id+"/items?auth_token="+@bg.ott_auth_token+"&status=any&region=any"
					acresponse = http_request("get",{},acurl,60)
					if acresponse["code"] == 200
					accesscontrols = acresponse["body"]["data"]["items"]

					accesscontrols.each do |acc|
						ac = {}
						ac["name"] = t.name
						ac["title"] = acc["title"]
						ac["content_id"] = acc["content_id"]
						@accesscontrol << ac
					end
					end
                	end	
                	@item.merge!({"catalog_cms_id" => catalog["_id"],
                	          "item_apps" => item_apps,
                	          "tenants_data" => tenants_data,
                	          "catalog_name" => catalog.name+"["+catalog.theme+"]",
                	          "catalog_custom_tags" => @catalog["custom_tags"],
                	          "catalog_theme" => catalog["theme"],
                	          "item_genres" => @item["genres"]
                	          })
                end

                url = "http://" + get_ott_base_url + "/people?auth_token=" + @bg.ott_auth_token
				response = http_request("get",{},url,60)
				if response["code"] == 200
					@people = response["body"]["data"]["items"]
					# @total_people_count = response["body"]["data"]["total_people_count"]
				end

				# catalogs = Catalog.find_by(:bg_id => current_bg.id, :theme => "access_control")
			 #    url = "http://"+get_ott_base_url+"/catalogs/"+catalogs.ott_id+"/items?auth_token="+current_bg.ott_auth_token+"&status=any&region=any"
				# response = http_request("get",{},url,60)
				# if response["code"] == 200
				# @accesscontrol = response["body"]["data"]["items"]
				# puts "=====accesscontrol====="
				# p @accesscontrol
				# end
                render "edit"
            else
                #handle error     
                render js: "alert('Can not edit this item!');"
            end
            
	    end
		
		#render "new"
	end
	def new_bulk_upload
		@bg = current_bg
		render "new_bulk_upload"
	end

	def bulk_update
		@bg = current_bg
		render "bulk_update"
	end
		
	def view_media
		@bg = current_bg
		@media = Media.find(params[:media_id])

		catalog = Catalog.find_by(:ott_id => @media.ott_catalog_id)
        case catalog.theme
        when "show"
	    mediaid = Media.find_by(:id => params[:media_id])
            subcategory_data = Subcategory.find_by(:id => mediaid.subcategory_id)
            if !subcategory_data.nil?
            subcategory_data = Subcategory.find_by(:id => mediaid.subcategory_id)
            url = "http://"+get_ott_base_url+"/catalogs/shows/subcategories/"+subcategory_data.ott_id+"/episodes/"+mediaid.ott_id+"?auth_token="+@bg.ott_auth_token+"&status=any&region=any"
            else
	        show_data = ShowTheme.find_by(:id => mediaid.show_id) || ShowTheme.find_by(:ott_id => mediaid.show_id)
            url = "http://"+get_ott_base_url+"/catalogs/shows/"+show_data.ott_id+"/episodes/"+mediaid.ott_id+"?auth_token="+@bg.ott_auth_token+"&status=any&region=any"
            end
        else
	        url = "http://"+get_ott_base_url+"/catalogs/"+@media.ott_catalog_id+"/items/"+@media.ott_id+"?auth_token="+@bg.ott_auth_token+"&status=any&region=any"
        end
		#url = "http://"+get_ott_base_url+"/catalogs/"+@media.ott_catalog_id+"/items/"+@media.ott_id+"?auth_token="+@bg.ott_auth_token+"&status=any&region=any"
		response = http_request("get",{},url,60)
		@smart_url = response["body"]["data"]["play_url"]
		if @smart_url.blank?
		else
		@smart_url = response["body"]["data"]["play_url"]["saranyu"]["url"]
		end

		#render partial: "media/view_media"
		render "media/view_media_info"
	end


	def check_media_title
		title  = params[:title].strip
		catalog_id = params[:catalog_id]
		@bg = current_bg

		catalog_ott_id = Catalog.find_by(:id => catalog_id)
		theme = catalog_ott_id.theme

		titleexits = Media.find_by(:title => title, :ott_catalog_id => catalog_ott_id.ott_id, :is_removed => false)

		puts "====titleexits===="
		p titleexits

		if titleexits.nil?
			if theme == "show"
            if params[:show].nil? || params[:show].empty?
            	subcategory_data = Subcategory.find_by(:id => params[:subcategory_id])
            	shows = ShowTheme.find_by(:id => subcategory_data.show_id);
            	show_name = shows.title
            	if subcategory_data.parent_subcategory_id == "show"
			  	s3uploadremovspace = @bg.name+"/"+catalog_ott_id["name"]+"/shows/"+show_name+"/subcategories/"+subcategory_data.title+"/episodes/"+title+"/images"
			  	s3upload = s3uploadremovspace.gsub(" ", "_");
			    else
			    parent_subcategory = Subcategory.find_by(:id => subcategory_data.parent_subcategory_id)
			    if parent_subcategory.parent_subcategory_id != "show"
			    	parent_s_subcategory = Subcategory.find_by(:id => parent_subcategory.parent_subcategory_id)
			    s3uploadremovspace = @bg.name+"/"+catalog_ott_id["name"]+"/shows/"+show_name+"/subcategories/"+parent_s_subcategory.title+"/subcategories/"+parent_subcategory.title+"/subcategories/"+subcategory_data.title+"/episodes/"+title+"/images"
			    s3upload = s3uploadremovspace.gsub(" ", "_");
			    else
			    s3uploadremovspace = @bg.name+"/"+catalog_ott_id["name"]+"/shows/"+show_name+"/subcategories/"+parent_subcategory.title+"/subcategories/"+subcategory_data.title+"/episodes/"+title+"/images"
			    s3upload = s3uploadremovspace.gsub(" ", "_");
			  	end
			    end

            	#s3upload = @bg.name+"/"+catalog_ott_id["name"]+"/"+params[:subcategory]+"/episodes/"+title+"/images"
            else
            	s3uploadremovspace = @bg.name+"/"+catalog_ott_id["name"]+"/shows/"+params[:show]+"/episodes/"+title+"/images"
            	s3upload = s3uploadremovspace.gsub(" ", "_");
            end
        	else
        		if params[:video_list] == "true"
        		s3uploadremovspace = @bg.name+"/"+catalog_ott_id["name"]+"/video_list/"+title+"/images"
        		else
        		s3uploadremovspace = @bg.name+"/"+catalog_ott_id["name"]+"/"+title+"/images"
        		end
        	s3upload = s3uploadremovspace.gsub(" ", "_");
        	end
			@signature = get_s3_upload_key(s3upload,@bg.bucket_name,@bg.domain_name)

			puts "signature==========="
			p @signature
			render :json => {:signature => @signature}.to_json
		else
			render :json => {:error => "Title Already Exists!"}.to_json, :status => 500
		end
	end

	def bulk_data_update
		status = true
		error = ""
		if status
			if !params[:excel_file_path]
				status = false
				error = "No file selected for upload!"
			end
			if status
				if !params[:excel_file_path].content_type.include? ".sheet"
					status = false
					error = "Please provide valid file format(xslx)!"
				end	
			end	
		end

		if status
			bg = current_bg
			upload_file = params[:excel_file_path]
			#write file to local disk
	  		name =  upload_file.original_filename
			directory = Rails.root.join("public/excel_files")
			path = File.join(directory, name)
			File.open(path, "wb") { |f| f.write(upload_file.read) }
			#parse the excel sheet
			media_list = excel_to_hash(path)

			if media_list.blank?
				status = false
				error = "First Column Can't be blank!"
			end
		    media_list.each_with_index do |media_row,index|
		    	if params[:validate] == "yes"
		    		params[:validate] = true
		    		#validate all data
				    # if status
				    # 	status,error = validate_media_row_for_ott_saas(media_row,index+1) 
				    # end
				else 
				    #validate only mandatory fields
				    if status
				    	params[:validate] = false
					    status,error = validate_only_mandatory_params_for_update(media_row,index+1)
				    end    
				end    

			end
			if status   
				status = check_for_identical_catalog(media_list) 	
				if !status
					status = false
					error = "Please provide indentical catalog id!!"
				else
				    catalog_id = media_list.map { |h| h["Catalog Id"] }.uniq.last	
					catalog = Catalog.new.get_catalog_by_id(catalog_id)
					if catalog
						url = "http://"+get_ott_base_url+"/catalogs/"+catalog["ott_id"]+"?auth_token="+bg.ott_auth_token
						response = http_request("get",{},url,60)
						if response["code"] == 200
				    		catalog_info = response["body"]["data"] 
				    		params[:catalog_ott_obj] = catalog_info
				    	end
				    else
				    	status = false
				        error = "Please provide valid Catalog Id"	
				    end		
				end	
			end	
		end

		ps_list = []	
  	    PublishSettings.where(:bg_id=>current_bg.id).each do |ps|
    		ps_list << ps.attributes
        end

		if status
		   	media_params = {}
		   	bulkupload = BulkUpload.new
			bulkupload.excel_file_path = path
			bulkupload.mail_option = params[:mail_option]
			bulkupload.bg_id = current_bg.id
			bulkupload.user_id = current_user.id
			bulkupload.save
			#creating ID appended excel sheet filename
			justfilename = File.basename(path)
			justdirpath = File.dirname(path)
			new_name = justdirpath+"/"+bulkupload.id+"_"+justfilename
			system("sudo mv "+path+" "+new_name)
			bulkupload.excel_file_path = new_name
			bulkupload.total_count = 0
			bulkupload.published_count = 0
			bulkupload.save
	    end

		if status && (bg.plan == "ott_saas")
			status,error = validate_data_at_cms(media_list,"bulk_update")
			if status
				flag = 0
				media_list.each do |media_row|
					source_url_present = 0
			    	media_params["title"] = media_row["Title"]
					
					media_params["priority"] = "low"
					if media_row["Priority"]
						media_params["priority"] = media_row["Priority"]
					end
					
					media_params["notification_url"] = media_row["Notification URL"]
					media_params["user_id"] = current_user.id
					media_params["user_role"] = current_user.roles.first
					media_params["publish_settings_id"] = ps_list.first["_id"]
					media_params["bg_id"] = current_bg.id
					
					media_params["ott_status"] = "edit"
					media_params["is_removed"] = false
					media_params["bulk_upload_id"] = bulkupload.id
					#if show is present then assign episode to show
					if !media_row["Show Id"].blank?
						parent_id = media_row["Show Id"]
						media_params["show_id"] = media_row["Show Id"]
					else
					#if subcategory is present then assign episode to subcategory
						parent_id = media_row["Subcategory Id"]	
						media_params["subcategory_id"] = media_row["Subcategory Id"]
					end	
					#media_params["metadata"] = extract_metadata_from_row_for_ott(media_row)
					media_params["custom_keys"] = {}
					bulkupload.total_count = bulkupload.total_count+1
					bulkupload.save
					# catalog = Catalog.find_by(:id => media_row["Catalog Id"])
					media = Media.find_by(:ott_catalog_id => catalog.ott_id ,:asset_id => media_row["Asset Id"],:is_removed=> false)
					params[:type] = "bulk_update" 
					data = extract_metadata_from_row_for_ott(media_row,bulkupload,params)
					metadata = extract_metadata_from_params_for_bulk_upload(data,bg)
					status,error = media.update_at_ott(metadata)
					if !status
					   flag = 1
					end	
				    # end
				    if flag == 1
				    	break
				    end	
				end	
			end	
	   	end

	    #media addition
	    if status
			flash[:notice] = "All media in the bulk upload sheet has been added to the queue!"
		else
			flash[:error] = error
		end
		redirect_to "/media/view_all"
	end

	def bridge_upload
		@bg = current_bg
		render "bridge_upload"
	end
	
	def bridge_update
		@bg = current_bg
		render "bridge_update"
	end	

	def bridge_data_upload
		status = true
		error = ""
		if status
			if !params[:excel_file_path]
				status = false
				error = "No file selected for upload!"
			end
			if status
				if !params[:excel_file_path].content_type.include? ".sheet"
					status = false
					error = "Please provide valid file format(xslx)!"
				end	
			end	
		end

		if status
			bg = current_bg
			upload_file = params[:excel_file_path]
			#write file to local disk
	  		name =  upload_file.original_filename
			directory = Rails.root.join("public/excel_files")
			path = File.join(directory, name)
			File.open(path, "wb") { |f| f.write(upload_file.read) }
			#parse the excel sheet
			media_list = excel_to_hash(path)

			#check bulk upload for billable items
			params[:bg_object] = bg
			params[:entity] = "media"
			params[:bg_plan] = @bg_plan
			params[:is_bulk_upload] = "yes"
			params[:bulk_upload_count] = media_list.count
				
		    media_list.each_with_index do |media_row,index|
			    #code needs to be added for validate mandatory params for bridge
			end

			if status   
				status = check_for_identical_movie_catalog(media_list) 
				status = check_for_identical_person_catalog(media_list) 
				Rails.logger.debug "#{status}=========catalog status"	
				if !status
					status = false
					error = "Please provide indentical catalog id!!"
				else
					
						catalog_id = media_list.map { |h| h["Movie Catalog Id"] }.uniq.last
						catalog = Catalog.new.get_catalog_by_id(catalog_id)
						if catalog
							url = "http://"+get_ott_base_url+"/catalogs/"+catalog["ott_id"]+"?auth_token="+bg.ott_auth_token
    						response = http_request("get",{},url,60)
    						if response["code"] == 200
				    			catalog_info = response["body"]["data"] 
				    			params[:movie_catalog_ott_obj] = catalog_info
				    		end
				    	else
				    		status = false
				       		 error = "Please provide valid movie Catalog Id"	
				    	end	
					
						catalog_id = media_list.map { |h| h["Person Catalog Id"] }.uniq.last
						catalog_person = Catalog.new.get_catalog_by_id(catalog_id)
						if catalog_person
							url = "http://"+get_ott_base_url+"/catalogs/"+catalog_person["ott_id"]+"?auth_token="+bg.ott_auth_token
    						response = http_request("get",{},url,60)
    						if response["code"] == 200
				    			catalog_info = response["body"]["data"] 
				    			params[:person_catalog_ott_obj] = catalog_info
				    		end
				   		 else
				    		status = false
				        	error = "Please provide valid person Catalog Id"	
				    	end	
					
				    #catalog_id = media_list.map { |h| h["Catalog Id"] }.uniq.last	
					
				end	
			end	
		end

		##No Extra key support for ott_saas users
	   	if status
	   		ps_list = []	
		    PublishSettings.where(:bg_id=>current_bg.id).each do |ps|
				ps_list << ps.attributes
	    	end
		   	media_params = {}
		   	bulkupload = BulkUpload.new
			bulkupload.excel_file_path = path
			bulkupload.mail_option = params[:mail_option]
			bulkupload.bg_id = current_bg.id
			bulkupload.user_id = current_user.id
			bulkupload.save
			#creating ID appended excel sheet filename
			justfilename = File.basename(path)
			justdirpath = File.dirname(path)
			new_name = justdirpath+"/"+bulkupload.id+"_"+justfilename
			system("sudo mv "+path+" "+new_name)
			bulkupload.excel_file_path = new_name
			bulkupload.total_count = 0
			bulkupload.published_count = 0
			bulkupload.save
	    end

	    #add bridge code
	    if status && (bg.plan == "ott_saas")
				flag = 0
				i = 2
				media_list.each do |media_row|
					source_url_present = 0
					
					media_params["priority"] = "low"
					if media_row["Priority"]
						media_params["priority"] = media_row["Priority"]
					end

					if media_row.key? "Source URL" 
						if !media_row["Source URL"].blank?
							if media_row["Source URL"].starts_with?("ftp")
								media_params["source_type"] = "FTP"
							else
								media_params["source_type"] = "HTTP"
							end
					    	media_params["source_url"] = fix_multiple_slashes(media_row["Source URL"]) 
					    	#media_params["source_url"] = media_row["Source URL"]
					    	media_params["status"] = "Created"
					    	media_params["smart_url"] = nil
					    	source_url_present = 1 		
					    end	
					end

					if source_url_present == 0
						media_params["source_url"] = nil
						# media_params["smart_url"] = media_row["Smart URL"]
						media_params["smart_url"] = nil
						media_params["imgoptradio"] = "no"
						media_params["source_type"] = "external"
						media_params["status"] = "Created Externally"
					end
					
					media_params["notification_url"] = media_row["Notification URL"]
					media_params["user_id"] = current_user.id
					media_params["user_role"] = current_user.roles.first
					media_params["publish_settings_id"] = ps_list.first["_id"]
					media_params["bg_id"] = current_bg.id
					
					media_params["ott_status"] = "edit"
					media_params["is_removed"] = false
					media_params["asset_id"] = media_row["Asset Id"] if media_row["Asset Id"]
					media_params["bulk_upload_id"] = bulkupload.id

					#if show is present then assign episode to show
					if !media_row["Show Id"].blank?
						parent_id = media_row["Show Id"]
						media_params["show_id"] = media_row["Show Id"]
					else
					#if subcategory is present then assign episode to subcategory
						parent_id = media_row["Subcategory Id"]	
						media_params["subcategory_id"] = media_row["Subcategory Id"]
					end	

					media_params["custom_keys"] = {}
					bulkupload.total_count = bulkupload.total_count+1
					bulkupload.save

					#request to cinestaan
					
					status,data = get_data_from_external_db(params,media_row,bg)

					
					if status
						#build json response
						params[:media_row] = media_row
						params[:bridge_data] = data 
						params[:media_params] = media_params
						metadata = build_data_from_bridge(params)

						
						status,people_data = construct_data_people(params,bg)
						
						if status	
						
							status,ott_people_data = get_ott_data people_data,catalog_person,bg
							
							if status
								
								metadata["people"] = ott_people_data
							else
								status = false
				        		error = "People data can not be creted at ott"
				        	end


						else
							status = false
				        	error = "People data can not be constructed"
						end
						
						media_params["title"] = metadata["title"]
						#create media at cms
						media = create_media(media_params)
						media.ott_catalog_id = catalog.ott_id

						if media.save
						    #create item at ott
							status,data = media.create_at_ott(metadata,catalog,parent_id)
							logger.debug "*************************#{status}======media status==#{data}"	
							if !status
								#delete media from cms
								media.delete
								#send email for failure to registered user	
							else


								s_catalog_id = catalog["ott_id"]
								
								
								logger.debug "movie_ott_data==#{data}"
								item = data["body"]["data"]
								s_item_id = item["item_id"]
								#soundtrack_data = []
								status,soundtrack_data = create_videolist params,bg,catalog_person
								logger.debug "=====videolist status#{status}=======#{soundtrack_data}=="

								if status
									logger.debug "sound track details=============#{soundtrack_data}"
									soundtrack_data.each do |s|
									logger.debug "from videolist creating-====="
									status,data = media.create_video_list s,s_catalog_id,s_item_id
									logger.debug "from videolist ott===#{data}"

									end

								else
									logger.debug "Sound track details not created"
								end
						
						end
						end
	
					else
					    #code to be added for send email to registered user for failure 
					end	
	
				    i = i + 1
				end	
	   	end

		#media addition
	    if status
			flash[:notice] = "All media in the bulk upload sheet has been added to the queue!"
		else
			flash[:error] = error
		end
		redirect_to "/media/view_all"

	end	
	
	def bridge_data_update
		status = true
		error = ""
		if status
			if !params[:excel_file_path]
				status = false
				error = "No file selected for upload!"
			end
			if status
				if !params[:excel_file_path].content_type.include? ".sheet"
					status = false
					error = "Please provide valid file format(xslx)!"
				end	
			end	
		end
		if status
			bg = current_bg
			upload_file = params[:excel_file_path]
			#write file to local disk
	  		name =  upload_file.original_filename
			directory = Rails.root.join("public/excel_files")
			path = File.join(directory, name)
			File.open(path, "wb") { |f| f.write(upload_file.read) }
			#parse the excel sheet
			media_list = excel_to_hash(path)

			#check bulk upload for billable items
			params[:bg_object] = bg
			params[:entity] = "media"
			params[:bg_plan] = @bg_plan
			params[:is_bulk_upload] = "yes"
			params[:bulk_upload_count] = media_list.count
				
		    media_list.each_with_index do |media_row,index|
			    #validate mandatory params for bridge update
			end

			if status   
				status = check_for_identical_movie_catalog(media_list) 
				status = check_for_identical_person_catalog(media_list) 
				Rails.logger.debug "#{status}=========catalog status"	
				if !status
					status = false
					error = "Please provide indentical catalog id!!"
				else					
					catalog_id = media_list.map { |h| h["Movie Catalog Id"] }.uniq.last
					catalog = Catalog.new.get_catalog_by_id(catalog_id)
					if catalog

						url = "http://"+get_ott_base_url+"/catalogs/"+catalog["ott_id"]+"?auth_token="+bg.ott_auth_token
    					response = http_request("get",{},url,60)
    					logger.debug "url ==#{url}====catalog res===#{response}"
    					if response["code"] == 200
				    		catalog_info = response["body"]["data"]
				    		logger.debug "catalog======#{catalog_info}" 
				    		params[:movie_catalog_ott_obj] = catalog_info
				    	end
				    else
				    	status = false
				    	error = "Please provide valid movie Catalog Id"	
				    end	
					
					catalog_id = media_list.map { |h| h["Person Catalog Id"] }.uniq.last
					catalog_person = Catalog.new.get_catalog_by_id(catalog_id)
					if catalog_person
						#url = "http://"+get_ott_base_url+"/catalogs/"+catalog_person["ott_id"]+"?auth_token="+bg.ott_auth_token    						response = http_request("get",{},url,60)
    					if response["code"] == 200
				    		catalog_info = response["body"]["data"] 
				    		params[:person_catalog_ott_obj] = catalog_info
				    	end
				    else
				    	status = false
				       	error = "Please provide valid person Catalog Id"	
				    end	
				end	
			end	
		end

		if status
	   		ps_list = []	
		    PublishSettings.where(:bg_id=>current_bg.id).each do |ps|
				ps_list << ps.attributes
	    	end
		   	media_params = {}
		   	bulkupload = BulkUpload.new
			bulkupload.excel_file_path = path
			bulkupload.mail_option = params[:mail_option]
			bulkupload.bg_id = current_bg.id
			bulkupload.user_id = current_user.id
			bulkupload.save
			#creating ID appended excel sheet filename
			justfilename = File.basename(path)
			justdirpath = File.dirname(path)
			new_name = justdirpath+"/"+bulkupload.id+"_"+justfilename
			system("sudo mv "+path+" "+new_name)
			bulkupload.excel_file_path = new_name
			bulkupload.total_count = 0
			bulkupload.published_count = 0
			bulkupload.save
	    end
	    if status && (bg.plan == "ott_saas")
				flag = 0
				i = 2
				media_list.each do |media_row|
					source_url_present = 0
					
					media_params["priority"] = "low"
					if media_row["Priority"]
						media_params["priority"] = media_row["Priority"]
					end

					if media_row.key? "Source URL" 
						if !media_row["Source URL"].blank?
							media_params["source_type"] = "FTP"
					    	media_params["source_url"] = fix_multiple_slashes(media_row["Source URL"]) 
					    	#media_params["source_url"] = media_row["Source URL"]
					    	media_params["status"] = "Created"
					    	media_params["smart_url"] = nil
					    	source_url_present = 1 		
					    end	
					end

					if source_url_present == 0
						media_params["source_url"] = nil
						# media_params["smart_url"] = media_row["Smart URL"]
						media_params["smart_url"] = nil
						media_params["imgoptradio"] = "no"
						media_params["source_type"] = "external"
						media_params["status"] = "Created Externally"
					end
					
					media_params["notification_url"] = media_row["Notification URL"]
					media_params["user_id"] = current_user.id
					media_params["user_role"] = current_user.roles.first
					media_params["publish_settings_id"] = ps_list.first["_id"]
					media_params["bg_id"] = current_bg.id
					
					media_params["ott_status"] = "edit"
					media_params["is_removed"] = false
					media_params["asset_id"] = media_row["Asset Id"] if media_row["Asset Id"]
					media_params["bulk_upload_id"] = bulkupload.id

					#if show is present then assign episode to show
					if !media_row["Show Id"].blank?
						parent_id = media_row["Show Id"]
						media_params["show_id"] = media_row["Show Id"]
					else
					#if subcategory is present then assign episode to subcategory
						parent_id = media_row["Subcategory Id"]	
						media_params["subcategory_id"] = media_row["Subcategory Id"]
					end	

					media_params["custom_keys"] = {}
					bulkupload.total_count = bulkupload.total_count+1
					bulkupload.save

					#request to cinestaan
					status,data = get_data_from_external_db(params,media_row,bg)
					if status
						#build json response
						params[:media_row] = media_row
						params[:bridge_data] = data 
						params[:media_params] = media_params
						metadata = build_data_from_bridge(params)

						status,people_data = construct_data_people(params,bg)
						if status
							status,ott_people_data = update_ott_people(people_data,catalog_person,bg)
							
							logger.debug "people=======#{ott_people_data}"
							if status
								metadata[:people] = ott_people_data
							else
								status = false
				        		error = "People data can not be creted at ott"
							end
						else
								status = false
				        		error = "People data can not be creted at ott"

						end

						

						media_params["title"] = metadata["title"]
						#update media at cms
						media = Media.find_by(:ott_catalog_id => catalog.ott_id ,:asset_id => media_row["Asset Id"],:is_removed=> false)
						media = update_media(media,metadata)
						#media = create_media(media_params)
						#media = Media.find_by(:ott_catalog_id => catalog.ott_id ,:asset_id => media_row["Asset Id"],:is_removed=> false)
						media.ott_catalog_id = catalog.ott_id

						if media.save
						    #update item at ott
							status,data = media.update_at_ott(metadata)
							if !status
								#delete media from cms
								media.delete
								#send email for failure to registered user	
							end	

							s_catalog_id = catalog["ott_id"]
							
							
							#logger.debug "movie_ott_data==#{data}"
							item = data["body"]["data"]
							s_item_id = item["item_id"]

							status,soundtrack_data = create_videolist params,bg,catalog_person
							logger.debug "=====videolist data#{status}======#{soundtrack_data}======"
							if status
								soundtrack_data.each do |s|
								status,videolist = media.get_videolist_data s_catalog_id,s_item_id
								
									videolists = videolist["body"]["data"]
									videolist_items = videolists["items"]
									video_title = videolist_items.select { |a1| a1["title"] == s["title"] }.empty?
									
									if video_title
										status,data = media.create_video_list s,s_catalog_id,s_item_id
										logger.debug "from videolist ott===#{data}"

									else
										
										videolist_items.each do |v|
											if s["title"] == v["title"]
												videolist_id = v["videolist_id"]
												status,video_data = media.update_video_list s,s_catalog_id,videolist_id,s_item_id
												
											end
										end
									end
								end	
							else
								logger.debug "videolist can not be created"
							
							end

						end

						
					else
					    #send email for failure to registered user	
					end	
	
				    i = i + 1
				end	
	   	end
#media addition
	    if status
			flash[:notice] = "All media in the bulk update sheet has been added to the queue!"
		else
			flash[:error] = error
		end
		redirect_to "/media/view_all"


	end
	def videolist_upload
		@bg = current_bg
		render "videolist_upload"
	end
	
	def videolist_update
		@bg = current_bg
		render "videolist_update"
	end	


	def bulk_videolist_upload
		status = true
		error = ""
		if status
			if !params[:excel_file_path]
				status = false
				error = "No file selected for upload! from videolist"
			end
			if status
				if !params[:excel_file_path].content_type.include? ".sheet"
					status = false
					error = "Please provide valid file format(xslx)!"
				end	
			end
		end
		logger.debug "===00#{status}"
		if status
			bg = current_bg
			logger.debug "=====#{bg.inspect}"
			upload_file = params[:excel_file_path]
			#write file to local disk
	  		name =  upload_file.original_filename
			directory = Rails.root.join("public/excel_files")
			path = File.join(directory, name)
			File.open(path, "wb") { |f| f.write(upload_file.read) }
			#parse the excel sheet
			video_list = excel_to_hash(path)
			params[:bg_object] = bg
			params[:entity] = "media"
			params[:bg_plan] = @bg_plan
			params[:is_bulk_upload] = "yes"
			params[:bulk_upload_count] = video_list.count
			status,message = track_entity_count(params)
			if status
				
			    video_list.each_with_index do |media_row,index|
					    if status
					    	# params[:validate] = false
 					    	#status,error = validate_only_mandatory_params(media_row,index+1)
					    end    
					# end    

				end
				logger.debug "===2#{status}"
				if status   
					status = check_for_identical_catalog(video_list) 	
					if !status
						status = false
						error = "Please provide indentical catalog id!!"
					else
					    catalog_id = video_list.map { |h| h["Catalog Id"] }.uniq.last	
						catalog = Catalog.new.get_catalog_by_id(catalog_id)
						if catalog
							url = "http://"+get_ott_base_url+"/catalogs/"+catalog["ott_id"]+"?auth_token="+bg.ott_auth_token
	    					response = http_request("get",{},url,60)
	    					if response["code"] == 200
					    		catalog_info = response["body"]["data"] 
					    		params[:catalog_ott_obj] = catalog_info
					    		logger.debug "====catalog info....#{catalog_info}"
					    	end
					    else
					    	status = false
					        error = "Please provide valid Catalog Id"	
					    end	
					end	
				end	
			else
			    error = message	
			end	   
		end
		if status
	   		ps_list = []	
		    PublishSettings.where(:bg_id=>current_bg.id).each do |ps|
				ps_list << ps.attributes
	    	end
		   	media_params = {}
		   	bulkupload = BulkUpload.new
			bulkupload.excel_file_path = path
			bulkupload.mail_option = params[:mail_option]
			bulkupload.bg_id = current_bg.id
			bulkupload.user_id = current_user.id
			bulkupload.save
			#creating ID appended excel sheet filename
			justfilename = File.basename(path)
			justdirpath = File.dirname(path)
			new_name = justdirpath+"/"+bulkupload.id+"_"+justfilename
			system("sudo mv "+path+" "+new_name)
			bulkupload.excel_file_path = new_name
			bulkupload.total_count = 0
			bulkupload.published_count = 0
			bulkupload.save
	    end
		#build media list

		if status && (bg.plan == "ott_saas")
			
			#status,error = validate_data_at_cms(video_list,"bulk_upload")
			if status
				flag = 0
				i = 2
				video_list.each do |media_row|
					source_url_present = 0
			    	media_params["title"] = media_row["Title"]
			    	logger.debug "===#{media_row}"
			    	logger.debug "===12====#{catalog["ott_id"]}=====#{media_row["media_asset_id"]}"
					media = Media.find_by(:ott_catalog_id=>catalog["ott_id"],:asset_id=>media_row["Movie Asset Id"])
					logger.debug "====media element====#{media.inspect}"
					if media
						params[:type] = "bulk_upload"
						metadata = extract_videodata_from_row_for_ott(media_row,bulkupload,params)
						
						logger.debug "data created from bulk upload.....#{media.ott_catalog_id}=========#{media.ott_id}"

						status,error = media.create_video_list metadata,media.ott_catalog_id,media.ott_id
						if !status
						   error = "#{error} at row number #{i}!"
						   flag = 1
						end	
				    else
				    	flash[:notice] = "Media is not present"
				    end
				    if flag == 1
				    	break
				    end	
				    i = i + 1
				end	
			end	
	   	end

	    #media addition
	    if status
			flash[:notice] = "All videos in the bulk upload sheet has been added"
		else
			flash[:error] = error
		end
		redirect_to "/catalogs/view_all"


		end  #bulk_videolist upload

		def bulk_videolist_update


		status = true
		error = ""
		if status
			if !params[:excel_file_path]
				status = false
				error = "No file selected for upload!"
			end
			if status
				if !params[:excel_file_path].content_type.include? ".sheet"
					status = false
					error = "Please provide valid file format(xslx)!"
				end	
			end	
		end

		if status
			bg = current_bg
			upload_file = params[:excel_file_path]
			#write file to local disk
	  		name =  upload_file.original_filename
			directory = Rails.root.join("public/excel_files")
			path = File.join(directory, name)
			File.open(path, "wb") { |f| f.write(upload_file.read) }
			#parse the excel sheet
			media_list = excel_to_hash(path)

			if media_list.blank?
				status = false
				error = "First Column Can't be blank!"
			end
		    media_list.each_with_index do |media_row,index|
		    	if params[:validate] == "yes"
		    		params[:validate] = true
		    		
				else 
				    #validate only mandatory fields
				    if status
				    	params[:validate] = false
					#    status,error = validate_only_mandatory_params_for_update(media_row,index+1)
				    end    
				end    

			end
			if status   
				status = check_for_identical_catalog(media_list) 	
				if !status
					status = false
					error = "Please provide indentical catalog id!!"
				else
				    catalog_id = media_list.map { |h| h["Catalog Id"] }.uniq.last	
					catalog = Catalog.new.get_catalog_by_id(catalog_id)
					if catalog
						url = "http://"+get_ott_base_url+"/catalogs/"+catalog["ott_id"]+"?auth_token="+bg.ott_auth_token
						response = http_request("get",{},url,60)
						if response["code"] == 200
				    		catalog_info = response["body"]["data"] 
				    		params[:catalog_ott_obj] = catalog_info
				    	end
				    else
				    	status = false
				        error = "Please provide valid Catalog Id"	
				    end		
				end	
			end	
		end

		ps_list = []	
  	    PublishSettings.where(:bg_id=>current_bg.id).each do |ps|
    		ps_list << ps.attributes
        end

		if status
		   	media_params = {}
		   	bulkupload = BulkUpload.new
			bulkupload.excel_file_path = path
			bulkupload.mail_option = params[:mail_option]
			bulkupload.bg_id = current_bg.id
			bulkupload.user_id = current_user.id
			bulkupload.save
			#creating ID appended excel sheet filename
			justfilename = File.basename(path)
			justdirpath = File.dirname(path)
			new_name = justdirpath+"/"+bulkupload.id+"_"+justfilename
			system("sudo mv "+path+" "+new_name)
			bulkupload.excel_file_path = new_name
			bulkupload.total_count = 0
			bulkupload.published_count = 0
			bulkupload.save
	    end

		if status && (bg.plan == "ott_saas")

			#status,error = validate_data_at_cms(media_list,"bulk_update")
			if status
				flag = 0
				media_list.each do |media_row|
					source_url_present = 0
			    	media_params["title"] = media_row["Title"]
					# catalog = Catalog.find_by(:id => media_row["Catalog Id"])
					media = Media.find_by(:ott_catalog_id => catalog.ott_id ,:asset_id => media_row["Movie Asset Id"],:is_removed=> false)
					params[:type] = "bulk_update" 
					metadata = extract_videodata_from_row_for_ott(media_row,bulkupload,params)
					
					status,videolist = media.get_videolist_data media.ott_catalog_id,media.ott_id
					#logger.debug "videolist data===#{videolist}"
					videolists = videolist["body"]["data"]
					videolist_items = videolists["items"]
					video_title = videolist_items.select { |a1| a1["title"] == media_row["Title"] }.empty?
					logger.debug "#{video_title}====sound titlte #{media_row["Title"]}===="
					if video_title
						status,data = media.create_video_list metadata,media.ott_catalog_id,media.ott_id
						logger.debug "from videolist ott===#{data}"

					else
						logger.debug "items video====#{videolist_items}"
						videolist_items.each do |v|
							if media_row["Title"] == v["title"]
								videolist_id = v["videolist_id"]
								status,video_data = media.update_video_list metadata,media.ott_catalog_id,media.ott_id,videolist_id
								logger.debug "videolist updated at ott==="
							end
						end
					end



					if !status
					   flag = 1
					end	
				    # end
				    if flag == 1
				    	break
				    end	
				end	
			end	
	   	end

	    #media addition
	    if status
			flash[:notice] = "All videolist have been added"
		else
			flash[:error] = error
		end
		redirect_to "/catalogs/view_all"


	end	


	def get_republish_media
		@bg = current_bg
		@media = Media.find(params[:media_id]) || Media.find_by(:ott_id => params[:media_id])
		title = @media.title
		@catalog = Catalog.find_by(:ott_id => @media.ott_catalog_id)
		theme = @catalog.theme

		s3uploadremovspace = @bg.name+"/"+@catalog["name"]+"/republishvideo/"+title
		s3upload = s3uploadremovspace.gsub(" ", "_");
		@signature = get_s3_upload_key(s3upload,@bg.bucket_name,@bg.domain_name)
	end


	def post_republish_media
		#update code here
		status = true
		error = ""
		bg = current_bg
		logger.debug "somappa =================== #{params.inspect}"

		if status
			if !params[:source_url]
				status = false
				error = "Video URL cannot be empty!"
			end
		end
		if status
			if (!params[:source_url].start_with?('ftp://')) && (!params[:source_url].start_with?('http://'))
				status = false
				error = "Error in Video URL Format"
			end
		end
		if status
            if params[:source_url].empty?
            	status = false
            	error = "Please provide source url!"
            end	
		end	

		if status
			if params[:source_type] == "uploadvideo"
				source_type = "HTTP"
			else
				source_type = params["source_type"]
			end
			
			source_url = fix_multiple_slashes(params[:source_url])

			#republish call
			bg = current_bg
			media = nil
			if !Media.find_by(:id => params[:media_id])
				status = false
				error = "Invalid Media ID"
			else
				media = Media.find_by(:id => params[:media_id])
				media.source_url = source_url
				media.source_type = source_type
				media.save
			end

			if status
				data = {}
				data["auth_token"] = bg.tx_auth_token
				data["videos"] = {}
				case media.source_type
		        when "FTP"
		            data["videos"]["method"] = "ftp_link"
		        when "HTTP"
		            data["videos"]["method"] = "http_link"
		        when "S3"
		            data["videos"]["method"] = "s3_link"
		        end
				data["videos"]["source_url"] = source_url
            	url = "http://"+bg.tx_base_url+"/videos/"+media.platform_media_id
            	response = http_request("put",data,url,60)
            	if response["code"] == 200
					media.status = "Processing Request"
					media.republish_flag = true
		            media.save
					render json: {} , status: :ok
				else
					render :json => {:error => error}.to_json, :status => 500
				end
            end
		else
			#render :json => {:error => error}.to_json, :status => 500
			render :json => {:error => error}.to_json, :status => 500
		end
	end


	end





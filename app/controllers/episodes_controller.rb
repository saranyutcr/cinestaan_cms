class EpisodesController < ApplicationController
	include EpisodesHelper
	include ApplicationHelper
	
	before_filter :set_object

	def set_object
		@showtheme = ShowTheme.new
		@sub = Subcategory.new
		@catalog_obj = Catalog.new
	end	

	def new
		if params[:show_id]
			@show = (@showtheme.get_show_by_ott_id(params[:show_id])).as_json(:except => ["updated_at","created_at"]) || \
			        (@showtheme.get_show_by_id(params[:show_id])).as_json(:except => ["updated_at","created_at"])
			url = "http://"+get_ott_base_url+"/catalogs/"+ \
						@show["ott_catalog_id"]+"/episodes?region=any&status=any&auth_token="+ \
						current_bg.ott_auth_token
		else
			@subcategory = (@sub.get_subcategory_by_id(params[:subcategory_id])).as_json(:except => ["updated_at","created_at"]) || \
			               (@sub.get_subcategory_by_ott_id(params[:subcategory_id])).as_json(:except => ["updated_at","created_at"])
			url = "http://"+get_ott_base_url+"/catalogs/"+\
						@subcategory["ott_catalog_id"]+"/episodes?region=any&status=any&auth_token="+ \
						current_bg.ott_auth_token
		end  
		response = http_request("get",{},url,60)
		if response["code"] == 200
			@catalog = response["body"]["data"]
			@episodes = response["body"]["data"]["items"]
		end
	end

	def create
		params[:parentobj] = eval(params[:parentobj])
		params[:bg_obj] = current_bg
		if params[:parent_flag] == "show"
			status,obj = create_episode_for_show(params)
		else
			status,obj = create_episode_for_subcategory(params)
		end	
		if status
			render :json =>{:response => obj}.to_json , status: :ok
		else
			render :json => {:error => obj}.to_json, :status => 500
		end	
	end	

	def view_all
		@bg = current_bg
		if params[:page].nil?
				page = 0
				next_page = page + 1
			else
				page = params[:page]
				next_page = page.to_i + 1
			end
			page_size = 2
		if params[:show_id]
			show = @showtheme.get_show_by_id(params[:show_id])
			
			 if show.episodes_flag = "yes"
			url = "http://"+get_ott_base_url+"/catalogs/shows/"+show.ott_id + "/episodes?region=any&status=any&page="+page+"&page_size="+page_size+"auth_token="+ \
						current_bg.ott_auth_token
			response = http_request("get",{},url,60)
			if response["code"] == 200
				catalog = response["body"]["data"]
				episodes = response["body"]["data"]["items"]
				episodes.each do |mid|
					mid = mid["content_id"]
					media = Media.find_by(:ott_id => mid)
					msid = media.show_id
					if msid.nil?
						media.show_id = params[:show_id]
						media.save
					end
				end
			end
			media_list = Media.where(:bg_id=>@bg.id).where(:show_id => params[:show_id]).where(:is_removed => false).order_by(:created_at => 'desc').page(params[:page]).per(params[:page_size])
			render json: {:list => media_list, :total_count => media_list.total_count, :total_pages => media_list.total_pages}
			end	
			
			# 	render json: {:list => episodes}
			# end
			
		else
			@subcategory = (@sub.get_subcategory_by_id(params[:subcategory_id]))
		end	
	end	

	def get_all
		@bg = current_bg
		if params[:show_id]

		else #episodes of subcategory
			@subcategory = (@sub.get_subcategory_by_id(params[:subcategory_id])) || (@sub.get_subcategory_by_ott_id(params[:subcategory_id]))
			
			url = "http://"+get_ott_base_url+"/catalogs/shows/subcategories/"+\
						@subcategory["ott_id"]+"/episodes?region=any&status=any&auth_token="+ \
						current_bg.ott_auth_token
			response = http_request("get",{},url,60)
			if response["code"] == 200
				episodes = response["body"]["data"]["items"]
				episodes.each do |episode|
					mid = episode["content_id"]
					media = Media.find_by(:ott_id => mid)
					showid = @showtheme.get_show_by_ott_id(episode["show_theme_id"])
					msid = media.show_id
					if msid.nil? 
						media.show_id = showid.id
						media.save
					end
					mssid = media.subcategory_id
					if mssid.nil?
						media.subcategory_id = params[:subcategory_id]
						media.save
					end
					# episode["show_name"] = @showtheme.get_show_by_ott_id(episode["show_theme_id"]).title
					# episode["subcategory_name"] = response["body"]["data"]["title"]
					# episode["catalog_name"] = @catalog_obj.get_catalog_by_ott_id(episode["catalog_id"]).name
				end	
		    end		
			media_list = Media.where(:bg_id=>@bg.id).where(:subcategory_id => params[:subcategory_id]).where(:is_removed => false).order_by(:created_at => 'desc').page(params[:page]).per(params[:page_size])
			render :json => {:list => media_list, :total_count => media_list.total_count, :total_pages => media_list.total_pages}	
		end	
	end	
end

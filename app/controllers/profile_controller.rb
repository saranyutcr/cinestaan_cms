class ProfileController < ApplicationController
	include ApplicationHelper
	
	before_filter :login_check, :except => []
	def view_trial
		render "profile/view_trial"
	end
	def create
		status = true
		error = ""
		bg = current_bg

	    duplicate_profile = Profile.where(:name => params[:name].strip, :bg_id => bg.id)
	   	if duplicate_profile.count !=0
	   	status = false
	   	error = "Profile Already Exists!"
	   	end

		if status
			profile = Profile.new
			profile.name = params[:name].strip
			profile.video_bitrate = params[:v_bitrate]
			profile.audio_bitrate = params[:a_bitrate]
			profile.container = params[:container]
			profile.video_codec = params[:vcodec]
			profile.audio_codec = params[:acodec]
			profile.height = params[:height]
			profile.width = params[:width]
			profile.bg_id = bg.id
			profile.is_system_profile = false
			profile.save
		end
		if status
			render json: {} , status: :ok
		else
			render :json => {:error => error}.to_json, :status => 500
		end
	end
	def fetch
	end
	def delete
		profile = Profile.find(params[:profile_id])
		profile.destroy
		render json: {} , status: :ok
	end
	def fetch_all
		@bg = current_bg
		profile_list = []
		Profile.where(:is_system_profile => true).order_by(:created_at => 'desc').each do |profile|
			profile_list << profile
		end
		Profile.where(:bg_id => @bg.id).order_by(:created_at => 'desc').each do |profile|
			profile_list << profile
		end
		profile_list = Kaminari.paginate_array(profile_list).page(params[:page]).per(params[:page_size])
		render json: {:list => profile_list, :total_count => profile_list.total_count, :total_pages => profile_list.total_pages} , status: :ok
	end
	def new
		@bg = current_bg
	end
	def view_all
		@bg = current_bg
	end
end

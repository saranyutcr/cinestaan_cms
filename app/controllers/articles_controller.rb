class ArticlesController < ApplicationController
	include ApplicationHelper
	include ArticlesHelper

	before_filter :set_objects

	def set_objects
		@article = ArticleTheme.new
	end 

	def new
		status = true
		@bg = current_bg
    	@catalog_id = params[:catalog_id]


		@catalog = Catalog.find_by(:id => @catalog_id)
		@catalogtheme = @catalog.theme
		

		url = "http://"+get_ott_base_url+"/catalogs/"+@catalog.ott_id+"?auth_token="+@bg.ott_auth_token
		response = http_request("get",{},url,60)
		if !response["status"]
			status = response["status"]
			error = response["error"]
		end


		if status
		    @catalog = response["body"]["data"]	

		    @t_ott_id = response["body"]["data"]["tenant_ids"]
		    @videolist_tags = response["body"]["data"]["videolist_tags"]

		    @tenant_list = []
		    @accesscontrol = []
		    @valvideolist_tags = []

		    @t_ott_id.each do |obj|
		  	  Tenant.where(:ott_id=>obj).each do |tenan|
			  	
			  	h = {}
			 	h["id"] = tenan.id
			 	h["name"] = tenan.name
			 	h["ott_id"] = tenan.ott_id

				h ["genres"] =[]
				response["body"]["data"]["genres"].keys.each do |g|
		          h["genres"] = (response["body"]["data"]["genres"][tenan.ott_id]).map{|gen| gen["name"] } if g == h["ott_id"]
				end

				h ["languages"] =[]
				response["body"]["data"]["languages"].keys.each do |l|
				  h ["languages"] = (response["body"]["data"]["languages"][tenan.ott_id]).map{|lan| lan["name"] } if l == h["ott_id"]
				end 
		    	
			    h["applications"] = []
			    App.where(:tenant_id => tenan.id).each do |tapp|
			      if @catalog["application_ids"].include? tapp.ott_id	
			            application = {}
			            application["a_id"] = tapp.id
					    application["a_name"] = tapp.name
					    application["a_tid"] = tapp.tenant_id
					    h['applications'] << application
				    end
			    end

		 		@tenant_list << h

		 		catalogs = Catalog.find_by(:bg_id => @bg.id, :theme => "access_control", :tenant_id => tenan.id.to_s)
		 		acurl = "http://"+get_ott_base_url+"/catalogs/"+catalogs.ott_id+"/items?auth_token="+@bg.ott_auth_token+"&status=any&region=any"
				acresponse = http_request("get",{},acurl,60)
				if acresponse["code"] == 200
				accesscontrols = acresponse["body"]["data"]["items"]

				accesscontrols.each do |acc|
					ac = {}
					ac["name"] = tenan.name
					ac["title"] = acc["title"]
					ac["content_id"] = acc["content_id"]
					@accesscontrol << ac
				end
				end  #end of acc if
		      end
	        end
         end	

		url = "http://" + get_ott_base_url + "/people?auth_token=" + current_bg.ott_auth_token
		response = http_request("get",{},url,60)
		if response["code"] == 200
			@people = response["body"]["data"]["items"]
			@total_people_count = response["body"]["data"]["total_people_count"]
		end
	end

	def create
		if params[:utc_published_date] 
			if params[:utc_published_date] == "undefined"
				params[:utc_published_date] = ""
			else
				t = params[:utc_published_date].to_i / 1000
				params[:utc_published_date] =  Time.at(t).utc
			end	
		end	

		if params[:utc_unpublished_date] 
			if params[:utc_unpublished_date] == "undefined"
				params[:utc_unpublished_date] = ""
			else	
				t = params[:utc_unpublished_date].to_i / 1000
				params[:utc_unpublished_date] =  Time.at(t).utc
			end	
		end
		params[:bg_obj] = current_bg
		params[:cms_catalog_obj] = Catalog.find(params[:catalog_id])

		status,msg = validate_article_params(params)

		if status
			status,msg = @article.create_at_ott(params)
		end	

		if status
			render json: {} , status: :ok
		else
			render :json => {:error => msg}.to_json, :status => 500
		end
	end

	def edit
		@bg = current_bg
		@edit_media = ArticleTheme.find(params[:article_id]) || ArticleTheme.find_by(:ott_id => params[:article_id])
		catalog = Catalog.find_by(:ott_id => @edit_media.ott_catalog_id)
         
    url = "http://"+get_ott_base_url+"/catalogs/"+@edit_media.ott_catalog_id+"/items/"+@edit_media.ott_id+"?auth_token="+@bg.ott_auth_token+"&status=any&region=any"
    response = http_request("get",{},url,60)
            
    if response["code"] == 200
    	@item = response["body"]["data"]
      if !@item['published_date'].blank?
      	@item['published_date'] = @item['published_date'].to_datetime.utc.to_i * 1000
      end
      	
      if !@item['unpublished_date'].blank?
      	@item['unpublished_date'] = @item['unpublished_date'].to_datetime.utc.to_i * 1000
      end

      catalog = Catalog.find_by(:ott_id => @edit_media.ott_catalog_id)
      item_apps = App.any_in(:ott_id => @item["app_ids"]).as_json.map {|h| h["_id"]}

      #get catalog info from ott
      url = "http://"+get_ott_base_url+"/catalogs/"+@edit_media.ott_catalog_id+"?auth_token="+@bg.ott_auth_token
      response = http_request("get",{},url,60)
      if response["code"] == 200
      	@catalog = response["body"]["data"]

      	tenants_data = []
      	@accesscontrol = []
      	@catalog["tenant_ids"].each do |tenant|
      		t = Tenant.find_by(:ott_id => tenant)
      	  h = {}
      	  h["name"] = t.name
      	  h["cms_id"] = t.id
      	  h["ott_id"] = t.ott_id
      	  h["languages"] = (@catalog["languages"][tenant]).map{ |h| h["name"] }
      	  h["genres"] = (@catalog["genres"][tenant]).map{ |h| h["name"] }
      	  h["apps"] = []
      	  App.where(:tenant_id => t.id).each do |app|
      	  	id = app.id
				  	applications = catalog.app_id
				  	if applications.include?("#{id}")
				  		app_h = {}
		          app_h["app_id"] = app.id
						  app_h["app_name"] = app.name
						  app_h["app_tid"] = app.tenant_id
						  h["apps"] << app_h
				  	end	
      	  end	
      	  tenants_data << h
      	  catalogs = Catalog.find_by(:bg_id => @bg.id, :theme => "access_control", :tenant_id => t.id.to_s)
			 		acurl = "http://"+get_ott_base_url+"/catalogs/"+catalogs.ott_id+"/items?auth_token="+@bg.ott_auth_token+"&status=any&region=any"
					acresponse = http_request("get",{},acurl,60)
					if acresponse["code"] == 200
						accesscontrols = acresponse["body"]["data"]["items"]
						accesscontrols.each do |acc|
							ac = {}
							ac["name"] = t.name
							ac["title"] = acc["title"]
							ac["content_id"] = acc["content_id"]
							@accesscontrol << ac
						end
					end

      	end	
      	@item.merge!({"catalog_cms_id" => catalog["_id"],
          "item_apps" => item_apps,
          "tenants_data" => tenants_data,
          "catalog_name" => catalog.name+"["+catalog.theme+"]",
          "catalog_custom_tags" => @catalog["custom_tags"],
          "catalog_theme" => catalog["theme"],
          "item_genres" => @item["genres"]
          })

      	url = "http://" + get_ott_base_url + "/people?auth_token=" + @bg.ott_auth_token
		response = http_request("get",{},url,60)
		if response["code"] == 200
			@people = response["body"]["data"]["items"]
			render "edit"
			# @total_people_count = response["body"]["data"]["total_people_count"]
		# else
		# 	render js: "alert('Can not edit this item!');"
		end
	      
      else
        render js: "alert('Can not edit this item!');"	
      end	
    else
    	#handle error     
      render js: "alert('Can not edit this item!');"
    end	
            
	end

	def update
		if params[:method] == "update"
			if params[:utc_published_date] 
				if params[:utc_published_date] == "undefined"
					params[:utc_published_date] = ""
				else
					t = params[:utc_published_date].to_i / 1000
					params[:utc_published_date] =  Time.at(t).utc
				end	
			end	

			if params[:utc_unpublished_date] 
				if params[:utc_unpublished_date] == "undefined"
					params[:utc_unpublished_date] = ""
				else	
					t = params[:utc_unpublished_date].to_i / 1000
					params[:utc_unpublished_date] =  Time.at(t).utc
				end	
			end
			params[:bg_obj] = current_bg
			params[:cms_catalog_obj] = Catalog.find(params[:catalog_id])

			status,msg = validate_article_params(params)
			if status
				article = @article.get_by_id(params[:id])	
				if article			
					status,msg = article.update_at_ott(params)
				else
					status = false
				  msg	= "Article does not exist!"
				end	
			end	

			if status
				render json: {} , status: :ok
			else
				render :json => {:error => msg}.to_json, :status => 500
			end
		end	

		if params[:method] == "delete"
			status = true
			m = @article.get_data_by_ott_id(params[:id])
			if m

			  #delete existing delayed jobs if presents
				if m.ott_publish_req_job_id
					Delayed::Job.where(:id => m.ott_publish_req_job_id).delete_all
				end	

				if m.ott_unpublish_req_job_id
					Delayed::Job.where(:id => m.ott_unpublish_req_job_id).delete_all
				end
			  
			  if m.ott_id
			  	status,error = m.delete_at_ott
			  end
			  
				if status
				  m.delete
				  render json: {} , status: :ok
				else
				  render :json => {:error => "Delete action failed!"}.to_json, :status => 500	  	
				end	
			  
			else
			  render :json => {:error => "Article Not Found!!"}.to_json, :status => 500
			end

		end	

	end	
		
end

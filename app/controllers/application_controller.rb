class ApplicationController < ActionController::Base
  protect_from_forgery
  skip_before_filter  :verify_authenticity_token
  helper_method :current_user
  helper_method :current_bg, :get_cms_managers
  helper_method :content_owner_details
  
  before_filter :set_access
  before_filter :login_check, except: [:login_form, :signup_form, :forgot_form, :create]

	def set_access
  		response.headers["Access-Control-Allow-Origin"] = "*"
  		response.headers["Access-Control-Allow-Methods"] = "*"
  		response.headers["Access-Control-Request-Method"] = "*"
	end
	def current_user
		if params[:auth_token]
			@current_user ||= User.where(:auth_token => params[:auth_token]).first
		else
	  		@current_user ||= User.where(:id => session[:user_id]).first if session[:user_id]
	  	end
	end
	def current_bg
		if current_user
			if current_user.roles.include?"cms_manager"
				if session[:bg_id]
					@current_bg = BusinessGroup.find(session[:bg_id])
				else
					@current_bg = nil
				end
			else
				@current_bg = BusinessGroup.find(current_user.bg_id)
			end
		else
			@current_bg = nil
		end
	end

	def get_cms_managers
		@cms_managers = User.where(:roles => "cms_manager").as_json.map { |h| h["_id"] }
	end	

	def login_check
		if !current_user
			redirect_to "/"
		end
	end
	def http_request(method,body,url,timeout_at)
		response_status = true
		response_error = ""
		response_body = nil
		response_code = nil
		ret = nil
		begin
			Timeout::timeout(timeout_at) do
				case method
				when "post"
					ret = Typhoeus::Request.post(url,:body => body.to_json,:headers => {"content-type" => "application/json","accept" =>"application/json"})
				when "put"
					ret = Typhoeus::Request.put(url,:body => body.to_json,:headers => {"content-type" => "application/json","accept" =>"application/json"})
				when "get"
					ret = Typhoeus::Request.get(url,:headers => {"content-type" => "application/json","accept" =>"application/json"})
				when "delete"
					ret = Typhoeus::Request.delete(url,:body => body.to_json,:headers => {"content-type" => "application/json","accept" =>"application/json"})
				end
				Rails.logger.debug ret.inspect
			end
		rescue Timeout::Error
			response_status = false
			response_error = "Request Timed Out"
			response_code = 408
		rescue Exception => e
			response_status = false
			response_error = "Exception Raised for HTTP Request"
			response_code = 500
		end
		if ret.options[:response_body] && ret.options[:response_body]!=""
			response_body = JSON.parse(ret.options[:response_body])
		else
			response_body = ""
		end
		if ret.options[:response_code]
			response_code = ret.options[:response_code]
			if response_code != 200
				response_status = false
				response_error = "Request Failed"
			end
		end
		response = {}
		response["status"] = response_status
		response["error"] = response_error
		response["body"] = response_body
		response["code"] = response_code
		return response
	end
	def content_owner_details
		status = true
		error = ""
		all_items = []
		co_details = []
		cata = Catalog.where(:theme=>"person").where(:layout_type=>"content_owner").to_a
		cata.each do |i|
		bg = BusinessGroup.find(i.bg_id)
		url = "http://"+get_ott_base_url+"/catalogs/"+i.ott_id+"/items?auth_token="+bg.ott_auth_token+"&status=any&region=any&page=0&page_size=30"
		response = http_request("get",{},url,60)
		if !response["status"]
			status = response["status"]
			error = response["error"]
		end
		if status
			data = response["body"]["data"]["items"]
			all_items << data
		end
		end
		all_items.flatten.each do |h|
			co_details << h["title"] + "-" + h["content_id"] + "-" + h["catalog_id"]
		end	

		return co_details
	end

end

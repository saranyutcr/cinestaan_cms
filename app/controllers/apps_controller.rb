class AppsController < ApplicationController
	include AppsHelper
	include SubscriptionHelper
	include ApplicationHelper
	
	def create
		status = true
		error = ""
		#validate parameters received
		#check for uniqueness of app name inside tenant
		#remote post tenant
		tenant = Tenant.find(params[:tenant_id])
		platforms = Array(params[:platforms])
		names = Array(params[:name])
		status,error,apps = create_apps(tenant,platforms,names)
		if status && apps.nil?
			status = false
			error = "Error in creating Apps"
		end
		if status && apps.empty?
			status = false
			error = "Error in creating Apps"
		end

		if status 
		    bg = current_bg
		    updatecatalog = create_subscription(bg,tenant)
			appid = App.find_by(:ott_id => apps[0].to_s)
			tenantid = appid.tenant_id
		    planitems = subscription_items(bg,tenantid,appid)
		end

		if status
			render json: {} , status: :ok
		else
			render :json => {:error => error}.to_json, :status => 500
		end
	end
	def fetch
	end
	def fetch_all
		@bg = current_bg
		tenant_id = params[:tenant_id]
		if tenant_id
			app_list = App.where(:bg_id => @bg.id,:tenant_id => tenant_id).order_by(:created_at => 'desc').page(params[:page])
		else
			app_list = App.where(:bg_id => @bg.id).order_by(:created_at => 'desc').page(params[:page])	
		end	
		# app_list = App.all_of(:tenant_id => params[:tenant_id]).order_by(:created_at => 'desc').page(params[:page])
		render json: {:list => app_list, :total_count => app_list.total_count, :total_pages => app_list.total_pages} , status: :ok
	end
	def update
		status = true
		error = ""
		case params[:method]
		when "delete"
			app = App.find(params[:app_id])
			tenant = Tenant.find(app.tenant_id)
			body = {}
			body["auth_token"] = tenant.ott_auth_token
			url = "http://"+get_ott_base_url+"/apps/"+app.ott_id
			response = http_request("delete",body,url,60)
			if !response["status"]
				status = response["status"]
				error = response["error"]
			end
			if status
				app.destroy
			end
		end
		if status
			render json: {} , status: :ok
		else
			render :json => {:error => error}.to_json, :status => 500
		end
	end
	def view_all
		@bg = current_bg
		@all_tenants = Tenant.where(:bg_id => @bg.id).order_by(:created_at => 'desc')

		env = Rails.env
		case env
		when "development"
			@webview = @bg.staging_webview
		when "preproduction"
			@webview = @bg.preprod_webview
		when "production"
			@webview = @bg.prod_webview
		end

		
		if @all_tenants.count == 1
			@all_tenants.each do |tent|
				@tid = tent.id
			end
		else
			@all_tenants
		end
	end
	def new
		#@tenants = Tenant.where(:bg_id=>current_bg.id)
		@tenants = Tenant.find_by(:id=>params[:tid])
	end
end

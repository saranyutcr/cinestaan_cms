class CatalogListsController < ApplicationController
	include ApplicationHelper
	include CatalogListsHelper
	
	before_filter :set_object
	before_filter :get_default, only: [:edit,:new, :view_all,:get_lists]

	def get_default
		bg_id = BusinessGroup.find_by(:name=>current_bg.name)
		bg_id = bg_id._id
		cat_obj = Catalog.find_by(:theme =>"message",:bg_id =>bg_id)
		if !cat_obj.nil?
			c_id = cat_obj.ott_id 
			url = get_ott_base_url+"/catalogs/"+c_id+"/items/cms-settings?auth_token="+current_bg.ott_auth_token + "&region=IN"
		response = http_request("get",{},url,60)
		if response["code"] ==  200
   			@default_tags = response["body"]["data"]
   		end
		url = get_ott_base_url+"/configurations?auth_token="+current_bg.ott_auth_token

   		response = http_request("get",{},url,60)
   		@default_data = response["body"]["data"]
   		end	
	end 

	def set_object
		@show = ShowTheme.new
		@sub  = Subcategory.new
	end	

	def new
		debugger
		@bg = current_bg
		@catalog_list = []
		Tenant.where(:bg_id=>@bg.id).each do |tenant|
			#Catalog.where(:tenant_id=>tenant.id.to_s).each do |catalog|
		    Catalog.where(:tenant_id=>tenant.id.to_s).not_in(:theme => [ "access_control","subscription","person","article","livetv","message","coupon"]).each do |catalog|
				h = {}
				# h["name"] = tenant.name+" - "+catalog.name+"  ["+catalog.theme+"]"
				h["name"] = catalog.name+"  [" +catalog.theme.capitalize+ " Theme" +"]"
				h["id"] = catalog.id 
				h["tenant_id"] = tenant.id
				@catalog_list << h
			end
		end

		s3uploadremovspace = @bg.name
		s3upload = s3uploadremovspace.gsub(" ", "_");
	    @signature = get_s3_upload_key(s3upload,@bg.bucket_name,@bg.domain_name)
	end
	def create
		if params["catalog_id"]
			params[:tenant_id] = params[:catalog_id].split("-")[1]
			params[:catalog_id] = params[:catalog_id].split("-")[0]
		end	

		status,obj = validate_list_params(params)
		 
		if status
	        c = nil
			cl = CatalogList.new
			cl.title = params[:title].strip.downcase
			cl.display_title = params[:display_title].strip
			cl.catalog_id = params[:catalog_id] ? params[:catalog_id] : ""
			cl.list_type = params[:type]
			if params[:tags]
				if !params[:tags].empty?
		          # if params[:tags].include?(",")
		          #   cl.tags = params[:tags].split(",")
		          # else  
		          #   cl.tags = [params[:tags]]
		          # end	
		          cl.tags = params[:tags]
		          # params[:tags] = cl.tags
		        else
		           cl.tags = ["catalog"]	
		           params.tap {|h| h.delete(:tags)}
				end	
			end	
			cl.list_order = params[:list_order]
			c = Catalog.find(cl.catalog_id) if params[:catalog_id]
			cl.tenant_id = [params[:tenant_id]] if params[:tenant_id]
			#cl.bg_id = c.bg_id
			cl.bg_id = current_bg.id
			# #cl.save
			status,obj = cl.create_at_ott(params,c)
		end
			
		if status
		  #cl.save
		  render json: {} , status: :ok
		else
		  cl.destroy if cl	
		  render :json => {:error => obj}.to_json, :status => 500  
	    end
	end
	def fetch_list
		status = true
		error = ""
		catalog_list = CatalogList.find(params[:catalog_list_id])
		bg = BusinessGroup.find(catalog_list.bg_id)
			if params[:page].nil?
				page = 0
				next_page = page + 1
			else
				page = params[:page]
				next_page = page.to_i + 1
			end
			page_size = 25
		#catalog remote fetch
		url = "http://"+get_ott_base_url+"/catalogs/"+catalog.ott_id+"/lists?auth_token="+bg.ott_auth_token+"&catalog_list_items=true&page="+page+"&page_size="+page_size
		response = http_request("get",{},url,60)
		if !response["status"]
			status = response["status"]
			error = response["error"]
		end
		if status
			content_list = response["body"]["data"]["catalog_list_items"]
			render json: {:list => content_list, :total_count => content_list.total_count, :total_pages => content_list.total_pages} , status: :ok
		end
	end
	def fetch_all
		@bg = current_bg
		
		cl_list = CatalogList.all_of(:bg_id => current_bg.id).order_by(:created_at => 'desc').page(params[:page]).per(params[:page_size])

		if params[:search] && params[:search] != ""
			cl_list = cl_list.where(:title => /#{params[:search]}/i)
		end 
		
		if params[:catalog] && params[:catalog] != "" 
			cl_list = cl_list.where(:catalog_id => /#{params[:catalog]}/i)
		end

		if params[:tag] && params[:tag] != ""
			cl_list = cl_list.where(:tags => /#{params[:tag]}/i)
		end

		lists = cl_list.as_json.map do |list|
            catalog = Catalog.find(list["catalog_id"])
            name = catalog ? catalog.name : ""
            t_names = []
            list["tenant_id"].each do |tenant|
            	if tenant
                	t_names << Tenant.find(tenant).name	
            	end
            end	
            list.merge!({"catalog_name" => name , "tenant_names" => t_names })
		end	
		render json: {:list => lists, :total_count => cl_list.total_count, :total_pages => cl_list.total_pages} , status: :ok
	end

	def view_all
	  #@tenant_id = params[:tenant_id]
	    @bg = current_bg
	    if params[:page].nil?
				page = 1
				next_page = page + 1
			else

				page = params[:page]
				next_page = page.to_i + 1
			end
			page_size = params[:page_size] ? params[:page_size] : 25
			params[:page_size] = page_size

	    cl_list = CatalogList.all_of(:bg_id => current_bg.id).order_by(:created_at => 'desc').page(page).per(page_size).as_json
	    next_cl_list = CatalogList.all_of(:bg_id => current_bg.id).order_by(:created_at => 'desc').page(next_page).per(page_size).as_json


		if params[:search] && params[:search] != ""
			cl_list = cl_list.where(:title => /#{params[:search]}/i)
			next_cl_list = next_cl_list.where(:title => /#{params[:search]}/i)
		end 
		
		if params[:catalog] && params[:catalog] != "" 
			cl_list = cl_list.where(:catalog_id => /#{params[:catalog]}/i)
			next_cl_list = next_cl_list.where(:catalog_id => /#{params[:catalog]}/i)
		end

		if params[:tag] && params[:tag] != ""
			cl_list = cl_list.where(:tags => /#{params[:tag]}/i)
			next_cl_list = next_cl_list.where(:tags => /#{params[:tag]}/i)
		end

		if params[:list_type] && params[:list_type] != ""
			cl_list = cl_list.where(:list_type => params[:list_type])
			next_cl_list = next_cl_list.where(:list_type => params[:list_type])
		end

		@items = []
			cl_list.each do |list|
            	catalog = Catalog.find(list["catalog_id"])
            	catalog_name = catalog ? catalog.name : ""
            	list["catalog_name"] = catalog_name
            	t_names = []
            	list["tenant_id"].each do |tenant|
            		if tenant
                		t_names << Tenant.find(tenant).name	
            		end
            end	
            @items << list
		end	
		@next_items = next_cl_list
	end

	def show_lists
        cl_list = CatalogList.all_of(:tenant_id => params[:tenant_id]).order_by(:created_at => 'desc').page(params[:page])
		lists = cl_list.as_json.map do |list|
            catalog = Catalog.find(list["catalog_id"])
            name = catalog ? catalog.name : "" 
            t_names = []
            list["tenant_id"].each do |tenant|
                t_names << Tenant.find(tenant).name	
            end	
            list.merge!({"catalog_name" => name , "tenant_names" => t_names })
		end
		data_list = Kaminari.paginate_array(lists).page(params[:page])
        render json: {:list => data_list, :total_count => data_list.total_count, :total_pages => data_list.total_pages}
        #render json: {:list => lists}
	end 
	
	def update
		status = true
		error = ""
		case params[:method]
		when "delete"
			cataloglist = CatalogList.find(params["catalog_list_id"])
			bg_auth_token = current_bg.ott_auth_token
			url = "http://" + get_ott_base_url+"/catalog_lists/"+cataloglist.ott_id+"?auth_token="+bg_auth_token
            response = http_request("delete",{},url,60)
            if response["code"] == 200
            	cataloglist.destroy
            	render json: {} , status: :ok
            else	
            	error = response["body"]["error"]["message"]
            	render :json => {:error => error}.to_json, :status => 500
            end	
        when "edit"
            change_flag = params[:catalog_flag]
            case change_flag
            when "yes"
            	params[:tenant_id] = params[:catalog_id].split("-")[1]
				params[:catalog_id] = params[:catalog_id].split("-")[0]

            when "no"	
            	if params.has_key? "existing_catalog_id"
            		params[:tenant_id] = params[:existing_catalog_id].split("-")[1]
					params[:catalog_id] = params[:existing_catalog_id].split("-")[0]
            	end	
            end	

            if params[:tags]
				if params[:tags].empty?
		           tags = ["catalog"]	
		           params.tap {|h| h.delete(:tags)}
				end	
			end	

   #          if !params[:tags].empty?
	  #         	if params[:tags].include?(",")
	  #           	tags = params[:tags].split(",")
	  #         	else  
	  #           	tags = [params[:tags]]
	  #         	end	
	  #         	params[:tags] = tags
	  #       else
	  #          	tags = ["catalog"]	
	  #          	params.tap {|h| h.delete(:tags)}
			# end	
			cl = CatalogList.find(params[:catalog_list_id])
			status,error = cl.update_at_ott(params)
			if status
            	render json: {} , status: :ok 
            else
            	render :json => {:error => error}.to_json, :status => 500
            end	
        end    
	end

	def view_catalog_list
		@catalog_list = CatalogList.find(params[:list_id])
		@catalog_list_ott_id = @catalog_list.ott_id
		bg = BusinessGroup.find(@catalog_list.bg_id)
	    	if params[:page].nil?
				page = 0
				next_page = page + 1
			else
				page = params[:page]
				next_page = page.to_i + 1
			end
			page_size = params[:page_size] ? params[:page_size] : 25

		catalog_id = @catalog_list.catalog_id
		list_ott_id = @catalog_list.ott_id
		tags = @catalog_list.tags
		url = "http://"+get_ott_base_url+"/catalog_lists/"+list_ott_id+"?auth_token="+bg.ott_auth_token+"&tag=any&page="+page.to_s+"&page_size="+page_size.to_s
		response = http_request("get",{},url,60)
		items = response["body"]["data"]["catalog_list_items"]

		@list_items = []
		items.each do |item|
			if item["theme"] == "catalog"
				cms_id = Catalog.find_by(:ott_id => item['catalog_id'])._id rescue false

			elsif item["theme"] == "catalog_list"
				cms_id = CatalogList.find_by(:ott_id => item['list_id'])._id rescue false
			end
			item["cms_id"] = cms_id if cms_id
			@list_items << item	
		end

		@cataloglist_items = []
		if !@list_items.empty?
			@list_items.each do |item|
				if item["theme"] == "catalog_list"
					item[:catalog_title] = item["display_title"]
				elsif item["theme"] == "custom_item"
 					item[:catalog_title] = item["display_title"]
				else
					if item["theme"] == "videolist"
						item[:catalog_title] = item["display_title"]
					else
 						item[:catalog_title] = Catalog.find_by(:ott_id => item["catalog_id"]).name								    		
 					end
				end
				@cataloglist_items << item
			end
		else
			@cataloglist_items = []    	
	    end
	    next_url = "http://"+get_ott_base_url+"/catalog_lists/"+list_ott_id+"?auth_token="+bg.ott_auth_token+"&tag=any&page="+next_page.to_s+"&page_size="+page_size.to_s
	    response = http_request("get",{},next_url,60)
		@next_items = response["body"]["data"]["catalog_list_items"]
	end
    
    def fetch_items
	    catalog_list = CatalogList.find(params[:list_id]) 
	    bg = BusinessGroup.find(catalog_list.bg_id)
	    	if params[:page].nil?
				page = 0
				next_page = page + 1
			else
				page = params[:page]
				next_page = page.to_i + 1
			end
			page_size = 25

		catalog_id = catalog_list.catalog_id
		list_ott_id = catalog_list.ott_id
		tags = catalog_list.tags
		#url = "http://"+get_ott_base_url+"/catalog_lists?auth_token="+bg.ott_auth_token+"&catalog_list_items=true&tag="+tags[0]
		#url = "http://"+get_ott_base_url+"/catalog_lists/"+list_ott_id+"?auth_token="+bg.ott_auth_token+"&catalog_list_items=true&item_type=all&tag="+tags[0]
		url = "http://"+get_ott_base_url+"/catalog_lists/"+list_ott_id+"?auth_token="+bg.ott_auth_token+"&tag=any&page="+page+"&page_size="+page_size
		# if tags.include? "catalog"
		# 	catalog = Catalog.find(catalog_id)
		#     url = "http://"+get_ott_base_url+"/catalogs/"+catalog.ott_id+"/lists?auth_token="+bg.ott_auth_token+"&catalog_list_items=true"
		#    else
		# end	

		response = http_request("get",{},url,60)
		#items = response["body"]["data"]["items"]
		items = response["body"]["data"]["catalog_list_items"]

		items.each do |item|
			if item["theme"] == "catalog"
				cms_id = Catalog.find_by(:ott_id => item['catalog_id'])._id rescue false

			elsif item["theme"] == "catalog_list"
				cms_id = CatalogList.find_by(:ott_id => item['list_id'])._id rescue false
			end

			item[:cms_id] = cms_id if cms_id
			
		end

		#item = items.select {|item| item["list_id"] == list_ott_id }

		#@data = item.first["catalog_list_items"]
		@data = items

		#return false
		if !@data.empty?
			@data.map do |item|
				if item["theme"] == "catalog_list"
					item[:catalog_title] = item["display_title"]
				elsif item["theme"] == "custom_item"
 					item[:catalog_title] = item["display_title"]
				else
					if item["theme"] == "videolist"
						item[:catalog_title] = item["display_title"]
					else
 						item[:catalog_title] = Catalog.find_by(:ott_id => item["catalog_id"]).name								    		
 					end
				end
			end
			@content_list = @data
=begin
			render :json => { :list => content_list, :total_count => content_list.total_count, :total_pages => content_list.total_pages } , status: :ok
=end
		else
			@content_list = []
			#render :json => { :list => [], :total_count => 0, :total_pages => 0 } , status: :ok
			# error = "No data to display"
		    # render :json => {:error => error}.to_json, :status => 500    	
	    end
	    
    end 

	def get_lists
		@bg = current_bg
		
		s3upload = ""
		
		if params[:catalog_id] && !params[:theme]
			item = Catalog.find(params[:catalog_id])
		elsif params[:cataloglist_id]
			item = CatalogList.find(params[:cataloglist_id])		
		elsif params[:content_id]
			item = Media.find_by(:ott_id=> params[:content_id]) || Media.find(params[:content_id])
		elsif params[:subcategory_id]
			item = @sub.get_subcategory_by_id(params[:subcategory_id])
		elsif params[:user_theme_id]
			url = "http://"+get_ott_base_url+"/catalogs/"+params[:catalog_id]+"/items"+params[:user_theme_id]+"?auth_token="+bg.ott_auth_token
	    response = http_request("get",{},next_url,60)

		else # Adding show to list
			item = @show.get_show_by_id(params[:show_id]) || @show.get_show_by_ott_id(params[:show_id])
		end

		if params[:theme] == "person" || params[:theme] == "livetv"
			@catalog = Catalog.find_by(ott_id: params[:catalog_id])
		elsif params[:catalog_id]
			@catalog = Catalog.find(params[:catalog_id])
		else	
			@catalog = Catalog.find_by(:ott_id=>item.ott_catalog_id) unless params[:cataloglist_id]
		end

		if params[:cataloglist_id]
			tenant_ids = item.tenant_id
		else
			tenant_ids = @catalog.tenant_id 
		end

		#tenant_ids = @catalog.tenant_id 

		if params[:cataloglist_id]
			lists = CatalogList.any_in(:tenant_id => tenant_ids).where(:_id.ne => params[:cataloglist_id])
		else
			lists = CatalogList.any_in(:tenant_id => tenant_ids)
		end

		if params[:catalog] && params[:catalog] != ""
			lists = lists.where(:catalog_id => params[:catalog])
		end

		if params[:tags] && params[:tags] != ""
			lists = lists.where(:tags => params[:tags])
		end

		#lists = CatalogList.any_in(:tenant_id => tenant_ids)

		@cl = []

		# logger.debug "====================> Your List size is:: #{lists.length} <====================="
		
		lists.each do |list|
			#handle for catalogs
			if params[:catalog_id] && !params[:theme]	
				content_type = "catalog"
				theme = "catalog"
				s3upload = @bg.name+"/Catalog/"+@catalog.name

			elsif params[:cataloglist_id]
				content_type = "catalog_list"
				theme = "catalog_list"
				s3upload = @bg.name+"/Catalog"+list.title

			elsif params[:content_id]
				if @catalog.theme == "show"
					content_type = "episode"
					s3upload = get_s3upload_url_for_episode(item,@catalog,@bg)
				else
					if params[:videolist] == "yes"

						media = Media.find(item.parent_media_id)
						title = media.title
						display_title = item.title
						friendly_id = display_title.strip.gsub(/\s/,"-").downcase
						theme = "videolist"
						content_type = "videolist"
					else
						content_type = @catalog.theme
					end
					s3uploadremovspace = @bg.name+"/"+@catalog.name+"/"+item.title+"/images/banners"
					s3upload = s3uploadremovspace.gsub(" ", "_");
				end
			elsif params[:subcategory_id]
				content_type = "subcategory"
				s3upload = get_s3upload_url_for_subcategory(item,@catalog,@bg)
			elsif params[:theme] == "person"
				content_id = params[:person_id]
				content_type = "person"
				theme = "person"
				s3upload = @bg.name+"/Catalog"+list.title
			elsif params[:theme] == "livetv"
				content_id = params[:channel_id]
				content_type = "livetv"
				theme = "livetv"
				s3upload = @bg.name+"/Catalog"+list.title
			else
				content_type = "show"
				s3upload = get_s3upload_url_for_show(item,@catalog,@bg)
			end

			puts "======s3upload path======"
			p s3upload

			if params[:theme] == "person" || params[:theme] == "livetv"
				items = [{"theme" => theme,"content_id" => content_id, "catalog_id" => @catalog.ott_id, "content_type" => content_type}]
			elsif params[:catalog_id]
				items = [{"theme" => theme, "content_id" => @catalog.ott_id, "catalog_id" => @catalog.ott_id, "content_type" => content_type }]
			elsif params[:cataloglist_id]
				items = [{"theme" => theme, "content_id" => item.ott_id, "list_id" => item.ott_id, "content_type" => content_type }]				
			else
				if params[:videolist]
					items = [{"display_title" => display_title, "friendly_id" => friendly_id , "item_title"=> title,"content_id" => item.ott_id, "catalog_id" => item.ott_catalog_id, "theme" => theme, "content_type" => content_type}]
				else
					items = [{"item_title"=> item.title,"content_id" => item.ott_id, "catalog_id" => item.ott_catalog_id, "content_type" => content_type}]
				end
			end
			
			puts 

			# if params[:cataloglist_id]
			# 	c = CatalogList.find_by(:id => list.catalog_id)	
			# else
			# 	c = Catalog.find_by(:id => list.catalog_id)	
			# end
		
			c = Catalog.find_by(:id => list.catalog_id)	

			list[:tags].delete("catalog")

			@cl << {
				"display_title" => list[:display_title],
				"title" => list[:title],
				"catalog_title" => c ? c[:name] : "",
				"catalog_id" => c ? c[:ott_id] : "",
				"cms_catalog_id" => list.catalog_id,
				"list_id" => list[:ott_id],
				"list_items" => items,
				"tags" => list[:tags].blank? ? [] : list[:tags].join(",")
			}
		end
		@signature = get_s3_upload_key(s3upload,@bg.bucket_name,@bg.domain_name)	

		# render partial: "view_lists"
		render "catalog_lists/add_list_item"
		# flash[:notice] = "No List Found. Please Create The Catalog List!"
	end	


	def add_items
	  status = true
      error = ""	
	  @bg = current_bg
	  maxtime = 60
	  #return false
	  success_list = []
	  failed_list = []
	  for i in 0...params[:list].length do 
		list = eval(params[:list][i]) 
	      list_id = list["list_id"]
	      	
	      item = list["list_items"][0].merge({"order_no" => params[:item_order].to_i})
	      if !params[:banner_image].blank?
	      	item["banner_image"] = params[:banner_image]
	      	item["link"] = params[:link]
	      end

	      if !params[:overlay_text].blank?
			item["overlay_text"] = params[:overlay_text]   	
	      end
	      	
	      body = {}
	      body["auth_token"] = @bg.ott_auth_token
	      body["items"] = {}
	      #body["items"]["list_items"] = [item]
	      body["items"]["delete_existing_data"] = to_boolean(params[:flag]) if params[:flag]
	      if item["theme"] == "videolist"
	      	#item["catalog_id"] = ""
	      	item["item_object"] = {}
	      	item["item_object"]["catalog"] = {}
	      	item["item_object"]["catalog"]["theme"] = "catalog"
	      	item["item_object"]["catalog"]["catalog_id"] = item["catalog_id"]
	      	item["item_object"]["catalog"]["item_id"] = item["content_id"]
	      	body["items"]["list_items"] = [item]
	      else
	      	body["items"]["list_items"] = [item]
	      end

	      url = "http://"+get_ott_base_url+"/catalog_lists/"+list_id+"/add"
	      response = http_request("put",body,url,maxtime)

	      if response["code"] == 200
	      	success_list.push(list['display_title'])
	      else
	      	failed_list.push(list['display_title'])
	      end
      end
      # if response["code"] == 200
      #   render json: { } , status: :ok
      # else 
      # 	error = response["body"]["error"]["message"]
      #   render :json => {:error => error}.to_json, :status => response["code"] 
      # end

      render :json => {:success_list => success_list , :failed_list => failed_list}, status: :ok	
	end 

	def delete_item
		if params[:list_id]
			items = [{:list_id => params[:list_id], :content_id=> params[:content_id]}]	
		else			
			items = [{:catalog_id => params[:catalog_id], :content_id=> params[:content_id]}]	
		end
      @bg = current_bg
      ott_list_id = params[:ott_list_id]
      body = {}
      body["auth_token"] = @bg.ott_auth_token
      body["items"] = {}
      body["items"]["list_items"] = items
      url = "http://"+get_ott_base_url+"/catalog_lists/"+ott_list_id+"/delete"
      response = http_request("put",body,url,60)
      if response["code"] == 200
      	render json: {} , status: :ok	
      else
      	error = response["body"]["error"]["message"]
        render :json => {:error => error}.to_json, :status => response["code"]
      end	
    end		

 	def edit
    	@bg = current_bg
        @list = CatalogList.find(params[:list_id])

        url = "http://"+get_ott_base_url+"/catalog_lists?auth_token="+@bg.ott_auth_token + "&tag=any"
    	response = http_request("get",{},url,60)
    	if response["code"] == 200
    		items = response["body"]["data"] 
    		items = items["items"]
    		items.each do |i|
    			if @list.ott_id == i["list_id"]
    				@list_info = i["thumbnails"]
    				@list["application_ids"] = i["application_ids"]
    			end
    		end
    	end	
    	s3uploadremovspace = @bg.name
    	s3upload = s3uploadremovspace.gsub(" ", "_");
	    @signature = get_s3_upload_key(s3upload,@bg.bucket_name,@bg.domain_name)
	    p "signature values"
	    p "#{@signature.inspect}"
        if @list
        	if !(@list.catalog_id).empty?
	        	@catalog = Catalog.find(@list.catalog_id) 
	        end	
	        # if @catalog
				@catalog_list = []
				tenant = Tenant.find(@list.tenant_id.first)
				Catalog.where(:tenant_id=>@list.tenant_id.first).not_in(:theme => [ "access_control","subscription","person","article","livetv","coupon","message"]).each do |catalog|
				#Catalog.where(:tenant_id=>@list.tenant_id.first).each do |catalog|
					h = {}
					# h["name"] = tenant.name+" - "+catalog.name+"  ["+catalog.theme+"]"
					h["name"] = catalog.name+"  ["+catalog.theme.capitalize+ " Theme" +"]"
					h["id"] = catalog.id
					h["ott_id"] = catalog.ott_id
					h["tenant_id"] = tenant.id
					@catalog_list << h
				end
		      	render "edit"
		    # else  	
		    # 	render js: "alert('Can not edit this item!');"
		    # end  	
      	else
      		render js: "alert('Can not edit this item!');"
      	end
    end

    	
	def to_boolean(str)
      return true if str == "true"
      return false if str == "false"
	end	

	def get_videotags
     catalog = Catalog.find(params[:catalog_id])
     @catalog_id = params[:catalog_id]
     @bg = current_bg
     url = "http://"+get_ott_base_url+"/catalogs/"+catalog.ott_id+"/videolist_tags"+"?auth_token="+@bg.ott_auth_token+"&region=IN&status=any"
     response = http_request("get",{},url,60)
     @video_list = response["body"]["data"]["items"]
      
	end	

	def videotags_items
		@bg = current_bg
		@category = params[:category]
		catalog = Catalog.find(params[:catalog_id])
		catalogott_id = catalog.ott_id
		media = Media.find_by(:ott_catalog_id => catalogott_id)
		url = "http://"+get_ott_base_url+"/catalogs/"+catalog.ott_id+"/videolists?auth_token="+@bg.ott_auth_token+"&region=IN&status=hanging&category="+@category
		response = http_request("get",{},url,60)
		@videolists = response["body"]["video_list"]
		render json: {:videolist => @videolists }
	end

	def videolist_items
		@bg = current_bg
	    @itemid	 = params[:videotag].split("-")[0]
		@category = params[:videotag].split("-")[1]
		@videolist_id = params[:videolist].split("-")[0]
		@catalog_id = params[:videolist].split("-")[1]
		
		metadata = {}
		catalog = Catalog.find(@itemid)
		catalogott_id = catalog.ott_id
		media = Media.find_by(:ott_catalog_id => catalogott_id)
		item_id = media.ott_id
		url = "http://"+get_ott_base_url+"/catalogs/"+catalog.ott_id+"/items/"+item_id+"?auth_token="+@bg.ott_auth_token+"&region=IN&status=any"
		response = http_request("get",{},url,60)
		@item = response["body"]["data"]
			metadata["title"] = @item["title"]
			metadata["featured_image"] = @item["thumbnails"]["l_large"]["url"]
			metadata["l_listing_image"] = @item["thumbnails"]["l_medium"]["url"]
			metadata["p_listing_image"] = @item["thumbnails"]["p_small"]["url"]
			metadata["category"]= @category
			metadata["regions"] = ["IN"]
			metadata["business_group_id"] = BusinessGroup.find(catalog.bg_id).ott_id
			metadata["description"] = @item["description"]
			metadata["duration"] = 60
			metadata["rating"] = @item["rating"]
			metadata["smart_url"] = "smart_url"
		    metadata["status"] = @item["status"]
		    
		    status,error = media.update_video_list(metadata)
			if status
            	render json: {} , status: :ok 
            else
            	render :json => {:error => error}.to_json, :status => 500
            end	
	end



	def reorder_items
		@bg = current_bg
		ott_list_id = params[:list_id]
		list_items = []
		params["list_items"].values.each_with_index do |i,index|
			list_items[index] = {"content_id" => i["content_id"], "theme" => i["theme"] ,"order_no" => i["order_no"].to_i, }
		end
		body = {}
		body["auth_token"] = @bg.ott_auth_token
		body["list_items"] = list_items
		url = "http://"+get_ott_base_url+"/catalog_lists/"+ott_list_id+"/update_items"
		response = http_request("put",body,url,60)
		if response["code"] == 200
			render json: {} , status: :ok	
		else
			error = response["body"]["error"]["message"]
			render :json => {:error => error}.to_json, :status => response["code"]
		end	

		end
end

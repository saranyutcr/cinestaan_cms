class UsersController < ApplicationController
	include UsersHelper
	include MediaHelper
	include ApplicationHelper
	skip_before_filter :login_check

	def new_password
		render "users/new_password"
	end
	def change_password
		user = User.authenticate(current_user.email, params[:password])
		if user == "Incorrect Password"
			render :json => {:error => "Incorrect Current password"}.to_json, :status => 500
	  	
	  	elsif user
	    	user.password = params[:new_password]
	    	user.save
	    	render json: {} , status: :ok
	  	else
	    	render :json => {:error => "Password didn't match"}.to_json, :status => 500
	  	end
	end
	def forgot_password
		render "users/forgot_password"
	end
	def update_password
		if User.where(:email => params[:email]).count > 0
			user = User.where(:email => params[:email]).first
			password = generate_password
			user.password = password
			user.save
			Delayed::Job.enqueue(SendSignupMail.new(user,password), :run_at => Time.now, :queue => "#{Rails.env}_cms")
			render json: {} , status: :ok
		else
			render :json => {:error => "This Email is not registered"}.to_json, :status => 500
		end
	end
	def new_trial
		render "users/new_trial"
	end
	def create_trial
		status = true
		error = ""
		if status
			if !params[:source_url].start_with?('ftp://')
				status = false
				error = "Error in Source URL Format"
			end
		end
		if status
			if !params[:email].match(/\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i)
				status = false
				error = "Invalid Email"
			end
		end
		if status
			trial_user = User.new
			trial_user.email = params[:email]
			password = generate_password
			trial_user.password = password
			trial_user.bg_id = BusinessGroup.where(:name => "Trial").first.id
			trial_user.roles = ["publisher"]
			trial_user.save
			Delayed::Job.enqueue(SendSignupMail.new(trial_user,password), :run_at => Time.now, :queue => "#{Rails.env}_cms")
			media = Media.new
			media.title = "Trial Video"
	  		media.user_id = trial_user.id
	  		media.user_role = trial_user.roles.first
	  		media.source_url = params[:source_url]
	  		media.publish_settings_id = PublishSettings.where(:name => "trial_pack").first.id
	  		media.bg_id = BusinessGroup.where(:name => "Trial").first.id
	  		media.status = "Created"
	  		media.save
	  		status,error = media.transcode_request
	  	end
	  	if status
			render json: {} , status: :ok
		else
			render json: {:error => error} , status: 500
		end
	end
	def check_email_trial
		render "users/check_email_trial"
	end
	def create
		status = true
		error = ""
		if status
			if !current_bg
				status = false
				error = "Business Group does not exist"
			end
		end
		if status
			if params[:password] != params[:password_confirm]
				status = false
				error = "Password confirmation mismatch"
			end
		end
		if status
			valid_email_regex = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z]+)*\.[a-z]+\z/i
			if !params[:email].match(valid_email_regex)
				status = false
				error = "Invalid Email Format"
			end
		end
		if status
			if User.find_by(:email => params[:email].strip)
				status = false
				error = "Email already registered"
			end
		end
		if status
			given_roles = params[:roles].split(",").map(&:strip)
			# if given_roles - ["admin","publisher","content_editor"] != []
			#new code for content owner
			if given_roles - ["admin","publisher","content_editor","content_owner","media_viewer"] != []
				status = false
				error = "One or more of the roles given are not valid roles"
			end
		end
		if status
			user = User.new
			user.email = params[:email].strip
			user.password = params[:password]
			user.isemail_verify = "NO"
			user.email_verification_key = get_email_verification_key params[:email]
			user.bg_id = current_bg.id
			user.roles = params[:roles].split(",").map(&:strip)
			user.has_public_access = false
			user.content_owner_id = params[:content_owner]
			user.first_name = params[:content_owner_name]
			# if status && params[:public_access]
			# 	if params[:public_access]=="on"
			# 		user.has_public_access = true
			# 	else
			# 		user.has_public_access = false
			# 	end
			# else
			# 	user.has_public_access = false
			# end
			user.save
			#Delayed::Job.enqueue(SendSignupMail.new(user,user.password), :run_at => Time.now, :queue => "#{Rails.env}_cms")
			Delayed::Job.enqueue(UserRegister.new(user), :run_at => Time.now, :queue => "#{Rails.env}_cms")
		end
		if status
			render json: {} , status: :ok
		else
			render json: {:error => error} , status: 500
		end
	end
	def delete
		user = User.find(params[:user_id])
		user.destroy
		render json: {} , status: :ok
	end
	def fetch_all
		status = true
		error = ""
		user_list = ""
		if current_user
			if current_user.roles.include?"cms_manager"
				user_list = User.where(:bg_id=>current_bg.id).order_by(:created_at => 'desc').page(params[:page]).per(params[:page_size])
			elsif current_user.roles.include?"admin"
				user_list = User.where(:bg_id=>current_user.bg_id).order_by(:created_at => 'desc').page(params[:page]).per(params[:page_size])
			else
				status = false
				error = "authorization failed"
			end
		else
			status = false
			error = "authentication failed"
		end
		if status
			render json: {:list => user_list, :total_count => user_list.total_count, :total_pages => user_list.total_pages} , status: :ok
		else
			render :json => {:error => error}.to_json, :status => 500
		end
	end

	def view_all
		@bg = current_bg
		status = true
		error = ""
		if params[:page].nil?
			page = 1
			next_page = page + 1
			params[:page] = page
		else
			page = params[:page]
			next_page = page.to_i + 1
		end
		page_size = params[:page_size] ? params[:page_size] : 25
			@user_list = []
			@next_user_list = []
		if current_user
			if current_user.roles.include?"cms_manager"
				@user_list = User.where(:bg_id=>current_bg.id).order_by(:created_at => 'desc').page(page).per(page_size)
				@next_user_list = User.where(:bg_id=>current_bg.id).order_by(:created_at => 'desc').page(next_page).per(page_size)
			elsif current_user.roles.include?"admin"
				@user_list = User.where(:bg_id=>current_user.bg_id).order_by(:created_at => 'desc').page(page).per(page_size)
				@next_user_list = User.where(:bg_id=>current_user.bg_id).order_by(:created_at => 'desc').page(next_page).per(page_size)
			else
				status = false
				error = "authorization failed"
			end
		else
			status = false
			error = "authentication failed"
		end
		render "view_all"
	end

	def new
		@bg = current_bg
		render "new"
	end

	def edit
		@bg = current_bg
		@user_id = params["user_id"]
		@user = User.find(@user_id)
		if (current_user.roles.exclude? "cms_manager") && (current_user.roles.include? "admin")
			params["co"]="yes"
		end
		render "edit"
	end

	def update
		@user = User.find(params[:user_id])
		if @user.update_fields(params)
			render json: {} , status: :ok , notice:"Successfully updated user"
		else
			render action:"edit"
		end
	end

	def content_owner_new
		@bg = current_bg
		render "content_owner_new"
	end

	def user_email_verify
		status = true
		error = ""
		userid = params[:id]
		admin = User.find_by(:email_verification_key => userid)
		if !admin
			status = false
		end

		if status
			bg = BusinessGroup.find_by(:id => admin.bg_id)
			admin.bg_id = bg.id
			admin.isemail_verify = "Yes"
			admin.save
			Delayed::Job.enqueue(ConfirmMail.new(admin), :run_at => Time.now, :queue => "#{Rails.env}_cms")
			redirect_to "/?v=pass" 
		else
			admin.destroy
			redirect_to "/?v=fail"
		end
	end

	def get_email_verification_key email
       Digest::MD5.hexdigest(email + Time.now.to_s)
    end

    def check_email
      email = User.find_by(email: params[:email])
      
      respond_to do |format|
        if email
          format.json { render :json => { status: "true" } }
        else
      	  format.json { render :json => { status: "false" } }
        end
      end
    end
end

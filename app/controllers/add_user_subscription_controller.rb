class AddUserSubscriptionController < ApplicationController

	include AddUserSubscriptionHelper
	include ApplicationHelper

	def new
		@bg = current_bg
		@all_tenants = Tenant.where(:bg_id => @bg.id).order_by(:created_at => 'desc')
		if @all_tenants.count == 1
			@all_tenants.each do |tent|
				@tid = tent.id
			end
		else
			@all_tenants
		end
	end

	def create
		status = true
		error = ""
		if status
			metadata = usersubscriptions(params)
		    logger.debug "User Subscriptions is " + metadata.to_s
			
			status = metadata["status"]
			error = metadata["error"]
		end
		if status
			render json: {} , status: :ok
		else
			render :json => {:error => error}.to_json, :status => 500
		end
	end
end
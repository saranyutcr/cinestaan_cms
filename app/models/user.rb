class User
  include Mongoid::Document
  include Mongoid::Timestamps
  field :email, type: String
  field :password_hash, type: String
  field :password_salt, type: String
  field :roles, type: Array #[cms_manager,admin,publisher,content_editor,content_owner,media_viewer]
  field :bg_id, type: String
  field :has_public_access, type: Boolean
  field :auth_token, type: String

   #New User register fields 
    field :first_name, type: String
    field :last_name, type: String
    field :organization, type: String
    field :isemail_verify, type: String
    field :email_verification_key, type: String
    field :forgot_verification_key, type: String 
  #New field for content owner
    field :content_owner_id, type: String
  attr_accessible :email, :password, :password_confirmation
  attr_accessor :password
  before_save :encrypt_password
  before_save :generate_auth_token

  validates_confirmation_of :password
  validates_presence_of :password, :on => :create
  validates_presence_of :email
  validates_uniqueness_of :email
  
  def self.authenticate(email, password)
    user = User.where(:email => email).last
    if user && user.password_hash != BCrypt::Engine.hash_secret(password, user.password_salt)
      return "Incorrect Password"
    elsif user && user.password_hash == BCrypt::Engine.hash_secret(password, user.password_salt)
        user
    else
       return "Invalid Email"
    end
    # if user && user.password_hash == BCrypt::Engine.hash_secret(password, user.password_salt)
    #   user
    # else
    #   nil
    # end
  end
  
  def encrypt_password
    if password.present?
      self.password_salt = BCrypt::Engine.generate_salt
      self.password_hash = BCrypt::Engine.hash_secret(password, password_salt)
    end
  end

  def generate_auth_token
    self.auth_token = loop do
      random_token = SecureRandom.urlsafe_base64(nil, false)
      break random_token unless !User.find_by(:auth_token => random_token).nil?
    end
  end

  def monthly_tx_stats
        stats = {}
        months = []
        created = []
        published = []
        for i in 0..5
            t = Time.now-i.months
            months << Date::MONTHNAMES[(Time.now-i.months).month]
            published << Media.all_of(:user_id => self.id,:created_at => t.beginning_of_month..t.end_of_month,:status=>"Published").count
            created << Media.all_of(:user_id => self.id,:created_at => t.beginning_of_month..t.end_of_month).count
        end
        stats["months"] = months.reverse
        stats["created"] = created.reverse
        stats["published"] = published.reverse
        return stats
  end

  def update_fields params
    self.password = params[:password]
    self.roles = params[:roles].split(",").map(&:strip)
    self.has_public_access = false
    self.content_owner_id = params[:content_owner]
    self.first_name = params[:content_owner_name]
    self.save
  end

end
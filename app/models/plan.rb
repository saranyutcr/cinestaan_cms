class Plan
  include Mongoid::Document
  include Mongoid::Timestamps

  field :title, type: String
  field :display_title, type: String
  field :cost, type: Integer
  field :currency, type: String
  field :days, type: Integer
  field :trackers, type: Array
  field :status, type: String, default: "active"

  # field :billable_items, type: Array

  #cms specific
  # field :total_media_limit, type: Integer #if the BG wants to go to a lower plan later, there shoud be provision to remove media to maintain this limit
  # field :has_transcoding_service, type: Boolean
  # field :has_ott_service, type: Boolean

  #ott service
  # field :catalog_limit, type: Integer
 
  # field :allows_subscription, type: Boolean
  # field :allows_ads, type: Boolean
  # field :has_embedded_player, type: Boolean
  # field :has_website, type: Boolean
  # field :has_android_app, type: Boolean
  # field :has_windows_app, type: Boolean
  # field :has_ios_app, type: Boolean
  # field :user_limit, type: Integer
  # field :bandwidth_usage_limit, type: Integer
  # field :total_ott_media_limit, type: Integer
  # #transcoding service
  # field :input_duration_limit, type: Integer
  # field :monthly_transcode_duration_limit, type: Integer
  # field :total_transcodes_media_limit, type: Integer
  # field :total_profiles_limit, type: Integer
end

class BulkUpload
  include Mongoid::Document
  include Mongoid::Timestamps
  field :excel_file_path, type: String
  field :mail_option, type: String #never,after_each,after_last
  field :total_count, type: Integer
  field :published_count, type: Integer
  field :bg_id, type: String
  field :user_id, type: String
end
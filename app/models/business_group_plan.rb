class BusinessGroupPlan
  include Mongoid::Document

  field :plan_id, type: String
  field :business_group_id, type: String
  field :plan_details, type: Hash  #Data will be copied from plans table
  field :status, type: String
  field :start_date, type: Date
  field :days, type: Integer

end

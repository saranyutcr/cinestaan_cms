class ShowTheme
	include Mongoid::Document
	include Mongoid::Timestamps
	include HttpRequest
	include ApplicationHelper

	field :title, type: String
	field :ott_status, type: String
	field :ott_id, type: String
	field :app_ids, type: Array
	field :tenant_ids, type: Array
	field :bg_id, type: String
	field :catalog_id, type: String
	field :ott_catalog_id, type: String
	field :subcategories_flag, type: String
	field :episodes_flag, type: String

	def get_shows_by_bg(bg_obj)
		ShowTheme.where(:bg_id => bg_obj.id)
	end	

	def get_show_by_id(show_id)
		ShowTheme.find_by(:id => show_id)
	end	

	def get_show_by_ott_id(show_id)
		ShowTheme.find_by(:ott_id => show_id)
	end	

	def get_show_by_catalog(params)
		ShowTheme.where(:catalog_id => params[:catalog_id])
	end	

	#Create Show at OTT
	def create_at_ott(params,bg_obj)
		error = ""
		status = true
		show_obj = create_show_object(params,bg_obj)
		url = "http://" + get_ott_base_url + "/catalogs/" + params["catalog_object"]["catalog_id"] + "/items"
		response = http_request("post",show_obj,url,60)
		if response["code"] == 200
			params["bg_object"] = bg_obj
			params["response_object"] = response["body"]["data"]
			save_at_cms(params)
		else
		  error = response["body"]["error"]["message"]	
		  status = false
		end
		return status,error
	end

	def update_at_ott(params,bg_obj)
		error = ""
		status = true
		show_obj = create_show_object(params,bg_obj)
		# show_obj["content"]["has_subcategories"] = params["show_obj"]["subcategory_flag"]
		# show_obj["content"]["has_episodes"] = params["show_obj"]["episode_flag"]
		url = "http://" + get_ott_base_url + "/catalogs/" + params["catalog_object"]["catalog_id"] + "/items/" + params["show_obj"]["content_id"]
		response = http_request("put",show_obj,url,60)
		if response["code"] == 200
			params["bg_object"] = bg_obj
			params["response_object"] = response["body"]["data"]
			update_at_cms(params)
		else
		  error = response["body"]["error"]["message"]	
		  status = false
		end
		return status,error
	end	

	def update_at_cms(params)
		show = get_show_by_ott_id(params["show_obj"]["content_id"])
		cat_obj = get_catalog_object
		show.title = params["name"]
		show.ott_status = params["response_object"]["status"]
		show.ott_id = params["response_object"]["item_id"]
		cms_catalog = cat_obj.get_catalog_by_ott_id(params["catalog_object"]["catalog_id"])
		show.catalog_id = cms_catalog.id
		show.ott_catalog_id = params["catalog_object"]["catalog_id"]
		show.app_ids = []
		show.app_ids = cms_catalog.app_id
		show.tenant_ids = []
		show.tenant_ids = cms_catalog.tenant_id
		show.bg_id = params["bg_object"]["_id"]
		show.subcategories_flag = params["subcategories_flag"]
		show.episodes_flag = params["episodes_flag"]
		show.save
	end	

	def save_at_cms(params)
		show = ShowTheme.new
		cat_obj = get_catalog_object
		show.title = params["name"]
		show.ott_status = params["response_object"]["status"]
		show.ott_id = params["response_object"]["item_id"]
		cms_catalog = cat_obj.get_catalog_by_ott_id(params["catalog_object"]["catalog_id"])
		show.catalog_id = cms_catalog.id
		show.ott_catalog_id = params["catalog_object"]["catalog_id"]
		show.app_ids = []
		show.app_ids = cms_catalog.app_id
		show.tenant_ids = []
		show.tenant_ids = cms_catalog.tenant_id
		show.bg_id = params["bg_object"]["_id"]
		show.subcategories_flag = params["subcategories_flag"]
		show.episodes_flag = params["episodes_flag"]
		show.save
	end	

	def create_show_object(params,bg_obj)
		body = {}
		body["auth_token"] = bg_obj["ott_auth_token"]
		body["content"] = {}
		body["content"]["title"] = params["name"].strip
		body["content"]["status"] = params["status"]
		body["content"]["l_listing_image"] = params["l_listing_image"]
		body["content"]["p_listing_image"] = params["p_listing_image"]
		body["content"]["featured_image"] = params["featured_image"]
		body["content"]["app_ids"] = params["catalog_object"]["application_ids"]
		body["content"]["tenant_ids"] = params["catalog_object"]["tenant_ids"]
		body["content"]["business_group_id"] = bg_obj["ott_id"]
		body["content"]["regions"] = ["IN"]
		body["content"]["has_subcategories"] = params["subcategories_flag"] if params["subcategories_flag"]
		body["content"]["has_episodes"] = params["episodes_flag"] if params["episodes_flag"]
		body["content"]["description"] = params["description"]
		body["content"]["sequence_no"] = params["sequence_no"]
		body["content"]["meta_title"] = params["meta_title"] if !params["meta_title"].blank?
		body["content"]["meta_keywords"] = params["meta_keywords"] if !params["meta_keywords"].blank?
		body["content"]["meta_description"] = params["meta_description"] if !params["meta_description"].blank?
		get_episodetypes = build_episode_type_hash(params)
		body["content"]["episodetype_tags"] = get_episodetypes		
		return body
	end	

	def get_catalog_object
		Catalog.new
	end	

	def delete_show_from_ott
		ott_id = self.ott_id
		ott_catalog_id = self.ott_catalog_id
		ott_id = self.ott_id
		bg = BusinessGroup.find(self.bg_id)
		url =  "http://"+ get_ott_base_url + "/catalogs/" + ott_catalog_id + "/items/"+ ott_id +"/?auth_token=" + bg.ott_auth_token 
		response = http_request("delete",{},url,60)
		if response["code"] == 200
			status = true
			error = ""
		else
		  error = response["body"]["error"]["message"]	
		  status = false
		end
		return status,error
	end	

	def delete_show_from_cms
		if self.delete
			status = true
			error = ""
		else
			status = false
			error = "Can't delete Show from CMS!"
		end	
		return status,error
	end
	def build_episode_type_hash(params)
	  	episodetype_tag = []
		if params["episodetype_tags"]
		    params["episodetype_tags"].each do |ett|
		      	et_tags = ett.split('-')
		      	name = et_tags[0]
		      	display_title = et_tags[1]
		        h = { "name" => name , "display_title" => display_title }
		        episodetype_tag << h
		    end   
	    end
	    return episodetype_tag
	end
end

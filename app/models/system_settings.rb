class SystemSettings
  include Mongoid::Document
  include Mongoid::Timestamps
  field :environment, type: String
  field :cms_base_url, type: String
  field :cms_public_url, type: String
  field :tx_service_base_url, type: String
  field :ott_base_url, type: String
  field :ott_auth_token, type: String
  field :aux_module_base_url, type: String
  field :web_view_base_url, type: String
end

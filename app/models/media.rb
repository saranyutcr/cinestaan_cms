class Media
    include Mongoid::Document
    include Mongoid::Timestamps
    include HttpRequest
    include ApplicationHelper
    include MediaHelper

    field :title, type: String
    field :platform_media_id, type: String
    field :user_id, type: String
    field :user_role, type: String
    field :transcoding_user_id, type: String
    field :publish_user_id, type: String
    field :unpublish_user_id, type: String
    field :delete_user_id, type: String
    field :bg_id, type: String
    field :source_url, type: String
    field :source_type, type: String
    field :notification_url, type: String
    field :publisher_id, type: String
    field :smart_url, type: String
    field :sprite_url, type: String
    field :status, type: String
    field :ott_status, type: String
    field :is_ott_ready, type: Boolean
    field :ingested_at, type: Time
    field :published_at, type: Time
    field :input_info, type: Hash
    field :output_info, type: Hash
    field :publish_settings_id, type: String
    field :ingest_req_job_id, type: String
    field :publish_req_job_id, type: String
    field :bulk_upload_id, type: String
    field :custom_keys, type: Hash
    field :is_removed, type: Boolean
    field :priority, type: String
    field :ott_id, type: String
    field :ott_catalog_id, type: String
    field :banner, type: String
    field :cut_intervals, type: Array
    field :videolist, type: Boolean
    field :show_id, type: String
    field :subcategory_id, type: String
    field :asset_id, type: String
    field :image_extraction_flag, type: String
    field :ott_publish_date, type: DateTime
    field :ott_unpublish_date, type: DateTime
    field :ott_publish_req_job_id, type: String
    field :ott_unpublish_req_job_id, type: String
    field :parent_media_id, type: String
    field :republish_flag, type: Boolean, :default => false
    #new fields for content owner module
    field :duration_string, type: String
    field :content_owner_id, type: String  
    field :cost_per_hour, type: Integer
    field :currency, type: String
    field :censor_rating, type: String
    field :episode_type, type: String


    def get_media_by_bg(params)
        Media.where(:bg_id => params[:bg_object].id, :is_removed => false)
    end

    def get_media_by_subcategory(subcategory_id)
        Media.where(:subcategory_id => subcategory_id, :is_removed => false)
    end    

    def get_media_by_show(show_id)
        Media.where(:show_id => show_id, :is_removed => false)
    end    

    def get_media_by_catalog(params)
        Media.where(:ott_catalog_id => params[:catalog_obj].ott_id, :is_removed => false)
    end   

    def add_media_in_publish_queue(metadata)
        self.ott_publish_req_job_id = self.delay(:queue => "#{Rails.env}_publish_cms",:run_at => self.ott_publish_date).publish_at_ott.id
    end

    def add_media_in_unpublish_queue(metadata)
        self.ott_unpublish_req_job_id = self.delay(:queue => "#{Rails.env}_unpublish_cms",:run_at => self.ott_unpublish_date).unpublish_at_ott.id
    end    

    def transcode_request
        status = true
        error = ""
        self.status = "Processing Request"
        self.transcoding_user_id = self.user_id

        # ip = get_cms_base_url.partition(':').first
        # url = "http://"+ip+":5050/media/#{self.id}"
        url = "http://" + get_tx_service_base_url + "/media/#{self.id}" 
        body = {}
        response = http_request("post",body,url,60)
        if !response["status"]
            self.status = "Primary Request Error"
        end
        self.save
        status = response["status"]
        error = response["error"]
        return status,error
    end
    def update_priority(priority)
        status = true
        error = ""
        body = {}
        maxtime = 60
        bg = BusinessGroup.find(self.bg_id)
        body["video_id"] = self.platform_media_id
        body["publisher_id"] = self.publisher_id
        body["priority"] = priority
        url = "http://#{bg.admin_base_url}/update_priority"
        response = http_request("post",body,url,maxtime)
        if response["status"]
            self.priority = priority
            self.save
        else
            status = false
            error = "Could not update priority!"
        end
        return status,error
    end
    def validate_media_for_transcoding(media)
        status = true
        error = ""
        if status
            if media.status!="Created"
                status = false
                error = "This media cannot be transcoded now"
            end
        end
        if status
            if !media.source_url
                status = false
                error = "Video URL cannot be empty"
            end
        end
        # if status
        #     if !media.source_url.start_with?('ftp://')
        #         status = false
        #         error = "Error in Video URL Format"
        #     end
        # end
        #plan = BusinessGroup.find(media.bg_id).plan
        if status 
            if !media.publish_settings_id
                status = false
                error = "Publish Settings ID cannot be empty"
            end
        end

        if status
            if media.publish_settings_id.strip == ""
                status = false
                error = "Publish Settings ID cannot be empty"
            end
        end
        return status,error
    end
    def create_at_ott(metadata,catalog,eshow_id)
        status = true
        error = ""
        maxtime = 60
        body = {}
        var=self.bg_id
        #logger.debug "#{var}===========bg id"
        body["auth_token"] = BusinessGroup.find(self.bg_id).ott_auth_token
        body["content"] = metadata
        # body["content"]["smart_url"] = self.smart_url
        # body["content"]["status"] = "edit"
        theme = catalog.theme
        case theme
        when "show"
          #url = "http://"+get_ott_base_url+"/catalogs/"+self.ott_catalog_id+"/episodes"
          show_data = ShowTheme.find_by(:id => eshow_id)
          if show_data.nil?
          subcategory_data = Subcategory.find_by(:id => eshow_id)
          
          url = "http://"+get_ott_base_url+"/catalogs/shows/subcategories/"+subcategory_data.ott_id+"/episodes"
          else
         
          url = "http://"+get_ott_base_url+"/catalogs/shows/"+show_data.ott_id+"/episodes"
          end           
        else 
          url = "http://"+get_ott_base_url+"/catalogs/"+self.ott_catalog_id+"/items"
        end
        response = http_request("post",body,url,maxtime)
        if response["code"] == 200
            self.ott_status = response["body"]["data"]["status"]
            self.ott_id = response["body"]["data"]["item_id"]
            self.show_id = subcategory_data.show_id if subcategory_data
            if metadata["asset_id"].blank?
                self.asset_id = response["body"]["data"]["item_id"]
            else
                self.asset_id = metadata["asset_id"]    
            end
            self.ott_publish_date = metadata["published_date"]
            self.ott_unpublish_date = metadata["unpublished_date"]
            self.episode_type = metadata["episode_type"]
            self.save 
            return true,response
        else
            status = false
            if response["body"].has_key? "error"
                error = response["body"]["error"]["message"]
            else
                error = response["body"]["errors"]
            end    
            #error = "Could not create at OTT!"
        end
        return status,error
    end

    def update_at_ott(metadata)
        status = true
        error = ""
        maxtime = 60
        body = {}
         bg = BusinessGroup.find(self.bg_id)
        body["auth_token"] = bg.ott_auth_token
        body["content"] = metadata
        catalog = catalog_object.get_catalog_by_ott_id(self.ott_catalog_id)
        case catalog.theme
        when "show"
          #url = "http://"+get_ott_base_url+"/catalogs/"+self.ott_catalog_id+"/episodes/"+self.ott_id+"?auth_token="+bg.ott_auth_token   
              if metadata["show_id"].nil?
              subcategory_data = Subcategory.find_by(:id => metadata["subcategory_id"])
              url = "http://"+get_ott_base_url+"/catalogs/shows/subcategories/"+subcategory_data.ott_id+"/episodes/"+self.ott_id+"?auth_token="+bg.ott_auth_token
              else
              show_data = ShowTheme.find_by(:id => metadata["show_id"]) || ShowTheme.find_by(:ott_id => metadata["show_id"])
              url = "http://"+get_ott_base_url+"/catalogs/shows/"+show_data.ott_id+"/episodes/"+self.ott_id+"?auth_token="+bg.ott_auth_token
              end
        else    
          url = "http://"+get_ott_base_url+"/catalogs/"+self.ott_catalog_id+"/items/"+self.ott_id+"?auth_token="+bg.ott_auth_token
        end
        response = http_request("put",body,url,maxtime)
        if response["code"] == 200
            self.ott_status = response["body"]["data"]["status"]
            self.title = metadata["title"]
            if metadata["asset_id"].blank?
                self.asset_id = response["body"]["data"]["item_id"]
            else
                self.asset_id = metadata["asset_id"]    
            end
            self.ott_publish_date = metadata["published_date"]
            self.ott_unpublish_date = metadata["unpublished_date"]
            self.save
            return true, response
        else    
            status = false
            error = response["body"]["error"]["message"]
            # error = "Could not update at OTT!"
        end
        return status,error
    end

    def update_transcoding_data_at_ott
        status = true
        error = ""
        maxtime = 60
        body = {}
        bg = BusinessGroup.find(self.bg_id)

        #Sign smart url

        if self.image_extraction_flag == "yes" && self.status == "Published"
            smart_url_key = bg.tx_auth_token
            surl = self.smart_url
            signed_url = sign_smart_url(smart_url_key,"#{surl}?service_id=6&play_url=yes&protocol=hls&us=")
            # thumbnails = signed_url.select { |h| h.has_key? "thumbnails" }
            header = {"Accept" => "application/json", "Cache-Control" => "no-cache",  "User-Agent" => "Mozilla/5.0 (iPad; CPU OS 5_0 like Mac OS X) AppleWebKit/534.46 (KHTML, like Gecko) Version/5.1 Mobile/9A334 Safari/7534.48.3"}
            response = Typhoeus::Request.get(signed_url, headers: header)
            if response.code == 200
              urls = JSON.parse(response.body)     
              thumbnails = urls.select { |h| h.has_key? "thumbnails" }
              if !thumbnails.empty?
                images = thumbnails.first["thumbnails"]
                images.each do |image|
                    resolution = image["resolution"]
                    case resolution
                    when "794x447"
                        @featured_image = image["url"]
                    when "410x231"
                        @l_listing_image = image["url"]
                    when "251x141"    
                        @p_listing_image = image["url"]
                    end    
                end 

              end
            end    
        end    

        catalog = catalog_object.get_catalog_by_ott_id(self.ott_catalog_id)
        case catalog.theme
        when "show"
          subcategory_data = Subcategory.find_by(:id => self.subcategory_id)
          if !subcategory_data.nil?
            url = "http://"+get_ott_base_url+"/catalogs/shows/subcategories/"+subcategory_data.ott_id+"/episodes/"+self.ott_id+"?auth_token="+bg.ott_auth_token+"&status=any&region=any"
          else 
            show_data = ShowTheme.find_by(:id => self.show_id) || ShowTheme.find_by(:ott_id => self.show_id) 
            url = "http://"+get_ott_base_url+"/catalogs/shows/"+show_data.ott_id+"/episodes/"+self.ott_id+"?auth_token="+bg.ott_auth_token+"&status=any&region=any"
          end
        else
          url = "http://"+get_ott_base_url+"/catalogs/"+self.ott_catalog_id+"/items/"+self.ott_id+"?auth_token="+bg.ott_auth_token+"&status=any&region=any"    
        end
        
        response = http_request("get",{},url,maxtime)
        if response["code"] == 200
            body["auth_token"] = bg.ott_auth_token
            resp_data =  response["body"]["data"]
            body["content"] = {}
            body["content"]["status"] = resp_data["status"]
            body["content"]["title"] = self.title
            # body["content"]["smart_url"] = self.smart_url
            body["content"]["app_ids"] = resp_data["app_ids"]
            body["content"]["tenant_ids"] = resp_data["tenant_ids"]
            body["content"]["business_group_id"] = bg.ott_id
            body["content"]["regions"] = ["IN"] # hardcoded value
            body["content"]["l_listing_image"] = @l_listing_image ? @l_listing_image : resp_data["thumbnails"]["l_medium"]["url"]
            body["content"]["p_listing_image"] = @p_listing_image ? @p_listing_image : resp_data["thumbnails"]["p_small"]["url"]
            body["content"]["featured_image"] = @featured_image ? @featured_image : resp_data["thumbnails"]["l_large"]["url"]
            body["content"]["description"] = resp_data["description"]
            body["content"]["short_description"] = resp_data["short_description"]
            body["content"]["release_date_string"] = resp_data["release_date_string"]
            body["content"]["duration_string"] = resp_data["duration_string"]
            body["content"]["genres"] = resp_data["genres"]
            body["content"]["language"] = resp_data["language"]
            body["content"]["rating"] = resp_data["rating"]
            body["content"]["asset_id"] = resp_data["asset_id"]
            body["content"]["keywords"] = resp_data["keywords"]
            body["content"]["custom_fields"] = resp_data["custom_fields"]

            ott_play_url = resp_data["play_url"]
            
            case self.status
            when "Published"
                if !self.smart_url.blank? && ott_play_url.empty?
                    body["content"]["play_url_type"] = "saranyu"
                    body["content"]["play_url"] = { "saranyu" => { "url" => self.smart_url, "width" => -1.0 , "height" => -1.0 } }
                end    

                if !self.smart_url.blank? && !ott_play_url.empty?
                    body["content"]["play_url_type"] = resp_data["play_url_type"]
                    body["content"]["play_url"] = resp_data["play_url"]
                end    
                
            when "Created Externally"
                if ott_play_url.empty?
                    body["content"]["play_url_type"] = ""
                    body["content"]["play_url"] = {}
                else
                    body["content"]["play_url_type"] = response["body"]["data"]["play_url_type"]
                    body["content"]["play_url"] = response["body"]["data"]["play_url"]
                end    
            end    

            # case catalog.theme
            # when "show"
            #   #url = "http://"+get_ott_base_url+"/catalogs/"+self.ott_catalog_id+"/episodes/"+self.ott_id
            #   show_data = ShowTheme.find_by(:id => self.show_id) || ShowTheme.find_by(:ott_id => self.show_id)
            #   if show_data.nil?
            #   subcategory_data = Subcategory.find_by(:id => self.subcategory_id)
            #   url = "http://"+get_ott_base_url+"/catalogs/shows/subcategories/"+subcategory_data.ott_id+"/episodes/"+self.ott_id
            #   else  
            #   url = "http://"+get_ott_base_url+"/catalogs/shows/"+show_data.ott_id+"/episodes/"+self.ott_id
            #   end
            # else    
            #     url = "http://"+get_ott_base_url+"/catalogs/"+self.ott_catalog_id+"/items/"+self.ott_id
            # end

            response = http_request("put",body,url,maxtime)
            if response["code"] != 200
            status = false
            error = response["body"]["error"]["message"]
            # error = "Could not publish at OTT!"
            else
                self.ott_status = response["body"]["data"]["status"]
                self.save
            end
        else
            status = false
            error = "Could not publish at OTT!"     
        end    

        return status,error 
    end
        
    def delete_at_ott
        status = true
        error = ""
        maxtime = 60
        body = {}
        bg = BusinessGroup.find(self.bg_id)
        body["auth_token"] = bg.ott_auth_token
        catalog = catalog_object.get_catalog_by_ott_id(self.ott_catalog_id)

        videolist = Media.find_by(:ott_id => self.ott_id)

        if videolist.videolist == true
            url = "http://"+get_ott_base_url+"/catalogs/"+self.ott_catalog_id+"/videolists/"+self.ott_id
        else
        case catalog.theme
        when "show"
          url = "http://"+get_ott_base_url+"/catalogs/"+self.ott_catalog_id+"/episodes/"+self.ott_id
        else    
          url = "http://"+get_ott_base_url+"/catalogs/"+self.ott_catalog_id+"/items/"+self.ott_id
        end
        end
          
        response = http_request("delete",body,url,maxtime)
        if response["code"] != 200 
            self.is_removed = false
            status = false
            error = "Could not delete at OTT!"
        end
        return status,error
    end

    def publish_at_ott
        status = true
        error = ""
        maxtime = 60
        body = {}
        bg = BusinessGroup.find(self.bg_id)
        catalog = catalog_object.get_catalog_by_ott_id(self.ott_catalog_id)

        videolist = Media.find_by(:ott_id => self.ott_id)
        if videolist.videolist == true
            url = "http://"+get_ott_base_url+"/catalogs/"+self.ott_catalog_id+"/videolists?auth_token="+bg.ott_auth_token+"&status=any&region=IN"
            #url = "http://"+get_ott_base_url+"/catalogs/"+self.ott_catalog_id+"/videolists/"+self.ott_id+"?auth_token="+bg.ott_auth_token
        else
        case catalog.theme
        when "show"
          subcategory_data = Subcategory.find_by(:id => self.subcategory_id)
          if !subcategory_data.nil?
            url = "http://"+get_ott_base_url+"/catalogs/shows/subcategories/"+subcategory_data.ott_id+"/episodes/"+self.ott_id+"?auth_token="+bg.ott_auth_token+"&status=any&region=any"
          else 
            show_data = ShowTheme.find_by(:id => self.show_id) || ShowTheme.find_by(:ott_id => self.show_id) 
            url = "http://"+get_ott_base_url+"/catalogs/shows/"+show_data.ott_id+"/episodes/"+self.ott_id+"?auth_token="+bg.ott_auth_token+"&status=any&region=any"
          end
        else
          url = "http://"+get_ott_base_url+"/catalogs/"+self.ott_catalog_id+"/items/"+self.ott_id+"?auth_token="+bg.ott_auth_token+"&status=any&region=any"    
        end
        end
        
        response = http_request("get",{},url,maxtime)
        if response["code"] == 200
            purl = "http://"+get_ott_base_url+"/catalogs/"+self.ott_catalog_id+"/videolists/"+self.ott_id
            body["auth_token"] = bg.ott_auth_token
            if videolist.videolist == true
                logger.debug "====================> #{response} <========================"
               # videodata = response["body"]["video_list"]
               videodata = response["body"]["data"]["items"]
               logger.debug "====================> Video Data:: #{response["body"]["data"]["items"]} <========================"
               videodata.each do |vli|
                if videolist.ott_id == vli["videolist_id"]
                    body["video_list"] = {}
                    case catalog.theme
                    when "movie"
                        body["video_list"]["catalog"] = "movie"
                    when "video"
                        body["video_list"]["catalog"] = "video"
                    when "show"
                        body["video_list"]["catalog"] = "show"
                    end
                        body["video_list"]["title"] = self.title
                        body["video_list"]["featured_image"] = vli["thumbnails"]["l_medium"]["url"]
                        body["video_list"]["l_listing_image"] = vli["thumbnails"]["l_medium"]["url"]
                        body["video_list"]["p_listing_image"] = vli["thumbnails"]["l_medium"]["url"]
                        body["video_list"]["category"]= vli["category"]
                        body["video_list"]["regions"] = ["IN"]
                        body["video_list"]["business_group_id"] = bg.ott_id
                        body["video_list"]["description"] = vli["description"]
                        body["video_list"]["duration"] = 60
                        body["video_list"]["rating"] = 1
                        body["video_list"]["smart_url"] = vli["smart_url"]
                        body["video_list"]["status"] = "published"
                    
                    # if params["source_url"].blank?
                    #     metadata["play_url_type"] = "saranyu"
                    #     metadata["play_url"] = { "saranyu" => { "url" => self.smart_url, "width" => -1.0 , "height" => -1.0 } }
                    # end

                    case self.status
                    # when "Published"
                    when "published"
                        
                        if !self.smart_url.blank? && body["video_list"]["smart_url"].empty?
                            body["video_list"]["play_url_type"] = "saranyu"
                            body["video_list"]["play_url"] = { "saranyu" => { "url" => self.smart_url, "width" => -1.0 , "height" => -1.0 } }
                        end    

                        if !self.smart_url.blank? && !body["video_list"]["smart_url"].empty?
                            body["video_list"]["play_url_type"] = "saranyu"
                            body["video_list"]["play_url"] = {"saranyu" => {"url" => vli["play_url"]["saranyu"]["url"]}}
                        end    
                        
                    when "Created Externally"
                        if body["video_list"]["smart_url"].empty?
                            body["video_list"]["play_url_type"] = ""
                            body["video_list"]["play_url"] = {}
                        else
                            body["video_list"]["play_url_type"] = "saranyu"
                            body["video_list"]["play_url"] = {"saranyu" => {"url" => vli["play_url"]["saranyu"]["url"]}}
                        end    
                    end

                    response = http_request("put",body,purl,maxtime)
                    if response["code"] != 200
                    status = false
                    error = response["body"]["error"]["message"]
                    # error = "Could not publish at OTT!"
                    else
                        self.ott_status = "published"
                        self.save
                    end

                end
               end
            else
            resp_data =  response["body"]["data"]
            body["content"] = {}
            body["content"]["status"] = "published"
            body["content"]["title"] = self.title
            # body["content"]["smart_url"] = self.smart_url
            body["content"]["app_ids"] = resp_data["app_ids"]
            body["content"]["tenant_ids"] = resp_data["tenant_ids"]
            body["content"]["business_group_id"] = bg.ott_id
            body["content"]["regions"] = ["IN"] # hardcoded value
            body["content"]["l_listing_image"] = resp_data["thumbnails"]["l_medium"]["url"]
            body["content"]["p_listing_image"] = resp_data["thumbnails"]["p_small"]["url"]
            body["content"]["featured_image"] = resp_data["thumbnails"]["l_large"]["url"]
            body["content"]["description"] = resp_data["description"]
            body["content"]["short_description"] = resp_data["short_description"]
            body["content"]["release_date_string"] = resp_data["release_date_string"]
            body["content"]["duration_string"] = resp_data["duration_string"]
            body["content"]["genres"] = resp_data["genres"]
            body["content"]["language"] = resp_data["language"]
            body["content"]["rating"] = resp_data["rating"]
            body["content"]["asset_id"] = resp_data["asset_id"]
            body["content"]["keywords"] = resp_data["keywords"]
            body["content"]["custom_fields"] = resp_data["custom_fields"]

            ott_play_url = resp_data["play_url"]
            
            case self.status
            when "Published"
                if !self.smart_url.blank? && ott_play_url.empty?
                    body["content"]["play_url_type"] = "saranyu"
                    body["content"]["play_url"] = { "saranyu" => { "url" => self.smart_url, "width" => -1.0 , "height" => -1.0 } }
                end    

                if !self.smart_url.blank? && !ott_play_url.empty?
                    body["content"]["play_url_type"] = resp_data["play_url_type"]
                    body["content"]["play_url"] = resp_data["play_url"]
                end    
                
            when "Created Externally"
                if ott_play_url.empty?
                    body["content"]["play_url_type"] = ""
                    body["content"]["play_url"] = {}
                else
                    body["content"]["play_url_type"] = response["body"]["data"]["play_url_type"]
                    body["content"]["play_url"] = response["body"]["data"]["play_url"]
                end    
            end  

            # case catalog.theme
            # when "show"
            #   #url = "http://"+get_ott_base_url+"/catalogs/"+self.ott_catalog_id+"/episodes/"+self.ott_id
            #   show_data = ShowTheme.find_by(:id => self.show_id) || ShowTheme.find_by(:ott_id => self.show_id)
            #   if show_data.nil?
            #   subcategory_data = Subcategory.find_by(:id => self.subcategory_id)
            #   url = "http://"+get_ott_base_url+"/catalogs/shows/subcategories/"+subcategory_data.ott_id+"/episodes/"+self.ott_id
            #   else  
            #   url = "http://"+get_ott_base_url+"/catalogs/shows/"+show_data.ott_id+"/episodes/"+self.ott_id
            #   end
            # else    
            #     url = "http://"+get_ott_base_url+"/catalogs/"+self.ott_catalog_id+"/items/"+self.ott_id
            # end

            response = http_request("put",body,url,maxtime)
            if response["code"] != 200
            status = false
            error = response["body"]["error"]["message"]
            # error = "Could not publish at OTT!"
            else
                self.ott_status = response["body"]["data"]["status"]
                self.save
            end
            end
        else
            status = false
            error = "Could not publish at OTT!"     
        end    

        return status,error
    end

    def unpublish_at_ott
        status = true
        error = ""
        maxtime = 60
        body = {}
        bg = BusinessGroup.find(self.bg_id)
        catalog = catalog_object.get_catalog_by_ott_id(self.ott_catalog_id)

        videolist = Media.find_by(:ott_id => self.ott_id)
        if videolist.videolist == true
            url = "http://"+get_ott_base_url+"/catalogs/"+self.ott_catalog_id+"/videolists?auth_token="+bg.ott_auth_token+"&status=any&region=IN"
        else
        case catalog.theme
        when "show"
          subcategory_data = Subcategory.find_by(:id => self.subcategory_id)
          if !subcategory_data.nil?
            url = "http://"+get_ott_base_url+"/catalogs/shows/subcategories/"+subcategory_data.ott_id+"/episodes/"+self.ott_id+"?auth_token="+bg.ott_auth_token+"&status=any&region=any"
          else 
            show_data = ShowTheme.find_by(:id => self.show_id) || ShowTheme.find_by(:ott_id => self.show_id) 
            url = "http://"+get_ott_base_url+"/catalogs/shows/"+show_data.ott_id+"/episodes/"+self.ott_id+"?auth_token="+bg.ott_auth_token+"&status=any&region=any"
          end
        else
          url = "http://"+get_ott_base_url+"/catalogs/"+self.ott_catalog_id+"/items/"+self.ott_id+"?auth_token="+bg.ott_auth_token+"&status=any&region=any"    
        end
        end
        
        response = http_request("get",{},url,maxtime)
        logger.debug "====================> Response on GET Call:: #{response.inspect} <===================="
        if response["code"] == 200
            purl = "http://"+get_ott_base_url+"/catalogs/"+self.ott_catalog_id+"/videolists/"+self.ott_id
            body["auth_token"] = bg.ott_auth_token
            if videolist.videolist == true
               #videodata = response["body"]["video_list"]
               videodata = response["body"]["data"]["items"]
               videodata.each do |vli|
                if videolist.ott_id == vli["videolist_id"]
                    body["video_list"] = {}
                    case catalog.theme
                    when "movie"
                        body["video_list"]["catalog"] = "movie"
                    when "video"
                        body["video_list"]["catalog"] = "video"
                    when "show"
                        body["video_list"]["catalog"] = "show"
                    end
                        body["video_list"]["title"] = self.title
                        body["video_list"]["featured_image"] = vli["thumbnails"]["l_medium"]["url"]
                        body["video_list"]["l_listing_image"] = vli["thumbnails"]["l_medium"]["url"]
                        body["video_list"]["p_listing_image"] = vli["thumbnails"]["l_medium"]["url"]
                        body["video_list"]["category"]= vli["category"]
                        body["video_list"]["regions"] = ["IN"]
                        body["video_list"]["business_group_id"] = bg.ott_id
                        body["video_list"]["description"] = vli["description"]
                        body["video_list"]["duration"] = 60
                        body["video_list"]["rating"] = 1
                        body["video_list"]["smart_url"] = vli["smart_url"]
                        body["video_list"]["status"] = "unpublished"
                    
                    # if params["source_url"].blank?
                    #     metadata["play_url_type"] = "saranyu"
                    #     metadata["play_url"] = { "saranyu" => { "url" => self.smart_url, "width" => -1.0 , "height" => -1.0 } }
                    # end

                    case self.status
                    when "Published"
                        if !self.smart_url.blank? && body["video_list"]["smart_url"].empty?
                            body["video_list"]["play_url_type"] = "saranyu"
                            body["video_list"]["play_url"] = { "saranyu" => { "url" => self.smart_url, "width" => -1.0 , "height" => -1.0 } }
                        end    

                        if !self.smart_url.blank? && !body["video_list"]["smart_url"].empty?
                            body["video_list"]["play_url_type"] = "saranyu"
                            body["video_list"]["play_url"] = {"saranyu" => {"url" => vli["play_url"]["saranyu"]["url"]}}
                        end    
                        
                    when "Created Externally"
                        if body["video_list"]["smart_url"].empty?
                            body["video_list"]["play_url_type"] = ""
                            body["video_list"]["play_url"] = {}
                        else
                            body["video_list"]["play_url_type"] = "saranyu"
                            body["video_list"]["play_url"] = {"saranyu" => {"url" => vli["play_url"]["saranyu"]["url"]}}
                        end    
                    end

                    response = http_request("put",body,purl,maxtime)
                    if response["code"] != 200
                    status = false
                    error = response["body"]["error"]["message"]
                    # error = "Could not publish at OTT!"
                    else
                        self.ott_status = "unpublished"
                        self.save
                    end

                end
               end
            else
            resp_data =  response["body"]["data"]
            body["content"] = {}
            body["content"]["status"] = "unpublished"
            body["content"]["title"] = self.title
            # body["content"]["smart_url"] = self.smart_url
            body["content"]["app_ids"] = resp_data["app_ids"]
            body["content"]["tenant_ids"] = resp_data["tenant_ids"]
            body["content"]["business_group_id"] = bg.ott_id
            body["content"]["regions"] = ["IN"] # hardcoded value
            body["content"]["l_listing_image"] = resp_data["thumbnails"]["l_medium"]["url"]
            body["content"]["p_listing_image"] = resp_data["thumbnails"]["p_small"]["url"]
            body["content"]["featured_image"] = resp_data["thumbnails"]["l_large"]["url"]
            body["content"]["description"] = resp_data["description"]
            body["content"]["short_description"] = resp_data["short_description"]
            body["content"]["release_date_string"] = resp_data["release_date_string"]
            body["content"]["duration_string"] = resp_data["duration_string"]
            body["content"]["genres"] = resp_data["genres"]
            body["content"]["language"] = resp_data["language"]
            body["content"]["rating"] = resp_data["rating"]
            body["content"]["asset_id"] = resp_data["asset_id"]
            body["content"]["keywords"] = resp_data["keywords"]
            body["content"]["custom_fields"] = resp_data["custom_fields"]

            ott_play_url = resp_data["play_url"]
            
            case self.status
            when "Published"
                if !self.smart_url.blank? && ott_play_url.empty?
                    body["content"]["play_url_type"] = "saranyu"
                    body["content"]["play_url"] = { "saranyu" => { "url" => self.smart_url, "width" => -1.0 , "height" => -1.0 } }
                end    

                if !self.smart_url.blank? && !ott_play_url.empty?
                    body["content"]["play_url_type"] = resp_data["play_url_type"]
                    body["content"]["play_url"] = resp_data["play_url"]
                end    
                
            when "Created Externally"
                if ott_play_url.empty?
                    body["content"]["play_url_type"] = ""
                    body["content"]["play_url"] = {}
                else
                    body["content"]["play_url_type"] = response["body"]["data"]["play_url_type"]
                    body["content"]["play_url"] = response["body"]["data"]["play_url"]
                end    
            end  
            # case catalog.theme
            # when "show"
            #   #url = "http://"+get_ott_base_url+"/catalogs/"+self.ott_catalog_id+"/episodes/"+self.ott_id
            #   show_data = ShowTheme.find_by(:id => self.show_id) || ShowTheme.find_by(:ott_id => self.show_id)
            #   if show_data.nil?
            #   subcategory_data = Subcategory.find_by(:id => self.subcategory_id)
            #   url = "http://"+get_ott_base_url+"/catalogs/shows/subcategories/"+subcategory_data.ott_id+"/episodes/"+self.ott_id
            #   else  
            #   url = "http://"+get_ott_base_url+"/catalogs/shows/"+show_data.ott_id+"/episodes/"+self.ott_id
            #   end
            # else    
            #     url = "http://"+get_ott_base_url+"/catalogs/"+self.ott_catalog_id+"/items/"+self.ott_id
            # end

            response = http_request("put",body,url,maxtime)
            if response["code"] != 200
            status = false
            error = response["body"]["error"]["message"]
            # error = "Could not publish at OTT!"
            else
                self.ott_status = response["body"]["data"]["status"]
                self.save
            end
            end
        else
            status = false
            error = "Could not publish at OTT!"     
        end    

        return status,error
    end
    
    # def unpublish_at_ott
    #     status = true
    #     error = ""
    #     maxtime = 60
    #     bg = BusinessGroup.find(self.bg_id)
    #     catalog = catalog_object.get_catalog_by_ott_id(self.ott_catalog_id)
    #     case catalog.theme
    #     when "show"
    #       #url = "http://"+get_ott_base_url+"/catalogs/"+self.ott_catalog_id+"/episodes/"+self.ott_id+"?auth_token="+bg.ott_auth_token+"&status=any&region=any"
    #           show_data = ShowTheme.find_by(:id => self.show_id) || ShowTheme.find_by(:ott_id => self.show_id)
    #           if show_data.nil?
    #           subcategory_data = Subcategory.find_by(:id => self.subcategory_id)
    #           url = "http://"+get_ott_base_url+"/catalogs/shows/subcategories/"+subcategory_data.ott_id+"/episodes/"+self.ott_id+"?auth_token="+bg.ott_auth_token+"&status=any&region=any"
    #           else  
    #           url = "http://"+get_ott_base_url+"/catalogs/shows/"+show_data.ott_id+"/episodes/"+self.ott_id+"?auth_token="+bg.ott_auth_token+"&status=any&region=any"
    #           end
    #     else
    #       url = "http://"+get_ott_base_url+"/catalogs/"+self.ott_catalog_id+"/items/"+self.ott_id+"?auth_token="+bg.ott_auth_token+"&status=any&region=any"    
    #     end
    #     response = http_request("get",{},url,maxtime)
    #     if response["code"] == 200
    #         body = {}
    #         body["auth_token"] = bg.ott_auth_token
    #         body["content"] = {}
    #         body["content"]["status"] = "unpublished"
    #         body["content"]["title"] = self.title
    #         body["content"]["business_group_id"] = bg.ott_id
    #         body["content"]["l_listing_image"] = response["body"]["data"]["thumbnails"]["l_medium"]["url"]
    #         body["content"]["p_listing_image"] = response["body"]["data"]["thumbnails"]["p_small"]["url"]
    #         body["content"]["featured_image"] = response["body"]["data"]["thumbnails"]["l_large"]["url"]
    #         ott_play_url = response["body"]["data"]["play_url"]
    #         case self.status
    #         when "Published"
    #             if !self.smart_url.blank? && ott_play_url.empty?
    #                 body["content"]["play_url_type"] = "saranyu"
    #                 body["content"]["play_url"] = { "saranyu" => { "url" => self.smart_url, "width" => -1.0 , "height" => -1.0 } }
    #             end    

    #             if !self.smart_url.blank? && !ott_play_url.empty?
    #                 body["content"]["play_url_type"] = response["body"]["data"]["play_url_type"]
    #                 body["content"]["play_url"] = response["body"]["data"]["play_url"]
    #             end    
                
    #         when "Created Externally"
    #             if ott_play_url.empty?
    #                 body["content"]["play_url_type"] = ""
    #                 body["content"]["play_url"] = {}
    #             else
    #                 body["content"]["play_url_type"] = response["body"]["data"]["play_url_type"]
    #                 body["content"]["play_url"] = response["body"]["data"]["play_url"]
    #             end    
    #         end
    #         body["content"]["app_ids"] = response["body"]["data"]["app_ids"]
    #         body["content"]["tenant_ids"] = response["body"]["data"]["tenant_ids"]
    #         body["content"]["regions"] = ["IN"]

    #         catalog = catalog_object.get_catalog_by_ott_id(self.ott_catalog_id)
    #         case catalog.theme
    #         when "show"
    #              #url = "http://"+get_ott_base_url+"/catalogs/"+self.ott_catalog_id+"/episodes/"+self.ott_id
    #               show_data = ShowTheme.find_by(:id => self.show_id) || ShowTheme.find_by(:ott_id => self.show_id)
    #               if show_data.nil?
    #               subcategory_data = Subcategory.find_by(:id => self.subcategory_id)
    #               url = "http://"+get_ott_base_url+"/catalogs/shows/subcategories/"+subcategory_data.ott_id+"/episodes/"+self.ott_id
    #               else  
    #               url = "http://"+get_ott_base_url+"/catalogs/shows/"+show_data.ott_id+"/episodes/"+self.ott_id
    #               end
    #         else
    #             url = "http://"+get_ott_base_url+"/catalogs/"+self.ott_catalog_id+"/items/"+self.ott_id
    #         end
    #         response = http_request("put",body,url,maxtime)
    #         if response["code"] == 200
    #             self.ott_status = response["body"]["data"]["status"]
    #             self.save  
    #         else
    #             status = false
    #             error = "Could not unpublish at OTT!"
    #         end 
    #     end    
    #     return status,error
    # end
    def delete_at_tx
        status = true
        error = ""
        maxtime = 60
        body = {}
        bg = BusinessGroup.find(self.bg_id)
        body["auth_token"] = bg.tx_auth_token
        if !bg.tag.blank?
            body["tag_name"] = bg.tag
        end
        url = "http://"+bg.tx_base_url+"/videos/"+self.platform_media_id
        response = http_request("delete",body,url,maxtime)
        if response["status"]
            self.platform_media_id = nil
            self.save 
        else
            status = false
            error = "Could not delete at transcoding!"
        end
        return status,error
    end

    def catalog_object
        Catalog.new
    end 

#Video_Tags Call

def create_videotags_at_ott(metadata,catalog)
        status = true
        error = ""
        maxtime = 60
        body = {}
        body["auth_token"] = BusinessGroup.find(self.bg_id).ott_auth_token
        body["video_list"] = metadata
        url = "http://"+get_ott_base_url+"/catalogs/"+self.ott_catalog_id+"/videolists"   
        response = http_request("post",body,url,maxtime)
        if response["code"] == 200
            self.ott_status = "edit"
            self.ott_id = response["body"]["videolist_id"]
            self.save 
        else
            status = false
            error = response["body"]["error"]["message"]
            #error = "Could not create at OTT!"
        end
        return status,error
    end
    def create_video_list metadata,catalog_id,item_id
        status = true
        error = ""
        maxtime = 60
        body = {}
        bg = BusinessGroup.find(self.bg_id)
        body["auth_token"] = bg.ott_auth_token
        metadata["business_group_id"] = bg.ott_id
        body["video_list"] = metadata
        url = "http://"+get_ott_base_url+"/catalogs/"+catalog_id+"/items/"+item_id+"/videolists"
        response = http_request("post",body,url,maxtime)
        if response["code"] == 200
            #self.ott_status = "edit"
            #self.save
            return true,response 

        else
            status = false
	    logger.debug "response=====#{response["body"]}"
            error = response["body"]["error"]
            return status,error
            #error = "Could not create at OTT!"
        end
        

    end

    def update_video_list(metadata,catalog_id,item_id,videolist_id)
        status = true
        error = ""
        maxtime = 60
        body = {}
        bg = BusinessGroup.find(self.bg_id)
        body["auth_token"] = bg.ott_auth_token
        metadata["business_group_id"] = bg.ott_id
        body["video_list"] = metadata
        url = "http://"+get_ott_base_url+"/catalogs/"+catalog_id+"/items/"+item_id+"/videolists/"+videolist_id
        response = http_request("put",body,url,maxtime)
        if response["code"] == 200
            #self.ott_status = "edit"
            #self.save 
            return true,response 
        else
            # status = false
          #  error = response["body"]["error"]["message"]
            #error = "Could not create at OTT!"
            return false,response["body"]["data"]
        end
        
    end

    def get_videolist_data catalog_id,item_id

        auth_token =BusinessGroup.find(self.bg_id).ott_auth_token
        url = "http://"+get_ott_base_url+"/catalogs/"+catalog_id+"/items/"+ item_id + "/videolists?auth_token=" + auth_token
        response = http_request("get",{},url,60)
        if response["code"] == 200
            return true,response
        else
            #status = false
            #error = response["body"]["error"]["message"]
            return false,response["body"]["error"]["message"]
            #error = "Could not create at OTT!"
        end
        return status,error
    end

    def edit_videolist(metadata,catalog,videolist_id)
        status = true
        error = ""
        maxtime = 60
        body = {}
        body["auth_token"] = BusinessGroup.find(self.bg_id).ott_auth_token
        body["video_list"] = metadata
        url = "http://"+get_ott_base_url+"/catalogs/"+self.ott_catalog_id+"/videolists/"+videolist_id
        response = http_request("put",body,url,maxtime)
        if response["code"] == 200
            self.ott_status = "edit"
            self.save
        else
            status = false
            error = response["body"]["error"]["message"]
            #error = "Could not create at OTT!"
        end
        return status,error
    end

    def publish_unpublish_data
        media = Media.where(:ott_status => "unpublished",:ott_publish_date => Time.now.utc)
        if media.count > 0
            media.each do |video|
               video.publish_at_ott 
            end    
        end    
    end    

end

class ConfigData
  include Mongoid::Document
  include Mongoid::Timestamps

#
  field :access_key, type: String
#
  field :data, type: Hash
#
  field :title, type: String


end
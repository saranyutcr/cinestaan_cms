class Emailer < ActionMailer::Base
  #default :from => 'vcms@saranyu.in'
  default :from => 'instaott@saranyu.in'
  add_template_helper(ApplicationHelper)
  
  # send a signup email to the user, pass in the user object that   contains the user's email address
  def send_signup_email(user,password)
    @user = user
    @password = password
    mail( :to => @user.email,
    :subject => 'Saranyu Encoder: Get your smart URL here!' )
  end

  def send_published_email(user,media)
  	@user = user
  	@media = media
  	mail( :to => @user.email,
    :subject => 'Saranyu Encoder: Your media is published!' )
  end

  def send_register_email(user)
    @user = user
    mail(:to => @user.email,
      :subject => 'Thanks for registering with InstaOTT')
  end

  def send_confirm_email(user)
    @user = user
    mail(:to => @user.email,
      :subject => "Welcome to InstaOTT")
  end

  def send_forgot_email(user)
    @user = user
    mail(:to => @user.email,
      :subject => "Your Forgot Password Link")
  end

  def send_user_register(user)
    @user = user
    mail(:to => @user.email,
      :subject => "Thanks for registering with InstaOTT")
  end

end

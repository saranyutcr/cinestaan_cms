module HomeHelper
	def has_custom_keys(naming_tag)
		status = false
		naming_tag["constructor"].each do |element|
			if element["type"] == "custom"
				status = true
				break;
			end
		end
		return status
	end
end

module AppsHelper
	def create_apps(tenant,platforms,names)
		status = true
		error = ""
		bg = BusinessGroup.find(tenant.bg_id)
		response = nil
		app_list = []
		if status
			body = {}
			body["auth_token"] = tenant.ott_auth_token
			body["apps"] = platforms
			body["names"] = names
			url = "http://"+get_ott_base_url+"/apps"
			response = http_request("post",body,url,60)
			if !response["status"]
				status = false
				error = response["body"]["error"]["message"]
			end
		end
		if status
			Rails.logger.debug response["body"]["data"]["items"]
			response["body"]["data"]["items"].each do |app_data|
				Rails.logger.debug app_data
				app = App.new
				app.name = app_data["name"]
				app.platform = app_data["app_type"]
				app.ott_id = app_data["application_id"]
				app.ott_auth_token = app_data["authentication_token"]
				app.bg_id = bg.id
				app.tenant_id = tenant.id
				if app.save
					app_list << app_data["application_id"]
				end
			end
		end
		return status,error,app_list
	end
end

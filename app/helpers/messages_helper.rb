module MessagesHelper

	# Create Message Catalog
	def create_catalog_message_ott(bg,tenant)
	status = true
		error = ""
		response = nil
		if status
			body = {}
			body["auth_token"] = bg.ott_auth_token
			body["catalog_data"] = {}
			body["catalog_data"]["name"] = "message"
			body["catalog_data"]["theme"] = "message"
			body["catalog_data"]["title"] = "message"
			body["catalog_data"]["apps"] = []
			tenant_ids = []
			App.where(:tenant_id => tenant.id).each do |app_id|
				app = App.find(app_id)
				body["catalog_data"]["apps"] << app.ott_id
				tenant_ids << app.tenant_id
			end
			tenant_ids = tenant_ids.uniq

			if params[:edit] == "update"
				# catalog = Catalog.find_by(:bg_id => bg.id, :theme => "message", :tenant_id => tenant.id.to_s)
				# url = "http://"+get_ott_base_url+"/catalogs/"+catalog.ott_id
				# response = http_request("put",body,url,60)
			else
				url = "http://"+get_ott_base_url+"/catalogs"
		    	response = http_request("post",body,url,60)
				catalog = Catalog.new
			end
			
			if !response["status"]
				status = false
				error = response["error"]
			end
		end
		if status
			catalog.name = "message"
			catalog.theme = "message"
			catalog.app_id = []
			catalog.tenant_id = []
			App.where(:tenant_id => tenant.id).each do |app_id|
				app = App.find(app_id)
				catalog.app_id << app.id
				catalog.tenant_id << app.tenant_id
			end
			catalog.tenant_id = catalog.tenant_id.uniq
			catalog.bg_id = bg.id
			catalog.ott_id = response["body"]["data"]["catalog_id"]
			catalog.save
		end
		return catalog
	end


	def message_items(bg,tenant,type)
		status = true
	    metadata = {}
		metadata["status"] = "published"
		metadata["business_group_id"] = bg.ott_id
		if type[:regions]
			metadata["regions"] = type[:regions].split(",").map(&:strip)
		else
			metadata["regions"] = ["IN"]
		end

		metadata["tenant_ids"] = []
		tenantotid = Tenant.find_by(:id => tenant.id)
		metadata["tenant_ids"] << tenant.ott_id

		metadata["app_ids"] = []
		App.where(:tenant_id => tenant.id).each do |app_id|
			app = App.find(app_id)
			metadata["app_ids"] << app.ott_id
		end

		metadata["title"] = type["title"] 
		metadata["platform_code"] = type["platform_code"]
		metadata["app_code"] = type["platform_code"]
		metadata["platform_note"] = type["platform_note"]
		metadata["text_message"] = type["text_message"]
		metadata["type"] = type["type"]

		if type["type"] == "static_page"
			params_hash2 = {}
			config_params = {}
			params_hash2["config_params"] = config_params
			template_params = {}
			params_hash2["template_params"] = template_params
			app_message_params = {}
			params_hash2["app_message_params"] = app_message_params
			static_page = {}
			static_page["display_title"] = type["display_title"]
			static_page["description"] = type["description"]
			params_hash2["static_page"] = static_page
			cms_params = {}
			params_hash2["cms_params"] = cms_params
			metadata["params_hash2"] = params_hash2
		end
		if type["type"] == "template"
			params_hash2 = {}
			config_params = {}
			params_hash2["config_params"] = config_params
			template_params = {}
			template_params["subject"] = type["subject"]
			template_params["body"] = type["body"]
			params_hash2["template_params"] = template_params
			app_message_params = {}
			params_hash2["app_message_params"] = app_message_params
			static_page = {}
			params_hash2["static_page"] = static_page
			cms_params = {}
			params_hash2["cms_params"] = cms_params
			metadata["params_hash2"] = params_hash2
		end
		if type["type"] == "app_message"
			params_hash2 = {}
			config_params = {}
			params_hash2["config_params"] = config_params
			template_params = {}
			params_hash2["template_params"] = template_params
			app_message_params = {}
			app_message_params["message"] = type["message"]
			params_hash2["app_message_params"] = app_message_params
			static_page = {}
			params_hash2["static_page"] = static_page
			cms_params = {}
			params_hash2["cms_params"] = cms_params
			metadata["params_hash2"] = params_hash2
		end
		if type["type"] == "config_params"
			params_hash2 = {}
			config_params = {}
			session_params = {}
			session_params["max_number_of_sessions_per_user"] = type["max_number_of_sessions_per_user"]
			session_params["max_number_of_devices"] = type["max_number_of_devices"]
			session_params["device_lock"] = type["device_lock"]
			session_params["delete_older_session"] = type["delete_older_session"]
			session_params["delete_device"] = type["delete_device"]
			session_params["override_from_user_plans"] = type["override_from_user_plans"]
			session_params["old_flow"] = type["old_flow"]

			registration_params = {}
			registration_params["verification_flag"] = type["verification_flag"]
			registration_params["send_welcome_email_for_internal_reg"] = type["send_welcome_email_for_internal_reg"]
			registration_params["send_welcome_email_for_external_login"] = type["send_welcome_email_for_external_login"]

			email_config_params = {}
			portal_base_address = "http://" + get_web_view_base_url
			if bg.friendly_id
				portal_base_url = portal_base_address+"/WEBVIEW/"+bg.friendly_id+"/"
			else	
				portal_base_url = portal_base_address
			end
			# email_config_params["portal_base_url"] = type["portal_base_url"]
			email_config_params["portal_base_url"] = portal_base_url
			# email_config_params["sender_email"] = type["sender_email"]
			email_config_params["sender_email"] = "instaott@saranyu.in"

			config_params["session_params"] = session_params
			config_params["registration_params"] = registration_params
			config_params["email_config_params"] = email_config_params
			params_hash2["config_params"] = config_params

			template_params = {}
			params_hash2["template_params"] = template_params
			app_message_params = {}
			params_hash2["app_message_params"] = app_message_params
			static_page = {}
			params_hash2["static_page"] = static_page
			cms_params = {}
			cms_params["logo"] = "https://s3-ap-southeast-1.amazonaws.com/ott-as-service/ott_default_images/logo100x60.png"
			cms_params["template"] = "template1"
			cms_params["color"] = "red"
			cms_params["text"] = "SARANYU"
			params_hash2["cms_params"] = cms_params
			metadata["params_hash2"] = params_hash2
		end

		status = true
        error = ""
        maxtime = 60
        body = {}
        body["auth_token"] = bg.ott_auth_token
        body["content"] = metadata

        catalogs = Catalog.find_by(:bg_id => bg.id, :theme => "message", :tenant_id => tenant.id.to_s)

        if type[:method] == "update"
        content_id = type[:content_id]
		url = "http://"+get_ott_base_url+"/catalogs/"+catalogs.ott_id+"/items/"+content_id+"?auth_token="+@bg.ott_auth_token
		response = http_request("put",body,url,maxtime)
		if response["code"] == 200
        else
            status = response["status"]
        end
        else
		url = "http://"+get_ott_base_url+"/catalogs/"+catalogs.ott_id+"/items?auth_token="+bg.ott_auth_token  
        response = http_request("post",body,url,maxtime)
        if response["code"] == 200
        else
            status = response["status"]
        end
    	end

        return response

	end

	def create_config_params(bg,tenant)
		status = true
	       metadata = {}
			
			metadata["title"] = "Config Params"
			metadata["status"] = "published"
			metadata["business_group_id"] = bg.ott_id

			if params[:regions]
				metadata["regions"] = params[:regions].split(",").map(&:strip)
			else
				metadata["regions"] = ["IN"]
			end

			metadata["tenant_ids"] = []
			tenantotid = Tenant.find_by(:id => tenant.id)
			metadata["tenant_ids"] << tenant.ott_id

			metadata["app_ids"] = []
			App.where(:tenant_id => tenant.id).each do |app_id|
				app = App.find(app_id)
				metadata["app_ids"] << app.ott_id
			end

			metadata["platform_code"] = "5000"
			metadata["app_code"] = "5000"
			metadata["platform_note"] = "Registration Details"
			metadata["text_message"] = "Thanks for registration!"
			metadata["type"] = "config_params"

			params_hash2 = {}
			config_params = {}

			session_params = {}
			session_params["max_number_of_sessions_per_user"] = 1
			session_params["max_number_of_devices"] = 1
			session_params["device_lock"] = "false"
			session_params["delete_older_session"] = "true"
			session_params["delete_device"] = "true"
			session_params["override_from_user_plans"] = "true"
			session_params["old_flow"] = "true"

			registration_params = {}
			registration_params["verification_flag"] = "true"
			registration_params["send_welcome_email_for_internal_reg"] = "true"
			registration_params["send_welcome_email_for_external_login"] = "true"

			email_config_params = {}
			email_config_params["portal_base_url"] = "http://128.199.119.23:4000/"
			email_config_params["sender_email"] = "instaott@saranyu.in"


			config_params["session_params"] = session_params
			config_params["registration_params"] = registration_params
			config_params["email_config_params"] = email_config_params
			params_hash2["config_params"] = config_params

			template_params = {}
			params_hash2["template_params"] = template_params

			app_message_params = {}
			params_hash2["app_message_params"] = app_message_params

			static_page = {}
			params_hash2["static_page"] = static_page

			metadata["params_hash2"] = params_hash2
			
		status = true
        error = ""
        maxtime = 60
        body = {}
        body["auth_token"] = bg.ott_auth_token
        body["content"] = metadata

        catalogs = Catalog.find_by(:bg_id => bg.id, :theme => "message", :tenant_id => tenant.id.to_s)

        if params[:method] == "update"
        else
		url = "http://"+get_ott_base_url+"/catalogs/"+catalogs.ott_id+"/items?auth_token="+bg.ott_auth_token  
        response = http_request("post",body,url,maxtime)
        if response["code"] == 200
        else
            status = response["status"]
        end
    	end

        return response
	end


	def create_confirm_email(bg,tenant)
		status = true
	       metadata = {}
			
			metadata["title"] = "Verification Mail"
			metadata["status"] = "published"
			metadata["business_group_id"] = bg.ott_id

			if params[:regions]
				metadata["regions"] = params[:regions].split(",").map(&:strip)
			else
				metadata["regions"] = ["IN"]
			end

			metadata["tenant_ids"] = []
			tenantotid = Tenant.find_by(:id => tenant.id)
			metadata["tenant_ids"] << tenant.ott_id

			metadata["app_ids"] = []
			App.where(:tenant_id => tenant.id).each do |app_id|
				app = App.find(app_id)
				metadata["app_ids"] << app.ott_id
			end	

			metadata["platform_code"] = "1001"
			metadata["app_code"] = "1001"
			metadata["platform_note"] = "Confirmation email template"
			metadata["text_message"] = "Please confirm your email"
			metadata["email_template"] = ""
			metadata["type"] = "template"

			params_hash2 = {}
			config_params = {}
			params_hash2["config_params"] = config_params

			template_params = {}
			template_params["subject"] = "Please confirm your email"
			template_params["body"] = "<table width='100%' cellpadding='2' cellspacing='2' frame='box' rules='none' style='border:2px solid #FF0031;'>
			<tr><td style='border-bottom: 1px solid #FF743A;'>
			<img style='height: 200px; padding-left: 30px;' src='http://162.243.123.230/assets/icons/email_logo_image.png'>
			</td></tr>
			<tr><td style='padding-left: 20px;font-size: small;font-family:arial;padding-top: 20px;'>Hello <b> $name$ ! </b> </td>
			</tr><tr><td style='padding-left: 20px;font-size: small;font-family:arial;padding-top: 20px;'>Greetings from 
			<a href='#'>http://www.saranyutv.com</a></td></tr>
			<tr><td style='padding-left: 20px;font-size: small;font-family:arial;padding-top: 20px;'>
			We have received a request to the confirmation email for your account on $time$</td></tr>
			<tr><td  style='padding-left: 20px;font-size: small;font-family:arial;padding-top: 20px;'>
			If the link is not clickable, please copy the url shown below and paste in your browser address bar to visit the reset page.</td></tr>
			<tr><td  style='padding-left: 20px;font-size: small;font-family:arial;padding-top: 20px;'>
			<a href='$verification_link$'>$verification_link$</a></td></tr>
			<tr><td style='padding-left: 20px;font-size: small;font-family:arial;padding-top: 20px;'>
			If you do not wish to confirm your account, please disregard this message.</td></tr><tr>
			<td  style='padding-left: 20px;font-size: small;font-family:arial;padding-top: 20px;'><b> Hope this helps ! </b></td></tr>
			<tr><td style='padding-left: 20px;font-size: small;font-family:arial;padding-top: 20px;padding-bottom: 50px;padding-top: 20px;border-bottom: 1px solid #FF743A;'>Best Regards, <br/>Team 
			<a href='http://www.saranyutv.com'>http://www.saranyutv.com</a></td></tr><tr><td style='padding-top: 15px;color: red;font-size: 16px;text-align: center;padding-bottom: 12px;'>This is an auto-generated email. Please do not reply to the same.</td></tr></table>"

			params_hash2["template_params"] = template_params

			app_message_params = {}
			params_hash2["app_message_params"] = app_message_params

			static_page = {}
			params_hash2["static_page"] = static_page

			metadata["params_hash2"] = params_hash2
			
		status = true
        error = ""
        maxtime = 60
        body = {}
        body["auth_token"] = bg.ott_auth_token
        body["content"] = metadata

        catalogs = Catalog.find_by(:bg_id => bg.id, :theme => "message", :tenant_id => tenant.id.to_s)

        if params[:method] == "update"
        else
		url = "http://"+get_ott_base_url+"/catalogs/"+catalogs.ott_id+"/items?auth_token="+bg.ott_auth_token  
        response = http_request("post",body,url,maxtime)
        if response["code"] == 200
        else
            status = response["status"]
        end
    	end

        return response
	end

	def create_welcome_email(bg,tenant)
		status = true
	       metadata = {}
			
			metadata["title"] = "Welcome Template"
			metadata["status"] = "published"
			metadata["business_group_id"] = bg.ott_id

			if params[:regions]
				metadata["regions"] = params[:regions].split(",").map(&:strip)
			else
				metadata["regions"] = ["IN"]
			end

			metadata["tenant_ids"] = []
			tenantotid = Tenant.find_by(:id => tenant.id)
			metadata["tenant_ids"] << tenant.ott_id

			metadata["app_ids"] = []
			App.where(:tenant_id => tenant.id).each do |app_id|
				app = App.find(app_id)
				metadata["app_ids"] << app.ott_id
			end	

			metadata["platform_code"] = "1002"
			metadata["app_code"] = "1002"
			metadata["platform_note"] = "Welcome email template"
			metadata["text_message"] = "Welcome to saranyutv"
			metadata["email_template"] = ""
			metadata["type"] = "template"

			params_hash2 = {}
			config_params = {}
			params_hash2["config_params"] = config_params
			
			template_params = {}
			template_params["subject"] = "Welcome to saranyutv"
			template_params["body"] = "<table cellpadding='2' cellspacing='2' width='100%' frame='box' rules='none' style='border:2px solid #FF0031;'>
			<tr><td class='bord' colspan='2' style='border-bottom: 1px solid #FF743A;'>
			<img style='height: 200px; padding-left: 30px;' src='http://162.243.123.230/assets/icons/email_logo_image.png'></td></tr>
			<tr><td style='padding-top: 10px;padding-left: 20px;font-size: small;font-family:arial;' height='20' colspan='2'></td></tr>
			<tr><td colspan='2' style='padding-top: 10px;padding-bottom: 10px;font-size: small;font-family:arial;text-align: center;color:#ffffff;font-weight: bold;'>
			<div style='background: #FF0031;width:18%;margin-left:-2px;padding-top: 10px;padding-bottom: 10px;font-size: small;font-family:arial;text-align: center;color:#ffffff;font-weight: bold;'>
			Registration Details</div></td></tr><tr><th></th><th></th></tr>  
			<tr><td colspan='2'  style='padding-top: 10px;padding-left: 20px;font-size: small;font-family:arial;'>Dear <b> $name$, </b> </td>
			</tr><tr><td colspan='2' style='padding-top: 10px;padding-left: 20px;font-size: small;font-family:arial;'>Welcome to 
			<b style='color: #FF0031;'>saranyutv</b>, thank you for your registration with us.</td></tr>
			<tr> <td colspan='2' style='padding-top: 10px;padding-left: 20px;font-size: small;font-family:arial;'>Your login information is as follows:</td> </tr>
			<tr><td  colspan='2' style='padding-top: 10px;padding-left: 20px;font-size: small;font-family:arial;'>
			<div style='border: 2px solid #AAA9AA !important;width: 96%;background-color: #ECECEC;'><b>Username :</b> 
			<span style='color: #423AF6'>$user_email$</span> <br/></div></td></tr>
			<tr><td colspan='2'  style='padding-top: 10px;padding-left: 20px;font-size: small;font-family:arial;'>To login, please access 
			<b style='color: #FF0031;'>saranyutv</b> and type the username above.</tr>
			<tr><td  colspan='2'  style='padding-top: 10px;padding-left: 20px;font-size: small;font-family:arial;'>
			We request you to make the note of this information for future reference.<br/>At <b style='color: #FF0031;'>saranyutv</b> 
			you can choose products from several categories and purchase them for your own use or to gift others. Simply log in and shop to your hearts content. </tr>
			<tr><td colspan='2'  style='padding-top: 10px;padding-left: 20px;font-size: small;font-family:arial;'>If you problems signing in, please call or email us.
			</tr><tr><td colspan='2'  style='padding-top: 10px;padding-left: 20px;font-size: small;font-family:arial;padding-bottom: 30px;padding-top: 20px;'>Warm Regards,<br/>
			<b style='color: #FF0031;'>saranyutv</b> <b>Team</b></tr><tr style='border-bottom: 1px solid #FF743A;'><td  colspan='2'  style='padding-left: 20px;font-size: small;font-family:arial;padding-bottom:50px;'>
			<b>Note:</b> You can change password by logging to <span style='color: #FF0031;'>SaranyuTV</span> and clicking on change password link in <b>My Account </b> section.<br/> 
			You can use the same username and password to login to the <b style='color: #FF0031;'>saranyutv</b> and its sub stores.</td></tr>
			<tr style='border-right:2px solid #FF0031;'><td  colspan='2' style='color: #FF7A40;font-size: small;text-align: center;padding-top: 15px;padding-bottom: 12px;'>
			This is an auto-generated email. Please do not reply to the same.</td></tr></table>"

			params_hash2["template_params"] = template_params

			app_message_params = {}
			params_hash2["app_message_params"] = app_message_params

			static_page = {}
			params_hash2["static_page"] = static_page

			metadata["params_hash2"] = params_hash2
			
		status = true
        error = ""
        maxtime = 60
        body = {}
        body["auth_token"] = bg.ott_auth_token
        body["content"] = metadata

        catalogs = Catalog.find_by(:bg_id => bg.id, :theme => "message", :tenant_id => tenant.id.to_s)

        if params[:method] == "update"
        else
		url = "http://"+get_ott_base_url+"/catalogs/"+catalogs.ott_id+"/items?auth_token="+bg.ott_auth_token  
        response = http_request("post",body,url,maxtime)
        if response["code"] == 200
        else
            status = response["status"]
        end
    	end

        return response
	end


	def create_reconfirm_email(bg,tenant)
		status = true
	       metadata = {}
			
			metadata["title"] = "Confirm Email"
			metadata["status"] = "published"
			metadata["business_group_id"] = bg.ott_id

			if params[:regions]
				metadata["regions"] = params[:regions].split(",").map(&:strip)
			else
				metadata["regions"] = ["IN"]
			end

			metadata["tenant_ids"] = []
			tenantotid = Tenant.find_by(:id => tenant.id)
			metadata["tenant_ids"] << tenant.ott_id

			metadata["app_ids"] = []
			App.where(:tenant_id => tenant.id).each do |app_id|
				app = App.find(app_id)
				metadata["app_ids"] << app.ott_id
			end		

			metadata["platform_code"] = "1003"
			metadata["app_code"] = "1003"
			metadata["platform_note"] = "Confirmation email template"
			metadata["text_message"] = "Please confirm your email"
			metadata["email_template"] = ""
			metadata["type"] = "template"

			params_hash2 = {}
			config_params = {}
			params_hash2["config_params"] = config_params

			template_params = {}
			template_params["subject"] = "Please confirm your email"
			template_params["body"] = "<table width='100%' cellpadding='2' cellspacing='2' frame='box' rules='none'  style='border:2px solid #FF0031;'>
			<tr><td style='border-bottom: 1px solid #FF743A;'>
			<img style='height: 200px; padding-left: 30px;' src='http://162.243.123.230/assets/icons/email_logo_image.png'></td></tr>
			<tr><td style='padding-left: 20px;font-size: small;font-family:arial;padding-top: 20px;'>Hello <b> $name$ ! </b> </td></tr>
			<tr><td style='padding-left: 20px;font-size: small;font-family:arial;padding-top: 20px;'>Greetings from <a href='#'>http://www.saranyutv.com</a></td></tr>
			<tr><td style='padding-left: 20px;font-size: small;font-family:arial;padding-top: 20px;'>We have received a request to resend the confirmation email for your account on $time$</td></tr>
			<tr><td style='padding-left: 20px;font-size: small;font-family:arial;padding-top: 20px;'>If the link is not clickable, please copy the url shown below and paste in your browser address bar to visit the reset page.</td></tr>
			<tr><td style='padding-left: 20px;font-size: small;font-family:arial;padding-top: 20px;'><a href='$verification_link$'>$verification_link$</a></td></tr>
			<tr><td style='padding-left: 20px;font-size: small;font-family:arial;padding-top: 20px;'>If you do not wish to confirm your account, please disregard this message.</td></tr>
				<tr><td style='padding-left: 20px;font-size: small;font-family:arial;padding-top: 20px;'><b> Hope this helps</b></td></tr>
				<tr><td style='padding-left: 20px;font-size: small;font-family:arial;padding-top: 20px;padding-bottom: 50px;border-bottom: 1px solid #FF743A;'>Best Regards, 
				<br/>Team <a href='http://www.saranyutv.com'>http://www.saranyutv.com</a></td></tr>
				<tr><td style='padding-top: 15px;color: red;font-size: 16px;text-align: center;padding-bottom: 12px;'>
				This is an auto-generated email. Please do not reply to the same.</td></tr></table>"

			params_hash2["template_params"] = template_params

			app_message_params = {}
			params_hash2["app_message_params"] = app_message_params

			static_page = {}
			params_hash2["static_page"] = static_page

			metadata["params_hash2"] = params_hash2
			
		status = true
        error = ""
        maxtime = 60
        body = {}
        body["auth_token"] = bg.ott_auth_token
        body["content"] = metadata

        catalogs = Catalog.find_by(:bg_id => bg.id, :theme => "message", :tenant_id => tenant.id.to_s)

        if params[:method] == "update"
        else
		url = "http://"+get_ott_base_url+"/catalogs/"+catalogs.ott_id+"/items?auth_token="+bg.ott_auth_token  
        response = http_request("post",body,url,maxtime)
        if response["code"] == 200
        else
            status = response["status"]
        end
    	end

        return response
	end


	def create_forgot_password(bg,tenant)
		status = true
	       metadata = {}
			
			metadata["title"] = "Forgot Password"
			metadata["status"] = "published"
			metadata["business_group_id"] = bg.ott_id

			if params[:regions]
				metadata["regions"] = params[:regions].split(",").map(&:strip)
			else
				metadata["regions"] = ["IN"]
			end

			metadata["tenant_ids"] = []
			tenantotid = Tenant.find_by(:id => tenant.id)
			metadata["tenant_ids"] << tenant.ott_id

			metadata["app_ids"] = []
			App.where(:tenant_id => tenant.id).each do |app_id|
				app = App.find(app_id)
				metadata["app_ids"] << app.ott_id
			end	

			metadata["platform_code"] = "1004"
			metadata["app_code"] = "1004"
			metadata["platform_note"] = "Forgot Password email template"
			metadata["text_message"] = "Please Click on this link to reset your password"
			metadata["email_template"] = ""
			metadata["type"] = "template"

			params_hash2 = {}
			config_params = {}
			params_hash2["config_params"] = config_params

			template_params = {}
			template_params["subject"] = "Please Click on this link to reset your password"
			template_params["body"] = "<table width='100%' cellpadding='2' cellspacing='2' frame='box' style='border:2px solid #FF0031;'>
			<tr><td style='border-bottom: 1px solid #FF743A;'>
			<img style='height: 200px; padding-left: 30px;' src='http://162.243.123.230/assets/icons/email_logo_image.png'></td></tr>
			<tr><td style='padding-left: 20px;font-size: small;font-family:arial;padding-top: 20px;'>Hello <b> $name$ ! </b> </td></tr>
			<tr><td style='padding-left: 20px;font-size: small;font-family:arial;padding-top: 20px;'>Greetings from 
			<a href='#'>http://www.saranyutv.com</a></td></tr>
			<tr><td style='padding-left: 20px;font-size: small;font-family:arial;padding-top: 20px;'>
			We have received a request to reset the password for your account on $time$</td></tr>
			<tr><td style='padding-left: 20px;font-size: small;font-family:arial;padding-top: 20px;'>
			Please reset your password by clicking here</td></tr></tr>
			<tr><td style='padding-left: 20px;font-size: small;font-family:arial;padding-top: 20px;'>
			If the link is not clickable, please copy the url shown below and paste in your browser address bar to visit the reset page.</td></tr>
			<tr><td style='padding-left: 20px;font-size: small;font-family:arial;padding-top: 20px;'>
			<a href='$reset_link$'>$reset_link$</a></td></tr>
			<tr><td style='padding-left: 20px;font-size: small;font-family:arial;padding-top: 20px;'>
			If you do not wish to reset your password, please disregard this message.</td></tr>
			<tr><td style='padding-left: 20px;font-size: small;font-family:arial;padding-top: 20px;'><b> Hope this helps ! </b> </td></tr>
			<tr><td style='padding-left: 20px;font-size: small;font-family:arial;padding-top: 20px;padding-bottom: 50px;border-bottom: 1px solid #FF743A;'>Best Regards, 
			<br/>Team <a href='http://www.saranyutv.com'>http://www.saranyutv.com</a></td></tr>
			<tr><td style='padding-top: 15px;color: red;font-size: 16px;text-align: center;padding-bottom: 12px;'>
			This is an auto-generated email. Please do not reply to the same.</td></tr></table>"

			params_hash2["template_params"] = template_params

			app_message_params = {}
			params_hash2["app_message_params"] = app_message_params

			static_page = {}
			params_hash2["static_page"] = static_page

			metadata["params_hash2"] = params_hash2
			
		status = true
        error = ""
        maxtime = 60
        body = {}
        body["auth_token"] = bg.ott_auth_token
        body["content"] = metadata

        catalogs = Catalog.find_by(:bg_id => bg.id, :theme => "message", :tenant_id => tenant.id.to_s)

        if params[:method] == "update"
        else
		url = "http://"+get_ott_base_url+"/catalogs/"+catalogs.ott_id+"/items?auth_token="+bg.ott_auth_token  
        response = http_request("post",body,url,maxtime)
        if response["code"] == 200
        else
            status = response["status"]
        end
    	end

        return response
	end


	def create_about_us(bg,tenant)
		status = true
	       metadata = {}
			
			metadata["title"] = "About Us"
			metadata["status"] = "published"
			metadata["business_group_id"] = bg.ott_id

			if params[:regions]
				metadata["regions"] = params[:regions].split(",").map(&:strip)
			else
				metadata["regions"] = ["IN"]
			end

			metadata["tenant_ids"] = []
			tenantotid = Tenant.find_by(:id => tenant.id)
			metadata["tenant_ids"] << tenant.ott_id

			metadata["app_ids"] = []
			App.where(:tenant_id => tenant.id).each do |app_id|
				app = App.find(app_id)
				metadata["app_ids"] << app.ott_id
			end	

			metadata["platform_code"] = "1005"
			metadata["app_code"] = "1005"
			metadata["platform_note"] = "About Us Page"
			metadata["text_message"] = "About Us"
			metadata["email_template"] = ""
			metadata["type"] = "static_page"

			params_hash2 = {}
			config_params = {}
			params_hash2["config_params"] = config_params

			template_params = {}
			params_hash2["template_params"] = template_params

			app_message_params = {}
			params_hash2["app_message_params"] = app_message_params

			static_page = {}
			static_page["display_title"] = "About Us"
			static_page["description"] = "<p>The  Saranyu TV platform is owned by Saranyu Pvt Ltd. The Saranyu TV is one of the most fascinating catalogue over offered on mobile and the web making 
			it a complete video destination for consumers. The podium has been designed and purpose-built for one reason- to be the best video experience for TV Shows, Spiritual programmes, all in a single destination.</p>
			<p>User friendly features</p><p>Video Playback Experience- Through video technology optimizations we have enabled adaptive video streaming that adapts itself to deliver the 
			best possible video quality basis available bandwidth and player technology ensures that there is never any compromise on audio-video 
			quality.</p><p>Collection, Chapters and Curation  It can be stunning for our users when they are able to choose from a catalogue as big as that of on a theme and calls it a chapter. Keeping our viewers interests in mind, 
			we have also clubbed content to make collections of video clips, episodes of a show and similar programmes.</p>"

			params_hash2["static_page"] = static_page

			metadata["params_hash2"] = params_hash2
			
		status = true
        error = ""
        maxtime = 60
        body = {}
        body["auth_token"] = bg.ott_auth_token
        body["content"] = metadata

        catalogs = Catalog.find_by(:bg_id => bg.id, :theme => "message", :tenant_id => tenant.id.to_s)

        if params[:method] == "update"
        else
		url = "http://"+get_ott_base_url+"/catalogs/"+catalogs.ott_id+"/items?auth_token="+bg.ott_auth_token  
        response = http_request("post",body,url,maxtime)
        if response["code"] == 200
        else
            status = response["status"]
        end
    	end

        return response
	end


	def create_term_and_cond(bg,tenant)
		status = true
	       metadata = {}
			
			metadata["title"] = "Terms and Conditions"
			metadata["status"] = "published"
			metadata["business_group_id"] = bg.ott_id

			if params[:regions]
				metadata["regions"] = params[:regions].split(",").map(&:strip)
			else
				metadata["regions"] = ["IN"]
			end

			metadata["tenant_ids"] = []
			tenantotid = Tenant.find_by(:id => tenant.id)
			metadata["tenant_ids"] << tenant.ott_id

			metadata["app_ids"] = []
			App.where(:tenant_id => tenant.id).each do |app_id|
				app = App.find(app_id)
				metadata["app_ids"] << app.ott_id
			end	

			metadata["platform_code"] = "1006"
			metadata["app_code"] = "1006"
			metadata["platform_note"] = "terms and conditions"
			metadata["text_message"] = "terms and conditions"
			metadata["email_template"] = ""
			metadata["type"] = "static_page"

			params_hash2 = {}
			config_params = {}
			params_hash2["config_params"] = config_params

			template_params = {}
			params_hash2["template_params"] = template_params

			app_message_params = {}
			params_hash2["app_message_params"] = app_message_params

			static_page = {}
			static_page["display_title"] = "TERMS AND CONDITIONS"
			static_page["description"] = "<p>By Accessing This Website, You Consent, Agree and Undertake to abide, 
			be bound by and adhere to the terms of use  (Terms of use or Terms or Terms & Conditions) and
			if you do not agree to these terms of use, you are not entitled to avail of/use thereafter shall be unauthorized.</p>
			<p>Descrpition of Service and Acceptance of Terms of Use Saranyu Pvt Ltd is a company incorporated under Companies Act, 
			1956, having its office in Banaglore, India. Saranyu TV provides an online website 'www.Saranyutv.com' ('Site') as an 
			interactive platform for its viewers and any associated mobile sites, applications ('Apps') to inter alia view episodes 
			and programmes, spiritual, devotional, religious content, view videos and /or photos of their favorite characters on 
			serials, read recap of serials, international content including viewing schedules of content offered etc. on the Site 
			and any other features, tools, applications, materials, or other services offered from time to time by Saranyu TV in 
			connection with its business, provided on the Site ('Content') and post comments. You may be able to access the Services 
			including Content through mobile phones, tablets and other IP based connected devices.</p><p>Electronic Communications The 
			Content contained in the Saranyu Media including but not limited to on the Site is protected by copyright, trademark, patent, 
			rade secret and other laws, and shall not be used except as provided in these Terms and Conditions, without the written 
			permission of Saranyu.</p><p>Access and Use of the Services Saranyu grants you a non-exclusive limited, revocable, limited 
			permission to use the Services on the Site for personal, non-commercial purposes during the subsistence of Your Account for 
			the territory of the world or limited territories as applicable in respect of specified content and as set forth in these 
			Terms and Conditions.</p><p> By agreeing to use/avail the Services :You confirm and warrant that all the data and information 
			supplied by You when You register (if applicable) and provided by You to Saranyu and or any Saranyu's affiliates, distributors 
			('Registration Data') is true, current, complete and accurate in all respects.You agree to promptly update Your Registration Data, 
			so that Your information remains true, current, complete, and accurate at all times. You acknowledge and agree that 
			Your Registration Data and any other personal data collected by Saranyu is subject to Saranyu's Privacy Policy. 
			For more information, view the full Privacy Policy. Saranyu may collect information such as occupation, language, 
			pincode, area code, unique device identifier, location, and the time zone where the Services are used and may use 
			these to target advertising and make content recommendations.You acknowledge, consent and agree that Saranyu may access, 
			preserve, transfer and disclose Your account information and/or User Material (see below) subject to the Privacy Policy, 
			if required to do so by law to co-operate with mandated government and law enforcement agencies or to any private parties
			 by an order under law for the time being in force to enforce and comply with the law including to various tax authorities 
			 upon any demand or request by them or in a good faith belief that such access, preservation, or disclosure is reasonably 
			 necessary to: (a) comply with legal process and/or any applicable law in the territory; (b) enforce the Terms and Conditions; 
			 (c) respond to Your requests for customer service; or (d) protect the rights, property or personal safety of Saranyu TV, 
			 affiliates, its users and the public.</p><p>You agree that Saranyu TV has the right to temporarily suspend access to the whole 
			 or any part of the Services for any technical/operational reason and shall be under no liability to You in such an event. 
			 Saranyu TV may, but shall not be obliged to, give You as much notice of any interruption of access to the Services as is 
			 reasonably practicable. Saranyu TV will restore access to the Services as soon as reasonably practicable after temporary 
			 suspension.</p>"

			params_hash2["static_page"] = static_page

			metadata["params_hash2"] = params_hash2
			
		status = true
        error = ""
        maxtime = 60
        body = {}
        body["auth_token"] = bg.ott_auth_token
        body["content"] = metadata

        catalogs = Catalog.find_by(:bg_id => bg.id, :theme => "message", :tenant_id => tenant.id.to_s)

        if params[:method] == "update"
        else
		url = "http://"+get_ott_base_url+"/catalogs/"+catalogs.ott_id+"/items?auth_token="+bg.ott_auth_token  
        response = http_request("post",body,url,maxtime)
        if response["code"] == 200
        else
            status = response["status"]
        end
    	end

        return response
	end


	def create_privacy_policy(bg,tenant)
		status = true
	       metadata = {}
			
			metadata["title"] = "Privacy Policy"
			metadata["status"] = "published"
			metadata["business_group_id"] = bg.ott_id

			if params[:regions]
				metadata["regions"] = params[:regions].split(",").map(&:strip)
			else
				metadata["regions"] = ["IN"]
			end

			metadata["tenant_ids"] = []
			tenantotid = Tenant.find_by(:id => tenant.id)
			metadata["tenant_ids"] << tenant.ott_id

			metadata["app_ids"] = []
			App.where(:tenant_id => tenant.id).each do |app_id|
				app = App.find(app_id)
				metadata["app_ids"] << app.ott_id
			end	

			metadata["platform_code"] = "1007"
			metadata["app_code"] = "1007"
			metadata["platform_note"] = "privacy policy"
			metadata["text_message"] = "privacy policy"
			metadata["email_template"] = ""
			metadata["type"] = "static_page"

			params_hash2 = {}
			config_params = {}
			params_hash2["config_params"] = config_params

			template_params = {}
			params_hash2["template_params"] = template_params

			app_message_params = {}
			params_hash2["app_message_params"] = app_message_params

			static_page = {}
			static_page["display_title"] = "Privacy Policy"
			static_page["description"] = "<p>Saranyu TV reserves the right to immediately terminate, suspend, limit, or restrict 
			Your account or Your use of the Services or access to Content at any time, without notice or liability, if Saranyu TV 
			so determines in its sole discretion, for any reason whatsoever, including that You have breached these 
			Terms and Conditions, the Privacy Policy, violated any law, rule, or regulation, engaged in any inappropriate conduct, 
			provided false or inaccurate information, or for any other reason. You hereby agree and consent to the above and agree and 
			acknowledge that Saranyu TVcan, at its sole discretion, exercise its right in relation to any or all of the above, 
			and that Star, its directors, officers, employees, affiliates, agents, contractors, principals or licensors shall not be 
			liable in any manner for the same; and you hereby agree, acknowledge and consent to the same.User Reviews, Comments and 
			Other Material. You may have an opportunity to publish, transmit, submit, or otherwise post (collectively, 'Post') only 
			reviews or comments (collectively, 'User Material'). As it concerns User Material, without prejudice to your obligation to
			otherwise comply with applicable laws during the course of using the Services, you agree to hereby comply with any and all 
			applicable laws, as well as any other rules and restrictions that may be set forth herein or on the Site or Services.</p>
			<p>You agree that Saranyu TV shall have the right but have no obligation, to monitor User Material and to restrict or remove 
			User Material that Saranyu TVmay determine, in its sole discretion, is inappropriate or for any other reason. 
			You acknowledge that Saranyu TV reserves the right to investigate and take appropriate legal action against anyone who, 
			in Saranyu TV's sole discretion, violates these Terms, including, but not limited to, terminating their account and/or 
			reporting such User Material, conduct, or activity, to law enforcement authorities, in addition to any other available 
			remedies under law or equity.</p>"

			params_hash2["static_page"] = static_page

			metadata["params_hash2"] = params_hash2
			
		status = true
        error = ""
        maxtime = 60
        body = {}
        body["auth_token"] = bg.ott_auth_token
        body["content"] = metadata

        catalogs = Catalog.find_by(:bg_id => bg.id, :theme => "message", :tenant_id => tenant.id.to_s)

        if params[:method] == "update"
        else
		url = "http://"+get_ott_base_url+"/catalogs/"+catalogs.ott_id+"/items?auth_token="+bg.ott_auth_token  
        response = http_request("post",body,url,maxtime)
        if response["code"] == 200
        else
            status = response["status"]
        end
    	end

        return response
	end


	def create_user_params(bg,tenant)
		status = true
	       metadata = {}
			
			metadata["title"] = "User Params"
			metadata["status"] = "published"
			metadata["business_group_id"] = bg.ott_id

			if params[:regions]
				metadata["regions"] = params[:regions].split(",").map(&:strip)
			else
				metadata["regions"] = ["IN"]
			end

			metadata["tenant_ids"] = []
			tenantotid = Tenant.find_by(:id => tenant.id)
			metadata["tenant_ids"] << tenant.ott_id

			metadata["app_ids"] = []
			App.where(:tenant_id => tenant.id).each do |app_id|
				app = App.find(app_id)
				metadata["app_ids"] << app.ott_id
			end	

			metadata["platform_code"] = "1011"
			metadata["app_code"] = "1011"
			metadata["platform_note"] = "User Params"
			metadata["text_message"] = "User Params"
			metadata["email_template"] = ""
			metadata["type"] = "app_message"

			params_hash2 = {}
			config_params = {}
			params_hash2["config_params"] = config_params

			template_params = {}
			params_hash2["template_params"] = template_params

			app_message_params = {}
			app_message_params["message"] = "User params are missed"
			params_hash2["app_message_params"] = app_message_params

			static_page = {}
			params_hash2["static_page"] = static_page

			metadata["params_hash2"] = params_hash2
			
		status = true
        error = ""
        maxtime = 60
        body = {}
        body["auth_token"] = bg.ott_auth_token
        body["content"] = metadata

        catalogs = Catalog.find_by(:bg_id => bg.id, :theme => "message", :tenant_id => tenant.id.to_s)

        if params[:method] == "update"
        else
		url = "http://"+get_ott_base_url+"/catalogs/"+catalogs.ott_id+"/items?auth_token="+bg.ott_auth_token  
        response = http_request("post",body,url,maxtime)
        if response["code"] == 200
        else
            status = response["status"]
        end
    	end

        return response
	end


	def create_user_notexist(bg,tenant)
		status = true
	       metadata = {}
			
			metadata["title"] = "User doesn't exist"
			metadata["status"] = "published"
			metadata["business_group_id"] = bg.ott_id

			if params[:regions]
				metadata["regions"] = params[:regions].split(",").map(&:strip)
			else
				metadata["regions"] = ["IN"]
			end

			metadata["tenant_ids"] = []
			tenantotid = Tenant.find_by(:id => tenant.id)
			metadata["tenant_ids"] << tenant.ott_id

			metadata["app_ids"] = []
			App.where(:tenant_id => tenant.id).each do |app_id|
				app = App.find(app_id)
				metadata["app_ids"] << app.ott_id
			end	

			metadata["platform_code"] = "1013"
			metadata["app_code"] = "1013"
			metadata["platform_note"] = "User doesn't exist"
			metadata["text_message"] = "User doesn't exist"
			metadata["email_template"] = ""
			metadata["type"] = "app_message"

			params_hash2 = {}
			config_params = {}
			params_hash2["config_params"] = config_params

			template_params = {}
			params_hash2["template_params"] = template_params

			app_message_params = {}
			app_message_params["message"] = "User doesn't exist"
			params_hash2["app_message_params"] = app_message_params

			static_page = {}
			params_hash2["static_page"] = static_page

			metadata["params_hash2"] = params_hash2
			
		status = true
        error = ""
        maxtime = 60
        body = {}
        body["auth_token"] = bg.ott_auth_token
        body["content"] = metadata

        catalogs = Catalog.find_by(:bg_id => bg.id, :theme => "message", :tenant_id => tenant.id.to_s)

        if params[:method] == "update"
        else
		url = "http://"+get_ott_base_url+"/catalogs/"+catalogs.ott_id+"/items?auth_token="+bg.ott_auth_token  
        response = http_request("post",body,url,maxtime)
        if response["code"] == 200
        else
            status = response["status"]
        end
    	end

        return response
	end


	def create_sign_in(bg,tenant)
		status = true
	       metadata = {}
			
			metadata["title"] = "Sign In"
			metadata["status"] = "published"
			metadata["business_group_id"] = bg.ott_id

			if params[:regions]
				metadata["regions"] = params[:regions].split(",").map(&:strip)
			else
				metadata["regions"] = ["IN"]
			end

			metadata["tenant_ids"] = []
			tenantotid = Tenant.find_by(:id => tenant.id)
			metadata["tenant_ids"] << tenant.ott_id

			metadata["app_ids"] = []
			App.where(:tenant_id => tenant.id).each do |app_id|
				app = App.find(app_id)
				metadata["app_ids"] << app.ott_id
			end	

			metadata["platform_code"] = "1015"
			metadata["app_code"] = "1015"
			metadata["platform_note"] = "Thank you for sign in!"
			metadata["text_message"] = "Thank you for sign in!"
			metadata["email_template"] = ""
			metadata["type"] = "app_message"

			params_hash2 = {}
			config_params = {}
			params_hash2["config_params"] = config_params

			template_params = {}
			params_hash2["template_params"] = template_params

			app_message_params = {}
			app_message_params["message"] = "Thank you for sign in!"
			params_hash2["app_message_params"] = app_message_params

			static_page = {}
			params_hash2["static_page"] = static_page

			metadata["params_hash2"] = params_hash2
			
		status = true
        error = ""
        maxtime = 60
        body = {}
        body["auth_token"] = bg.ott_auth_token
        body["content"] = metadata

        catalogs = Catalog.find_by(:bg_id => bg.id, :theme => "message", :tenant_id => tenant.id.to_s)

        if params[:method] == "update"
        else
		url = "http://"+get_ott_base_url+"/catalogs/"+catalogs.ott_id+"/items?auth_token="+bg.ott_auth_token  
        response = http_request("post",body,url,maxtime)
        if response["code"] == 200
        else
            status = response["status"]
        end
    	end

        return response
	end


def create_sign_up(bg,tenant)
		status = true
	       metadata = {}
			
			metadata["title"] = "Sign Up"
			metadata["status"] = "published"
			metadata["business_group_id"] = bg.ott_id

			if params[:regions]
				metadata["regions"] = params[:regions].split(",").map(&:strip)
			else
				metadata["regions"] = ["IN"]
			end

			metadata["tenant_ids"] = []
			tenantotid = Tenant.find_by(:id => tenant.id)
			metadata["tenant_ids"] << tenant.ott_id

			metadata["app_ids"] = []
			App.where(:tenant_id => tenant.id).each do |app_id|
				app = App.find(app_id)
				metadata["app_ids"] << app.ott_id
			end	

			metadata["platform_code"] = "1018"
			metadata["app_code"] = "1018"
			metadata["platform_note"] = "Thank you for Signing Up!"
			metadata["text_message"] = "Thank you for Signing Up!"
			metadata["email_template"] = ""
			metadata["type"] = "app_message"

			params_hash2 = {}
			config_params = {}
			params_hash2["config_params"] = config_params

			template_params = {}
			params_hash2["template_params"] = template_params

			app_message_params = {}
			app_message_params["message"] = "Thank you for Signing Up!"
			params_hash2["app_message_params"] = app_message_params

			static_page = {}
			params_hash2["static_page"] = static_page

			metadata["params_hash2"] = params_hash2
			
		status = true
        error = ""
        maxtime = 60
        body = {}
        body["auth_token"] = bg.ott_auth_token
        body["content"] = metadata

        catalogs = Catalog.find_by(:bg_id => bg.id, :theme => "message", :tenant_id => tenant.id.to_s)

        if params[:method] == "update"
        else
		url = "http://"+get_ott_base_url+"/catalogs/"+catalogs.ott_id+"/items?auth_token="+bg.ott_auth_token  
        response = http_request("post",body,url,maxtime)
        if response["code"] == 200
        else
            status = response["status"]
        end
    	end

        return response
	end

	def update_at_ott(params)
		cms_config = params[:cms_config]
		tenant = params[:tenant]
		bg = params[:bg]
	    metadata = {}
			
		metadata["title"] = cms_config["title"]
		metadata["status"] = "published"
		metadata["business_group_id"] = bg.ott_id

		metadata["regions"] = cms_config["regions"]

		metadata["tenant_ids"] = [tenant["ott_id"]]

		metadata["app_ids"] = []
		App.where(:tenant_id => tenant["_id"]).each do |app_id|
			app = App.find(app_id)
			metadata["app_ids"] << app.ott_id
		end

		metadata["platform_code"] = "5000"
		metadata["app_code"] = "5000"
		metadata["platform_note"] = "Registration Details"
		metadata["text_message"] = "Thanks for registration!"
		metadata["type"] = "config_params"

		params_hash2 = {}
		config_params = {}

		input_session = cms_config["params_hash2"]["config_params"]["session_params"]
		session_params = {}
		session_params["max_number_of_sessions_per_user"] = input_session["max_number_of_sessions_per_user"]
		session_params["max_number_of_devices"] = input_session["max_number_of_devices"]
		session_params["device_lock"] = input_session["device_lock"]
		session_params["delete_older_session"] = input_session["delete_older_session"]
		session_params["delete_device"] = input_session["delete_device"]
		session_params["override_from_user_plans"] = input_session["override_from_user_plans"]
		session_params["old_flow"] = input_session["old_flow"]

		input_reg = cms_config["params_hash2"]["config_params"]["registration_params"]
		registration_params = {}
		registration_params["verification_flag"] = input_reg["verification_flag"]
		registration_params["send_welcome_email_for_internal_reg"] = input_reg["send_welcome_email_for_internal_reg"]
		registration_params["send_welcome_email_for_external_login"] = input_reg["send_welcome_email_for_external_login"]

		input_email_config = cms_config["params_hash2"]["config_params"]["email_config_params"]
		email_config_params = {}
		email_config_params["portal_base_url"] = input_email_config["portal_base_url"]
		email_config_params["sender_email"] = input_email_config["sender_email"]


		config_params["session_params"] = session_params
		config_params["registration_params"] = registration_params
		config_params["email_config_params"] = email_config_params
		params_hash2["config_params"] = config_params

		template_params = {}
		params_hash2["template_params"] = template_params

		app_message_params = {}
		params_hash2["app_message_params"] = app_message_params

		static_page = {}
		params_hash2["static_page"] = static_page

		cms_params = {}
		cms_params["logo"] = params["logo"]
		cms_params["template"] = params["template"]
		cms_params["color"] = params["color"]
		cms_params["text"] = params["text"]
		params_hash2["cms_params"] = cms_params

		metadata["params_hash2"] = params_hash2
		
        error = ""
        maxtime = 60
        body = {}
        body["auth_token"] = bg.ott_auth_token
        body["content"] = metadata

        url = "http://"+get_ott_base_url+"/catalogs/"+cms_config["catalog_id"]+ "/items/"+ cms_config["content_id"] +"?auth_token="+bg.ott_auth_token  
        response = http_request("put",body,url,maxtime)
        if response["code"] == 200
        else
            status = response["status"]
            error = "Can not update Customization!"
        end
        return status,error
	end	


end #end of Message helper module
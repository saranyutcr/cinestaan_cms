
module CouponCodeHelper

	def create_catalog_coupon(bg,tenant)
		status = true
		error = ""
		response = nil
		if status
			body = {}
			body["auth_token"] = bg.ott_auth_token
			body["catalog_data"] = {}
			body["catalog_data"]["name"] = "coupon"
			body["catalog_data"]["theme"] = "coupon"
			body["catalog_data"]["title"] = "coupon"
			body["catalog_data"]["apps"] = []
			tenant_ids = []
			App.where(:tenant_id => tenant.id).each do |app_id|
				app = App.find(app_id)
				body["catalog_data"]["apps"] << app.ott_id
				tenant_ids << app.tenant_id
			end
			tenant_ids = tenant_ids.uniq

			if params[:edit] == "update"
				# catalog = Catalog.find_by(:bg_id => bg.id, :theme => "message", :tenant_id => tenant.id.to_s)
				# url = "http://"+get_ott_base_url+"/catalogs/"+catalog.ott_id
				# response = http_request("put",body,url,60)
			else
				url = "http://"+get_ott_base_url+"/catalogs"
		    	response = http_request("post",body,url,60)
				catalog = Catalog.new
			end
			
			if !response["status"]
				status = false
				error = response["error"]
			end
		end
		if status
			catalog.name = "coupon"
			catalog.theme = "coupon"
			catalog.app_id = []
			catalog.tenant_id = []
			App.where(:tenant_id => tenant.id).each do |app_id|
				app = App.find(app_id)
				catalog.app_id << app.id
				catalog.tenant_id << app.tenant_id
			end
			catalog.tenant_id = catalog.tenant_id.uniq
			catalog.bg_id = bg.id
			catalog.ott_id = response["body"]["data"]["catalog_id"]
			catalog.save
		end
		return catalog

	end

	def coupon_code(bg,params)
		status = true
	       metadata = {}
			
			metadata["status"] = "published"
			metadata["app_ids"] = []
			appid = App.where(:tenant_id => params[:tenant_id])
			appid.each do |a|
			  metadata["app_ids"] << a.ott_id	
			end
			metadata["business_group_id"] = bg.ott_id
			metadata["tenants"] = []
				tenantotid = Tenant.find_by(:id => params[:tenant_id])
				metadata["tenants"] << tenantotid.ott_id
				
				metadata["title"] = params[:coupon_code].strip
				metadata["coupon_code"] = params[:coupon_code].strip
				metadata["coupon_type"] = params[:coupon_type].strip
				metadata["coupon_amount"] = params[:coupon_amt].strip
				metadata["s_date"] = params[:start_date]
				metadata["e_date"] = params[:end_date]
			
			if params[:regions]
				metadata["regions"] = params[:regions].split(",").map(&:strip)
			else
				metadata["regions"] = ["IN"]
			end

		status = true
        error = ""
        maxtime = 60
        body = {}
        body["auth_token"] = bg.ott_auth_token
        body["content"] = metadata

        catalogs = Catalog.find_by(:bg_id => bg.id, :theme => "coupon", :tenant_id => params[:tenant_id])

        if params[:method] == "update"
        else
		url = "http://"+get_ott_base_url+"/catalogs/"+catalogs.ott_id+"/items?auth_token="+bg.ott_auth_token  
        response = http_request("post",body,url,maxtime)
        if response["code"] == 200
        else
            status = response["status"]
            #error = response["body"]["error"]["message"]
        end
    	end

        return response
		
	end



end
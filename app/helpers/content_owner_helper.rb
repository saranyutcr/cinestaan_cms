module ContentOwnerHelper
	def get_media_parent_type mid
		m = Media.find(mid)
		c = Catalog.where(:ott_id => m.ott_catalog_id).first
		cd = c.blank? ? " " : c.theme 
		return cd
	end
	def convert_datetime_co datetime
		date = DateTime.parse(datetime.to_s)
  		formatted_date = date.strftime('%d-%b-%Y')
  		return formatted_date
	end
	# def get_total_duration media_array
	# 	tot_val = 0
	# 	media_array_val = media_array.map(&:duration_string)
	# 	media_array_val.each do |i|
	# 		unless i.blank?
	# 		t = Time.parse(i)
	# 		s = t.hour * 60 * 60 + t.min * 60 + t.sec
	# 		tot_val = tot_val + s
	# 		end
	# 	end
	# 	tot_hrs = [tot_val / 3600, tot_val / 60 % 60, tot_val % 60].map { |t| t.to_s.rjust(2,'0')}
	# 	return tot_hrs
	# end
	def seasons_count arr
		c = []
		@all_subcata = arr.group_by(&:subcategory_id)
		@all_subcata.each do |k,v|
			if k == blank?

			else
				c << k
			end
		end
		@subcata_cnt = 0
		if c.include? nil
			@subcata_cnt = 0
		else
			@subcata_cnt = c.count
		end
		return @subcata_cnt
	end
	def individual_seasons_count arr
		@all_subcata = arr.group_by(&:subcategory_id)
		return @all_subcata
	end
	# Methods for new code
	def total_media_count media_details
		@media_count = 0
	    media_details.each do|i|
		    	@media_count = @media_count + 1
	    end
	    return @media_count
	end

	def get_total_duration media_details
		@duration_in_hrs = 0
		duration_count = 0
	    media_details.each do|i|
		   duration_count = duration_count + i["duration_string"].to_i
		end
	    @duration_in_hrs = duration_count / 60
	    return @duration_in_hrs
	end

	def get_duration_in_hrsminsec duration
		duration_in_secs =  duration.to_i * 60
		@duration_time_format = Time.at(duration_in_secs).utc.strftime("%H:%M:%S")
		return @duration_time_format
	end

	def total_revenue total_revenue,advance_paid
		total_val = total_revenue - advance_paid
		if total_val < 0
			total_val = 0.0
		else
			total_val
		end
		return total_val
	end
	

end

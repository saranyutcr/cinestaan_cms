module PaginateMe
  #index 0 included in the list
  @@per_page = 20
  #psize is an optional parameter that the user can set
  def paginate_me items, page, psize, scount
    psize = psize.to_i
    case psize
    when nil
      psize = 20
    when 0
      psize = 20
    else
      if psize > 500
        psize = 500
      end
    end
    scount = scount.to_i
    case scount
    when nil
      scount = 0
    end
    #logger.debug "page size is now set to #{psize}"
    page = page.to_i
    case page
    when nil
      page = 0 
    end
    #page.to_i shall take care of the incorrect page params
    start_count = (page.to_i * psize) + scount
    #logger.debug "start count is #{start_count}"
    end_count = (((page.to_i + 1) * psize) + scount) - 1
    page_items = items[start_count..end_count]
    if page_items
      return page_items
    else
      #handles out of range queries
      return []
    end
  end
end

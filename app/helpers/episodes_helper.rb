module EpisodesHelper
	
	def create_episode_for_show(params)
		obj = ""
		status = true
		url = "http://"+get_ott_base_url+\
						"/catalogs/shows/"+params[:parentobj][:ott_id]+\
						"/episodes"
		if params[:episode] == "new_episode"
			body = extract_metadata(params)
			response = http_request("post",body,url,60)
			if response["code"] == 200
				params[:ott_id] = response["body"]["data"]["item_id"]
				create_media(params) 
			end
		else
			params[:episode] = eval(params[:episode])
			url = url + "/" + params[:episode][:content_id]
			body = extract_metadata_for_update(params)
			response = http_request("put",body,url,60)      
		end	

		if response["code"] != 200
			obj = response["body"]["error"]["message"]	
			status = false
	    else
	        obj = response["body"]["data"]		
		end	
		return status,obj
	end
	
	def create_episode_for_subcategory(params)
		obj = ""
		status = true
		url = "http://"+get_ott_base_url+\
						"/catalogs/shows/subcategories/"+params[:parentobj][:ott_id]+\
						"/episodes"
		if params[:episode] == "new_episode"
			body = extract_metadata(params)
			response = http_request("post",body,url,60)
			if response["code"] == 200
				params[:ott_id] = response["body"]["data"]["item_id"]
				create_media(params) 
			end
		else
			params[:episode] = eval(params[:episode])
			url = url + "/" + params[:episode][:content_id]
			body = extract_metadata_for_update(params)
			response = http_request("put",body,url,60)
		end

		if response["code"] != 200
			obj = response["body"]["error"]["message"]	
			status = false
		else
			obj = response["body"]["data"]	
		end	
		return status,obj
	end	

	def extract_metadata(params)
		body = {}
		body["auth_token"] = params["bg_obj"]["ott_auth_token"]
		body["content"] = {}
		body["content"]["title"] = params["title"]
		body["content"]["status"] = params["status"]
		body["content"]["l_listing_image"] = params["l_listing_image"]
		body["content"]["p_listing_image"] = params["p_listing_image"]
		body["content"]["featured_image"] = params["featured_image"]
		body["content"]["business_group_id"] = params["bg_obj"]["ott_id"]
		body["content"]["regions"] = ["IN"]
		body["content"]["description"] = params["description"]
		body["content"]["rating"] = params["rating"]
		body["content"]["asset_id"] = params["asset_id"] if params["asset_id"]
	    body["content"]["sequence_no"] = params["sequence_no"] if params["sequence_no"]
		# body["content"]["smart_url"] = params["smart_url"]
		tenants = []
		params["parentobj"]["tenant_ids"].each do |t|
			tenants << Tenant.find(t).ott_id
		end	
		body["content"]["tenant_ids"] = tenants
		apps = []
		params["parentobj"]["app_ids"].each do |a|
			apps << App.find(a).ott_id
		end
		body["content"]["app_ids"] = apps 
		body["content"]["play_url_type"] = params[:default_url_type]
		body["content"]["play_url"] = build_play_url_hash(params)
		return body
	end	

	def extract_metadata_for_update(params)
		body = {}
		body["auth_token"] = params["bg_obj"]["ott_auth_token"]
		body["content"] = {}
		body["content"]["title"] = params["episode"]["title"]
		body["content"]["status"] = params["episode"]["status"]
		body["content"]["app_ids"] = params["episode"]["app_ids"]
		body["content"]["tenant_ids"] = params["episode"]["tenant_ids"]
		body["content"]["business_group_id"] = params["bg_obj"]["ott_id"]
		body["content"]["regions"] = params["episode"]["regions"]
		body["content"]["genres"] = params["episode"]["genres"]
		body["content"]["language"] = params["episode"]["language"]
		body["content"]["asset_id"] = params["episode"]["asset_id"] #if params["episode"]["asset_id"]
	    body["content"]["sequence_no"] = params["episode"]["sequence_no"] #if params["episode"]["sequence_no"]
		return body
	end	

	def create_media(media_params)
		media = Media.new
		media.title = media_params["title"]
		media.notification_url = media_params["notification_url"]
		media.user_id = media_params["user_id"]
		media.user_role = media_params["user_role"]
		media.publish_settings_id = media_params["publish_settings_id"]
		media.bg_id = media_params["bg_obj"]["_id"]
		media.priority = media_params["priority"]
		media.status = "Created Externally"
		media.ott_status = media_params["status"]
		media.is_removed = media_params["is_removed"]
		media.bulk_upload_id = media_params["bulk_upload_id"]
		media.custom_keys = media_params["custom_keys"]
		media.cut_intervals = media_params["cut_intervals"]
		media.banner = media_params["banner"]
		# media.smart_url = media_params["smart_url"]
		media.ott_id = media_params["ott_id"]
		media.ott_catalog_id = media_params["parentobj"]["ott_catalog_id"]
		media.is_removed = false
		if media.save
			return media
		else
			return nil
		end
	end

	def build_play_url_hash(params)
		play_url = {}
		count = params[:url_types].count
		for i in 0..count-1
			key = params[:url_types][i]
			h = {}
			h["url"] = params[:url_names][i]
			h["width"] = !params[:widths][i].empty? ? params[:widths][i].to_f : -1.0
			h["height"] = !params[:heights][i].empty? ? params[:heights][i].to_f : -1.0
			play_url[key] = h
		end	
		return play_url
	end

end

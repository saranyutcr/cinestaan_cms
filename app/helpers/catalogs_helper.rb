module CatalogsHelper
	def post_catalog_to_ott(bg,params)
		status = true
		error = {}

        if !params[:apps]
        	response = {}
        	response["status"] = false
        	response["error"] = "Please provide applications!"
        	return response
        end

		if status
			response = nil
			body = {}
			body["auth_token"] = bg.ott_auth_token
			body["catalog_data"] = {}
			body["catalog_data"]["name"] = params[:name]
			body["catalog_data"]["theme"] = params[:theme]
			body["catalog_data"]["meta_title"] = params[:meta_title] if params[:meta_title]
			body["catalog_data"]["meta_description"] = params[:meta_description] if params[:meta_description]
			body["catalog_data"]["meta_keywords"] = params[:meta_keywords] if params[:meta_keywords]
			body["catalog_data"]["sequence_no"] = params[:sequence_no] if params[:sequence_no]
			# new fields for content owner module starts
			body["catalog_data"]["censor_rating"] = params[:censor_rating] if params[:censor_rating]
			body["catalog_data"]["currency"] = params[:currency] if params[:currency]
			body["catalog_data"]["cost_per_hour"] = params[:cost_per_hour] if params[:cost_per_hour]
			body["catalog_data"]["content_owner_id"] = params[:content_owner_id] if params[:content_owner_id]
			body["catalog_data"]["layout_type"] = params[:layout_type] if params[:layout_type]
			# new fields for content owner module ends
			catalog_image = build_catalog_image(params)
			body["catalog_data"]["catalog_image"] = catalog_image

			item_images = build_item_images(params)
			body["catalog_data"]["item_images"] = item_images
			
			body["catalog_data"]["apps"] = []
			
            tenant_ids = []
			params[:apps].each do |app_id|
				app = App.find(app_id)
				body["catalog_data"]["apps"] << app.ott_id
				tenant_ids << app.tenant_id
			end
			
			tenant_ids = tenant_ids.uniq
			get_genres = build_genre_hash(tenant_ids,params)
			get_languages = build_language_hash(tenant_ids,params)
      		get_episodetypes = build_episode_type_hash(params) 
      		if !get_genres.empty?
				body["catalog_data"]["genres"] = get_genres 
			else
				response = {}	
				response["status"] = false
	        	response["error"] = "Please Select Genres!"
	        	return response
			end	

			if !get_languages.empty?
				body["catalog_data"]["languages"] = get_languages 
			else
				response = {}	
				response["status"] = false
	        	response["error"] = "Please Select Languages!"
	        	return response
			end
			body["catalog_data"]["episodetype_tags"] = get_episodetypes
			if params["custom_tag"] == "on" && params["tag_names"]
				get_tags = build_tag_array(params)
				if !get_tags.empty?
					body["catalog_data"]["custom_tags"] = get_tags
				else
					response = {}	
					response["status"] = false
					response["error"] = "Please Provide Tag Names!"
					return response	
				end	
			end	

			if params["videolist_tag"] == "on" && params["videolist_names"]
				get_videotags = build_videolist_array(params)
				if !get_videotags.empty?
					body["catalog_data"]["videolist_tags"] = get_videotags
				else
					response = {}	
					response["status"] = false
					response["error"] = "Please Provide Videolist Names!"
					return response	
				end	
			end	

			if params["action"] == "update"
				c = Catalog.find(params["catalog_id"])
				url = "http://"+get_ott_base_url+"/catalogs/"+c.ott_id
				resp = http_request("put",body,url,60)
			else
				url = "http://"+get_ott_base_url+"/catalogs"
				resp = http_request("post",body,url,60)
				c = Catalog.new
			end
			status = resp["status"]
		end
		response = {}
		if status
			c.name = params[:name] 
			c.app_id = []
			c.tenant_id = []
			params[:apps].each do |app_id|
				c.app_id << app_id
				app = App.find(app_id)
				c.tenant_id << app.tenant_id
			end
			c.tenant_id = c.tenant_id.uniq
			c.theme = params[:theme]
			c.bg_id = bg.id
			c.ott_id = resp["body"]["data"]["catalog_id"]
			c.layout_type = params[:layout_type]
			
			c.save
			response["error"] = ""
		else
				
			response["error"] = resp["body"]["error"]["message"]
		end
		
		response["status"] = status
		return response
	end

	def get_tenant(id)
			Tenant.find(id)
	end	



	def build_tag_array(params)
		status = validate_tags(params[:tag_names])
		tags = []
		if status
			count = params[:tag_names].count
			for i in 0..count-1
				h = {}
				h["tag"] = params[:tag_names][i]
				h["type"] = params[:tag_types][i]
				h["mandatory"] = params[:mandatory][i]
				tags << h
			end	
		end
		return tags	
	end

	def build_videolist_array(params)
		status = validate_videotags(params[:videolist_names])
		videotags = []
		if status
			 params[:videolist_names].each do |v|
	         videotags << v
			  end

		end
		return videotags	
	end


	def get_boolean(flag)
	  return true if flag == "yes"
	  return false
	end	

	def validate_tags(tags)
		tags.each do |tag|
			if tag.empty?
				return false
			end	
		end	
		return true
	end	

	def validate_videotags(videotags)
		videotags.each do |videotag|
			if videotag.empty?
				return false
			end	
		end	
		return true
	end	

	def build_genre_hash(tenant_ids,params)
      genres = {}

      tenant_ids.each do |tenant_id|
        if params["#{tenant_id}-genres"]
		    tenant = get_tenant(tenant_id) 
		    if !(genres.has_key? tenant)
		      genre = []
		      params["#{tenant_id}-genres"].each do |g|
		        h = { "name" => g , "description" => "" }
		        genre << h
		      end
		      genres.merge!({ tenant.ott_id => genre })                    
		    end
	    end
	  end
      return genres
	end	

	def build_language_hash(tenant_ids,params)
	  languages = {}
	  
	  tenant_ids.each do |tenant_id|
        if params["#{tenant_id}-languages"]
		    tenant = get_tenant(tenant_id) 
		    if !(languages.has_key? tenant)
		      language = []
		      params["#{tenant_id}-languages"].each do |l|
		        h = { "name" => l , "description" => "" }
		        language << h
		      end   
		      languages.merge!({ tenant.ott_id => language })                
		    end
	    end
	  end
      return languages
	end
	def build_episode_type_hash(params)
	  	episodetype_tag = []
		if params["episodetype_tags"]
		    params["episodetype_tags"].each do |ett|
		      	et_tags = ett.split('-')
		      	name = et_tags[0]
		      	display_title = et_tags[1]
		        h = { "name" => name , "display_title" => display_title }
		        episodetype_tag << h
		    end   
	    end
	    return episodetype_tag
	end
	def build_catalog_image(params)
		if params["catalog_p_listing_image"].blank?
		   image = "https://s3-ap-southeast-1.amazonaws.com/ott-as-service/ott_default_images/default.png"
		else 
		    image =  params["catalog_p_listing_image"]   
		end	
		
		return image
	end	

	def build_item_images(params)
		h = {}
		default_hash = default_images(params["theme"])
		if params[:item_featured_image].blank?
			h["l_large"] = default_hash["l_large"]
		else 
		    h["l_large"] = params[:item_featured_image]   	
		end
		
		if params[:item_l_listing_image].blank?
			h["l_medium"] = default_hash["l_medium"]
		else
		    h["l_medium"] = params[:item_l_listing_image]	
		end

		if params[:item_p_listing_image].blank?
			h["l_small"] = default_hash["l_small"]
		else
		    h["l_small"] = params[:item_p_listing_image]	
		end	
		return h
	end

	def default_images(theme)
		h = {}
		case theme
		when "video"
			h["l_large"] = "https://s3-ap-southeast-1.amazonaws.com/ott-as-service/ott_default_images/item_images/default_video_large.jpg"
			h["l_medium"] = "https://s3-ap-southeast-1.amazonaws.com/ott-as-service/ott_default_images/item_images/default_video_medium.jpg"
			h["l_small"] = "https://s3-ap-southeast-1.amazonaws.com/ott-as-service/ott_default_images/item_images/default_video_small.jpg"
		when "movie"
			h["l_large"] = "https://s3-ap-southeast-1.amazonaws.com/ott-as-service/ott_default_images/item_images/default_movies_large.jpg"
			h["l_medium"] = "https://s3-ap-southeast-1.amazonaws.com/ott-as-service/ott_default_images/item_images/default_movies_medium.jpg"
			h["l_small"] = "https://s3-ap-southeast-1.amazonaws.com/ott-as-service/ott_default_images/item_images/default_movies_small.jpg"
		when "show"
			h["l_large"] = "https://s3-ap-southeast-1.amazonaws.com/ott-as-service/ott_default_images/item_images/default_TV_show_large.jpg"
			h["l_medium"] = "https://s3-ap-southeast-1.amazonaws.com/ott-as-service/ott_default_images/item_images/default_TV_show_medium.jpg"
			h["l_small"] = "https://s3-ap-southeast-1.amazonaws.com/ott-as-service/ott_default_images/item_images/default_TV_show_small.jpg"
		when "people"
		    h["l_large"] = "https://s3-ap-southeast-1.amazonaws.com/ott-as-service/ott_default_images/item_images/default_people_large.jpg"
			h["l_medium"] = "https://s3-ap-southeast-1.amazonaws.com/ott-as-service/ott_default_images/item_images/default_people_medium.jpg"
			h["l_small"] = "https://s3-ap-southeast-1.amazonaws.com/ott-as-service/ott_default_images/item_images/default_people_small.jpg"	
		when "livetv"
			h["l_large"] = "https://s3-ap-southeast-1.amazonaws.com/ott-as-service/ott_default_images/item_images/default_Live_TV_large.jpg"
			h["l_medium"] = "https://s3-ap-southeast-1.amazonaws.com/ott-as-service/ott_default_images/item_images/default_Live_TV_medium.jpg"
			h["l_small"] = "https://s3-ap-southeast-1.amazonaws.com/ott-as-service/ott_default_images/item_images/default_Live_TV_small.jpg"
		end	
		return h
	end	
		
end


$('#bgpass').on('keyup', function checkStrength(password) {

password = $('#bgpass').val()
var score = 0

if (password.length >= 6 && password.length <= 15 && password.match(/^(?=.*[a-z])(?=.*[0-9])[A-Za-z0-9\d=!\-@._*#$+&^]+$/)) {
// If password length is greater than seven, increase score value.
if (password.length > 7) score += 1
// If password is alphanumeric, increase score value.
if (password.match(/[a-z0-9]/)) score += 1
// If password has upper case letters, increase score value
if (password.match(/[A-Z]/)) score += 1
// If it has atleast one special character, increase score value.
if (password.match(/([!,%,&,@,#,$,^,*,?,_,~])/)) score += 1
}

// Calculated score value, we can return messages
if (score == 2) {
console.log(score);
$('#result').removeClass();
$('#result').addClass('weak');
$('#result').text('Weak');
} 
else if (score == 3) {
console.log(score);
$('#result').removeClass();
$('#result').addClass('good');
$('#result').text('Normal');
} 
else if (score > 3){
console.log(score);
$('#result').removeClass();
$('#result').addClass('strong');
$('#result').text('Strong');
}
else if (score == 0){
$('#result').removeClass();
$('#result').addClass("");
$('#result').text("");
}
});
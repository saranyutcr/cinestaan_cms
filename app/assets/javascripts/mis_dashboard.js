// $(function () {
//   Highcharts.chart('hchart1', {
//     title: {
//       text: 'Monthly Average Temperature',
//       x: -20 //center
//     },
//     subtitle: {
//       text: 'Bangalore',
//       x: -20
//     },
//     xAxis: {
//       categories: ['Mar 1', 'Mar 2', 'Mar 3', 'Mar 4', 'Mar 5', 'Mar 6', 'Mar 7', 'Mar 8', 'Mar 9', 'Mar 10',
//           				'Mar 11', 'Mar 12', 'Mar 13', 'Mar 14', 'Mar 15', 'Mar 16', 'Mar 17', 'Mar 18', 'Mar 19', 'Mar 20',
//   								'Mar 21', 'Mar 22', 'Mar 23', 'Mar 24', 'Mar 25', 'Mar 26', 'Mar 27', 'Mar 28', 'Mar 29', 'Mar 30']
// 		},        								
//     yAxis: {
//       title: {
//         text: 'No of Users'
//       },
//       plotLines: [{
//         value: 0,
//         width: 1,
//         color: '#808080'
//       }]
//     },
//     credits: {
// 	    enabled: false
// 	  },
//     tooltip: {
//       valueSuffix: '°C'
//     },
//     legend: {
//       layout: 'vertical',
//       align: 'right',
//       verticalAlign: 'middle',
//       borderWidth: 0
//     },
//     series: [{
//       name: 'Bangalore',
//       color: '#92D050',
//       data: [10, 9, 10, 12, 13, 14, 15, 16, 14, 13, 15, 16, 17, 16, 15, 14, 16, 14, 14, 13, 
//       			13, 14, 14, 15, 17, 16, 18, 17, 18, 18]
//     }]
//   });

//   Highcharts.chart('hchart2', {
//     title: {
//       text: 'Monthly Average Temperature',
//       x: -20 //center
//     },
//     subtitle: {
//       text: 'Bangalore',
//       x: -20
//     },
//     xAxis: {
//       categories: ['Mar 1', 'Mar 2', 'Mar 3', 'Mar 4', 'Mar 5', 'Mar 6', 'Mar 7', 'Mar 8', 'Mar 9', 'Mar 10',
//                   'Mar 11', 'Mar 12', 'Mar 13', 'Mar 14', 'Mar 15', 'Mar 16', 'Mar 17', 'Mar 18', 'Mar 19', 'Mar 20',
//                   'Mar 21', 'Mar 22', 'Mar 23', 'Mar 24', 'Mar 25', 'Mar 26', 'Mar 27', 'Mar 28', 'Mar 29', 'Mar 30']
//     },                        
//     yAxis: {
//       title: {
//         text: 'No of Users'
//       },
//       plotLines: [{
//         value: 0,
//         width: 1,
//         color: '#808080'
//       }]
//     },
//     credits: {
//       enabled: false
//     },
//     tooltip: {
//       valueSuffix: '°C'
//     },
//     legend: {
//       layout: 'vertical',
//       align: 'right',
//       verticalAlign: 'middle',
//       borderWidth: 0
//     },
//     series: [{
//       name: 'Bangalore',
//       color: '#FF0000',
//       data: [10, 9, 10, 12, 13, 14, 15, 16, 14, 13, 15, 16, 17, 16, 15, 14, 16, 14, 14, 13, 
//             13, 14, 14, 15, 17, 16, 18, 17, 18, 18]
//     }]
//   });

  

//   if($(".menu-section").height() < $(".right-content").height()) {
//     $(".menu-section").css({'height':($(".right-content").height()+'px')});
//   }
//   if($(".menu-section").height() > $(".right-content").height()) {
//     $(".right-content").css({'height':($(".menu-section").height()+'px')});
//   }
// });
Cms::Application.routes.draw do
  
    root :to => "home#index"

    get "contact" => "static#contact"
    get "aboutus" => "static#aboutus"

    #dashboard
    get "dashboard" => "home#dashboard"
    get "dashboard/:tab" => "home#dashboard"
    #users
    post "users/set_bg" => "sessions#set_bg"
    post "users/unset_bg" => "sessions#unset_bg"
    post "users" => "users#create"
    get  "user_verify_email/:id" => "users#user_email_verify"
    get "users/view_all" => "users#view_all"
    get "users/login_form" => "sessions#login_form"
    get "users/signup_form" => "sessions#signup_form"
    get "users/forgot_form" => "sessions#forgot_form"
    get "users/new" => "users#new"
    post "users/login" => "sessions#create"
    get "users/logout" => "sessions#destroy" #should be converted to post
    get "users/trial/new" => "users#new_trial"
    get "users/trial/check_email" => "users#check_email_trial"
    get "users/new_password" => "users#new_password"
    post "users/change_password" => "users#change_password"
    get "users/forgot_password" => "users#forgot_password"
  post "users/update_password" => "users#update_password"
  post "users/trial/create" => "users#create_trial"
  post "users/:user_id" => "users#delete"
  get "users" => "users#fetch_all"
  get "users/check_email" => "users#check_email"
  get "users/edit" => "users#edit"
  post '/user/update' => "users#update"
 
  
 #videolist upload and update
  post "media/bulk_videolist_upload" => "media#bulk_videolist_upload"
  post "media/bulk_videolist_update" =>"media#bulk_videolist_update"

  get "media/videolist_upload" => "media#videolist_upload"
  get "media/videolist_update" => "media#videolist_update"

    #Bridge routes
    get "media/bridge_upload" => "media#bridge_upload"
    get "media/bridge_update" => "media#bridge_update"

    post "media/bridge_data_upload" => "media#bridge_data_upload"
    post "media/bridge_data_update" => "media#bridge_data_update"

    post "media/bulk_upload" => "media#bulk_upload"
    post "media/bulk_data_update" => "media#bulk_data_update"

    #media
    post "media" => "media#create"
    get "media/view_all" => "media#view_all"
    get "media/new" => "media#new"
    get "media/edit" => "media#edit"
    get "media/bulk_upload" => "media#new_bulk_upload"
    get "media/bulk_update" => "media#bulk_update"

    get "media/:media_id" => "media#fetch"
    get "media/play/:media_id" => "media#play"
    get "media/view_media/:media_id" => "media#view_media"
    post "media/titlecheck" => "media#check_media_title"
    post "media/:media_id" => "media#update" #delete, update_priority, transcode, push_to_ott, remove_from_ott, edit
    get "media" => "media#fetch_all"

    #people
    post "people/bulk_upload" => "people#bulk_upload"
    get "people/new_bulk_upload" => "people#new_bulk_upload"
    get "people/bulk_update" => "people#bulk_update"


    #profile
    get "profiles/new" => "profile#new"
    post "profiles" => "profile#create"
    get "profiles/view_all" => "profile#view_all"
    get "profiles/:profile_id" => "profile#fetch"
    post "profiles/:profile_id" => "profile#delete"
    get "profiles" => "profile#fetch_all"
    #publishsettings
    get "publish_settings/new" => "publish_settings#new"
    get "publish_settings/view_all" => "publish_settings#view_all"
    post "publish_settings" => "publish_settings#create"
    post "publish_settings/:ps_id" => "publish_settings#delete"
    get "publish_settings" => "publish_settings#fetch_all"
    #businessgroups
    get "business_groups/new" => "business_groups#new"
    get "business_groups" => "business_groups#view_all"
    get "business_groups/all" => "business_groups#fetch_all"
    get "business_groups/check_brand_name" => "business_groups#check_brand_name"
    get "business_groups/:bg_id" => "business_groups#fetch"
    post "business_groups" => "business_groups#create"
    post "business_groups/register" => "business_groups#register_bg"
    # get  "verify_email/:id" => "business_groups#verifing_bg"
    get  "verify_email/:id" => "business_groups#update_bg"
    get  "reset_password/:id" => "business_groups#verify_forgot_password_link"
    post  "verify_forgot_password" => "business_groups#verify_forgot_password"
    #put "business_groups/verify" => "business_groups#update_bg"
    post "business_groups/forgot" => "business_groups#forgot_password"
    post "business_groups/:bg_id" => "business_groups#update"
    get "business_groups/:bg_id/report" => "business_groups#report"
    get "business_groups/:bg_id/platform_details" => "business_groups#fetch_platform_details"
    get "business_groups/:bg_id/instance/:instance_id" => "business_groups#fetch_instance_details"

    #ott
    get "business_groups/:bg_id/ott_videos/fetch_all" => "ott_videos#fetch_all"
    post "business_groups/:bg_id/ott_videos/:content_id" => "ott_videos#delete"
    post "business_groups/:bg_id/ott_videos" => "ott_videos#create"
    #tenants
    get "tenants/new" => "tenants#new"
    get "tenants/view_all" => "tenants#view_all"
    get "tenants/view_tenant/:tenant_id" => "tenants#view_tenant"
    post "tenants" => "tenants#create"
    get "tenants/:tenant_id" => "tenants#fetch"
    post "tenants/:tenant_id" => "tenants#update"
    get "tenants" => "tenants#fetch_all"
    #apps
    get "apps/new" => "apps#new"
    get "apps/view_all" => "apps#view_all"
    post "apps" => "apps#create"
    get "apps/:app_id" => "apps#fetch"
    post "apps/:app_id" => "apps#update"
    get "apps" => "apps#fetch_all"
    #catalogs
    get "catalogs/new" => "catalogs#new"
    get "catalogs/edit" => "catalogs#edit"
    get "catalogs/get_info" => "catalogs#edit"
    get "catalogs/view_all" => "catalogs#view_all"
    get "catalogs/get_all" => "catalogs#get_all"
    get "catalogs/view_catalog/:catalog_id" => "catalogs#view_catalog"
    post "catalogs" => "catalogs#create"
    get "catalogs/:catalog_id/items" => "catalogs#fetch_items"
    get "catalogs/:catalog_id/items/:item_id" => "catalogs#view_item"
    post "catalogs/:catalog_id/export_data" => "catalogs#export_data"
    post "catalogs/:catalog_id" => "catalogs#update"
    get "catalogs" => "catalogs#fetch_all"
    get "catalogs/view_media_status" => "catalogs#view_media_status"
    get "catalogs/:catalog_id" => "catalogs#show"



    #lists
    get "catalog_lists/new" => "catalog_lists#new"
    get "catalog_lists/edit" => "catalog_lists#edit"
    get "catalog_lists/view_all" => "catalog_lists#view_all"
    get "catalog_lists/view_catalog_list/:list_id" => "catalog_lists#view_catalog_list"
    get "catalog_lists/:catalog_list_id/list" => "catalog_lists#fetch_list"
    post "catalog_lists/add_items" => "catalog_lists#add_items"
    post "catalog_lists" => "catalog_lists#create"
    post "catalog_lists/:catalog_list_id" => "catalog_lists#update"
    get "catalog_lists" => "catalog_lists#fetch_all"
    get "catalog_lists/get_lists" => "catalog_lists#get_lists"
    get "catalog_lists/fetch_items" => "catalog_lists#fetch_items"
    put "catalog_lists/delete_item" => "catalog_lists#delete_item"
    get "catalog_lists/show_lists" => "catalog_lists#show_lists"
    #get "catalogs/get_info/:catalog_id" => "catalogs#view_catalog_info"
    put "catalog_lists/reorder_items" => "catalog_lists#reorder_items"

    #videolist
    post "video_list" => "video_lists#create"
    get "video_list/new" => "video_lists#new_video_list"
    get "videolist/:catalog_id/videolist_tags" => "video_lists#get_videotags"
    get "videoitems/:catalog_id/" => "video_lists#videotags_items"
    put "videolistitems" => "video_lists#videolist_items"
    get "videolist/edit" => "video_lists#edit"
    put "videolist/:videolist_id" => "video_lists#update"


    #AccessController
    get "accesscontrol/new" => "access#new"
    post "accesscontrol" => "access#create"
    put "accesscontrol" => "access#create"
    get "accesscontrol/view_all" => "access#view_all"
    get "accesscontrol/get_all" => "access#get_all"
    get "accesscontrol/details" => "access#details"
    delete "accesscontrol/delete" => "access#acdelete"


    #Subscription
    get "subscription/new" => "subscription#new"
    get "subscription/apps" => "subscription#get_apps"
    post "subscription" => "subscription#create"
    get "subscription/view_all" => "subscription#view_all"
    get "subscription/get_all" => "subscription#get_all"
    get "subscription/plans" => "subscription#add_plans"
    post "addplans" => "subscription#add_more_plans"
    put "addplans" => "subscription#add_more_plans"
    get "user/plans" => "subscription#get_plan"
    get "subscription/edit" => "subscription#get_plan"
    get "UserPacks" => "subscription#get_user_pack"
    get "tenantplans" => "subscription#tenant_plans"

    #UserSubscriptions
    get 'subscribeuser/new' => 'add_user_subscription#new'
    post 'usersubscription' => 'add_user_subscription#create'
    get 'subscribeuser/pack' => 'add_user_subscription#new'

    #Messages
    get 'messages/get_all' => "messages#get_tenant_messages"
    get 'message/edit' => "messages#message_edit"
    put 'messages' => "messages#message_update"
    get 'messages/config_edit' => "messages#config_edit"
    get "messages/get_tenant_config/:id" => "messages#get_tenant_config"
    post "messages/update_config" => "messages#update_config"
    get 'messages/view_all' => "messages#view_all"

    #Show Routes
    get  'shows/new' => "shows#new"
    post 'shows'     => "shows#create"
    get 'shows/view_all' => "shows#view_all"
    get 'shows/get_all' => "shows#get_all"
    get 'shows/edit' => "shows#edit"
    post 'shows/:id' => "shows#update"

    #tvchannels
    get "livechannels/new" => "tvchannels#new"
    post "livechannels" => "tvchannels#create"

    #Subcategory Routes
    get 'subcategories/new' => "subcategories#new"
    post 'subcategories' => "subcategories#create"
    get 'subcategories/view_all' => "subcategories#view_all"
    get 'subcategories/get_all' => "subcategories#get_all"
    get 'shows/view_subcategories' => "subcategories#view_subcategories"
    get 'subcategories/all_subcategories' => "subcategories#show_subcategories"
    get 'subcategories/view_nested_subcategories' => "subcategories#view_nested_subcategories"
    get 'subcategories/get_nested_subcategories' => "subcategories#get_nested_subcategories"
    get 'subcategories/edit' => "subcategories#edit"
    post 'subcategories/:id' => "subcategories#update"

    #Episode Routes
    get 'episodes/new' => "episodes#new"
    post 'episodes' => "episodes#create"
    get 'episodes/view_all' => "episodes#view_all"
    get 'episodes/get_all' => "episodes#get_all"

    #People Routes
    get 'people/new' => "people#new"
    post 'people' => "people#create"
    get 'people/view_all' => "people#view_all"
    get 'people/get_all' => "people#get_all"
    get 'people/get_people'=> "people#get_people"
    get 'people/edit' => "people#edit"
    post 'people/:id' => "people#update"
    get '/search_people' => "people#search_people"

    #Article routes
    get 'articles/new' => "articles#new"
    post 'articles' => "articles#create"
    get 'articles/edit' => "articles#edit"
    post 'articles/:id' => "articles#update"

    #Content Owner module routes 
    get 'content_owner_dashboard' =>"content_owner#content_owner_dashboard"
    get 'content_owner_asset_info/:show_id' =>"content_owner#content_owner_asset_info"
    post 'content_owner_ga/date_range' => "content_owner#get_co_ga_details"

    #MisDashboard module routes
    get 'mis_dashboard' =>"mis_dashboard#dashboard"
    get 'get_top_items' =>"mis_dashboard#get_top_items"
    get 'get_all_items' =>"mis_dashboard#get_all_items"
    get 'content_performance_by_owner' =>"mis_dashboard#content_performance_by_owner"
    get 'content_performance_by_owner_on_daterange' =>"mis_dashboard#content_performance_by_owner_on_daterange"
    get 'get_all_items_on_daterange' =>"mis_dashboard#get_all_items_on_daterange"
    get 'get_top_items_on_daterange' =>"mis_dashboard#get_top_items_on_daterange"
    get 'language_data' => "mis_dashboard#language_data"
    get 'language_data_on_daterange' => "mis_dashboard#language_data_on_daterange"
    get 'consumption_by_category' => "mis_dashboard#consumption_by_category"
    get 'consumption_by_category_on_daterange' => "mis_dashboard#consumption_by_category_on_daterange"
    get 'content_by_geography_country' => "mis_dashboard#content_by_geography_country"
    get 'content_by_geography_country_on_daterange' => "mis_dashboard#content_by_geography_country_on_daterange"
    get 'content_by_geography_city' => "mis_dashboard#content_by_geography_city"
    get 'content_by_geography_city_on_daterange' => "mis_dashboard#content_by_geography_city_on_daterange"
    get 'content_by_geography_zipcode' => "mis_dashboard#content_by_geography_zipcode"
    get 'content_by_geography_zipcode_on_daterange' => "mis_dashboard#content_by_geography_zipcode_on_daterange"
    get "users_by_device" => "mis_dashboard#users_by_device"
    get "users_by_device_on_daterange" => "mis_dashboard#users_by_device_on_daterange"
    #Coupon Code
    get 'create_coupon' => "CouponCode#create_coupon_code"
    post 'coupon' => "CouponCode#create_coupon"
    get 'coupon/get_all' => "CouponCode#get_all"
    get 'coupons/generated_coupons' => "CouponCode#generated_coupons"
    get '/generate_coupon_csv' => "CouponCode#generate_coupon_csv"
    get '/generate_status_csv' => "CouponCode#generate_status_csv"
    get '/generate_coupon_codes' => "CouponCode#generate_coupon_codes"
    post '/generate_coupon' => "CouponCode#generate_coupon"
    # The priority is based upon order of creation:
    # first created -> highest priority.

    # Sample of regular route:
    #   match 'products/:id' => 'catalog#view'
    # Keep in mind you can assign values other than :controller and :action

    # Sample of named route:
    #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
    # This route can be invoked with purchase_url(:id => product.id)

    # Sample resource route (maps HTTP verbs to controller actions automatically):
    #   resources :products

    # Sample resource route with options:
    #   resources :products do
    #     member do
    #       get 'short'
    #       post 'toggle'
    #     end
    #
    #     collection do
    #       get 'sold'
    #     end
    #   end

    # Sample resource route with sub-resources:
    #   resources :products do
    #     resources :comments, :sales
    #     resource :seller
    #   end

    # Sample resource route with more complex sub-resources
    #   resources :products do
    #     resources :comments
    #     resources :sales do
    #       get 'recent', :on => :collection
    #     end
    #   end

    # Sample resource route within a namespace:
    #   namespace :admin do
    #     # Directs /admin/products/* to Admin::ProductsController
    #     # (app/controllers/admin/products_controller.rb)
    #     resources :products
    #   end

    # You can have the root of your site routed with "root"
    # just remember to delete public/index.html.
    # root :to => 'welcome#index'

    # See how all your routes lay out with "rake routes"

    # This is a legacy wild controller route that's not recommended for RESTful applications.
    # Note: This route will make all actions in every controller accessible via GET requests.
    # match ':controller(/:action(/:id))(.:format)'
end

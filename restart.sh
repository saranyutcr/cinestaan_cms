sudo kill -9 $(cat aux_module/aux_module.pid)
sudo nohup ruby aux_module/aux_module.rb -o 0.0.0.0 > aux_module/nohup.out 2>&1 &
sudo kill -9 $(cat tmp/pids/server.pid)
bundle exec rake jobs:clear
sudo /usr/local/bin/ruby script/delayed_job restart
sudo /usr/local/bin/ruby /usr/local/bin/rails s thin -p 5000 -d
